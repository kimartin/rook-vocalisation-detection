"""
Implements a bunch of metrics, mostly as an exercise to myself when I was learning Tensorflow.

Most interesting are FocalCrossentropy, which implements the focal loss, and MultiChannelAUC, which generalizes the AUC
metric from Tensorflow to outputs with multiple classes (multi-label classification at least).
"""

import tensorflow as tf
import tensorflow.keras.backend as K
import numpy as np
from operator import mul
from functools import reduce
from tensorflow.python.ops import math_ops
from tensorflow.python.keras.utils import metrics_utils
from tensorflow.python.keras.utils.metrics_utils import AUCCurve
from tensorflow.python.ops import array_ops
import matplotlib.pyplot as plt
from warnings import warn


class FocalCrossentropy(tf.keras.losses.Loss):
    """
    NB: this function expects a format argument to parameter axes.
    """
    def __init__(self,
                 gamma=0.,
                 label_smoothing=0.,
                 alpha=1.,
                 cos=0.,
                 beta=0.,
                 num_classes=1,
                 class_weights=None,
                 data_format="bftc",
                 name="focal_bce",
                 pooling="max",
                 **kwargs):
        """

        :param gamma: Stength of the focal term.
        :param label_smoothing: float in [0, 1[. Applied to the ground truth labels such that the labels are rescaled
        from {0, 1} to {label_smoothing, 1-label_smoothing}
        :param cos: Strength of the cosine similarity penalty. Should be a float >= 0
        :param alpha: regularisation term for the focal loss. Defaults to 0 if gamma is 0, otherwise should be
        a positive float such that 0 < alpha < 1.
        :param beta: alternative way to essentially set alpha based on the effective number of samples. Supersedes
        alpha if given as a float such that 0 < beta < 1.
        :param class_weights: Weights to be applied to each class during computation of the loss.
        :param pooling: "max" corresponds to use max pooling on pooled axes, "mean" or "average" uses average pooling.
        :param data_format:
        :param name:
        """
        super(FocalCrossentropy, self).__init__(name=name, **kwargs)

        assert data_format in ["bftc", "btfc", "bcft", "bctf"]

        self.channel_axis = -1 if K.image_data_format() == "channels_last" else 1
        self.num_classes = num_classes

        self.data_format = data_format
        if "ft" in self.data_format:
            self.axis = -2 if self.channel_axis == -1 else -1
        else:
            self.axis = 1 if self.channel_axis == -1 else 2

        if self.num_classes > 1:
            self.cos = cos
            self.beta = beta
            self.alpha = 1. if self.beta > 0. else alpha
            self.class_weights = class_weights
            if self.class_weights is not None and self.beta > 0:
                self.class_weights = (1 - self.beta) / (1 - self.beta ** self.class_weights)
                self.class_weights = np.array(self.class_weights, dtype="float")
            if self.class_weights is not None:
                self.class_weights /= tf.reduce_sum(self.class_weights)
                self.class_weights = np.array(self.class_weights, dtype="float")
        else:
            self.cos = 0.
            self.beta = 0.
            self.alpha = 1.
            self.class_weights = None

        self.gamma = gamma
        self.label_smoothing = label_smoothing

    def cosine_penalty(self, y_pred, perm=None):
        l2 = tf.sqrt(tf.reduce_sum(y_pred ** 2, axis=1 if self.channel_axis == -1 else 2))
        l2 = tf.expand_dims(l2, axis=-1)
        l2 = tf.matmul(l2, l2, transpose_b=True)
        if perm:
            y_pred = tf.transpose(y_pred, perm=perm)
        cos_penalty = tf.matmul(y_pred, y_pred, transpose_a=True)
        cos_penalty = tf.math.divide_no_nan(cos_penalty, l2)
        cos_penalty = tf.linalg.set_diag(cos_penalty, diagonal=tf.zeros(shape=cos_penalty.shape[:-1]))
        cos_penalty = tf.maximum(0, cos_penalty)
        cos_penalty = tf.reduce_sum(cos_penalty, axis=[-1, -2])
        # not explicitly dividing by 2 since it is compensated by the sum of a symmetric matrix above, only by n(n-1)
        cos_penalty /= (y_pred.shape[self.channel_axis] * (y_pred.shape[self.channel_axis] - 1))

        return tf.reduce_mean(cos_penalty)

    def call(self, y_true, y_pred, from_logits=False):
        # Start by squeezing extra dimensions
        y_true = tf.squeeze(y_true)
        y_pred = tf.squeeze(y_pred)

        # Weigh samples for each class so that negative and positives contribute equally to the loss
        # if self.num_classes > 1:
        #     axes = list(range(len(y_true.shape)))
        #     axes.pop(self.channel_axis)
        #     true_samples = tf.reduce_mean(y_true, axis=axes, keepdims=True)
        # else:
        #     true_samples = tf.reduce_mean(y_true, keepdims=True)
        # true_samples = tf.math.divide_no_nan(true_samples, tf.reduce_sum(true_samples))
        # true_weights = (1 - true_samples) * y_true + true_samples * (1 - y_true)

        # Cosine penalty to discourage overlaps between classes
        cos_penalty = 0.
        if self.cos > 0.:
            cos_penalty = self.cosine_penalty(y_pred, perm=None)

        # Apply label smoothing
        # first line would work for true multiclass classification, but not for multilabel classification
        # y_true = y_true * (1 - self.label_smoothing * classes) + self.label_smoothing / (classes - 1)
        y_true = y_true * (1 - self.label_smoothing * 2) + self.label_smoothing

        ce = K.binary_crossentropy(y_true, y_pred, from_logits=from_logits)

        if self.gamma > 0.:
            p_t = y_true * y_pred + (1-y_true) * (1-y_pred)
            p_t = tf.pow(1 - p_t, self.gamma)
            ce *= p_t

        if self.alpha != 1.:
            a_t = y_true * self.alpha + (1 - y_true) * (1 - self.alpha)
            ce *= a_t
        elif self.beta > 0. or self.class_weights is not None:
            ce *= self.class_weights

        # Apply weights so that positive samples are not overtaken by negative samples
        #ce *= true_weights

        if tf.rank(ce) == 3:
            ce = tf.reduce_sum(ce, axis=self.channel_axis)

        return ce * (1 + self.cos * cos_penalty)

    def get_config(self):
        conf = super(FocalCrossentropy, self).get_config()
        conf.update({
            "gamma": self.gamma,
            "alpha": self.alpha,
            "beta": self.beta,
            "cos": self.cos,
            "num_classes": self.num_classes,
            "label_smoothing": self.label_smoothing,
            "class_weights": self.class_weights,
            "data_format": self.data_format,
        })
        return conf


class MyMSE(tf.keras.losses.Loss):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def call(self, y_true, y_pred):
        return K.mean((y_true - y_pred)**2)


class BaseConfusionMatrixMetric(tf.keras.metrics.Metric):
    def __init__(self,
                 threshold=.5,
                 num_classes=1,
                 mode="micro",
                 name="name",
                 soft=False,
                 **kwargs):
        super().__init__(name=f"soft_{name}" if soft else name, **kwargs)
        self.threshold = threshold
        self.mode = mode
        self.num_classes = num_classes
        self.soft = soft

        self.total_cm = self.add_weight(name="cm",
                                        initializer="zeros",
                                        dtype=self.dtype,
                                        shape=(self.num_classes, 2, 2))
        self.channel_axis = -1 if K.image_data_format() == "channels_last" else 1

    def get_config(self):
        conf = super().get_config()
        conf.update({
            "threshold": self.threshold,
            "num_classes": self.num_classes,
            "mode": self.mode,
            "soft": self.soft,
        })
        return conf

    def cm(self, true, pred):
        # Confusion matrix using basically weighted averages
        keepdims = self.num_classes == 1
        true_samples = tf.reduce_sum(true, axis=0, keepdims=keepdims)
        false_samples = tf.reduce_sum(1 - true, axis=0, keepdims=keepdims)
        tp = tf.math.divide_no_nan(tf.reduce_sum(true * pred, axis=0, keepdims=keepdims),
                                   true_samples)
        tn = tf.math.divide_no_nan(tf.reduce_sum((1 - true) * (1 - pred), axis=0, keepdims=keepdims),
                                   false_samples)
        fp = tf.math.divide_no_nan(tf.reduce_sum((1 - true) * pred, axis=0, keepdims=keepdims),
                                   false_samples)
        fn = tf.math.divide_no_nan(tf.reduce_sum(true * (1 - pred), axis=0, keepdims=keepdims),
                                   true_samples)
        cm = tf.stack([tf.stack([tn, fp], axis=-1),
                       tf.stack([fn, tp], axis=-1)],
                      axis=-1)
        return cm

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_true = tf.squeeze(y_true)
        y_pred = tf.squeeze(y_pred)

        # take the maximum over the axes of y_pred not in y_true
        if y_pred.shape != y_true.shape:
            axes = [i for i, j in enumerate(y_pred.shape) if j not in y_true.shape]
            y_pred = tf.reduce_max(y_pred, axis=axes)

        if sample_weight is not None:
            sample_weight = tf.cast(sample_weight, y_pred.dtype)
            y_pred = tf.multiply(y_pred, sample_weight)

        if self.num_classes > 1:
            # preserve the class axis and transpose to (num_classes, -1)
            y_true = tf.reshape(y_true, shape=[-1, y_true.shape[self.channel_axis]])
            y_pred = tf.reshape(y_pred, shape=[-1, y_pred.shape[self.channel_axis]])
        else:
            # flatten all the outputs and labels
            y_true = tf.reshape(y_true, shape=-1)
            y_pred = tf.reshape(y_pred, shape=-1)

        if not self.soft:
            y_pred = tf.cast(y_pred > self.threshold, y_pred.dtype)

        cm = self.cm(y_true, y_pred)

        self.total_cm.assign_add(cm)
        return self.total_cm

    def op(self, cm):
        pass

    def result(self):
        if self.mode == "micro":
            cm = tf.reduce_sum(self.total_cm, axis=0)
            res = self.op(cm)
        else:
            res = tf.map_fn(self.op, elems=self.total_cm)
            res = tf.reduce_mean(res)
        return res

    def reset_states(self):
        self.total_cm.assign(value=tf.zeros_like(self.total_cm))


class Jaccard(BaseConfusionMatrixMetric):
    def __init__(self, threshold=.5, name="jacc", **kwargs):
        super().__init__(threshold=threshold, name=name, **kwargs)

    def op(self, cm):
        result = cm[1, 1] / (cm[1, 1] + cm[0, 1] + cm[1, 0])
        return result


class MatthewsCorrelationCoefficient(BaseConfusionMatrixMetric):
    def __init__(self, threshold=.5, name="mcc", **kwargs):
        super().__init__(threshold=threshold, name=name, **kwargs)

    def op(self, cm):
        num = cm[0, 0] * cm[1, 1] - cm[1, 0] * cm[0, 1]
        denom = tf.math.sqrt((cm[0, 0] + cm[0, 1]) *
                             (cm[0, 0] + cm[1, 0]) *
                             (cm[1, 1] + cm[0, 1]) *
                             (cm[1, 1] + cm[1, 0]))
        result = tf.math.divide_no_nan(num, denom)
        return result


class BalancedAccuracy(BaseConfusionMatrixMetric):
    def __init__(self, threshold=.5, name="balanced_accuracy", **kwargs):
        super().__init__(threshold=threshold, name=name, **kwargs)

    def op(self, cm):
        neg = tf.math.divide_no_nan(cm[0, 0], cm[0, 0] + cm[0, 1])
        pos = tf.math.divide_no_nan(cm[1, 1], cm[1, 1] + cm[1, 0])
        return (neg + pos) / 2


class Accuracy(BalancedAccuracy):
    def __init__(self, threshold=.5, name="accuracy", **kwargs):
        super().__init__(threshold=threshold, name=name, **kwargs)

    def op(self, cm):
        correct = tf.reduce_sum(tf.linalg.diag_part(cm))
        return tf.math.divide_no_nan(correct, tf.reduce_sum(cm))


class Precision(BaseConfusionMatrixMetric):
    def __init__(self, threshold=.5, name="precision", **kwargs):
        super().__init__(threshold=threshold, name=name, **kwargs)

    def op(self, cm):
        true_pos = cm[1, 1]
        false_pos = cm[0, 1]
        return tf.math.divide_no_nan(true_pos, true_pos + false_pos)


class Recall(BaseConfusionMatrixMetric):
    def __init__(self, threshold=.5, name="recall", **kwargs):
        super().__init__(threshold=threshold, name=name, **kwargs)

    def op(self, cm):
        true_pos = cm[1, 1]
        false_neg = cm[1, 0]
        return tf.math.divide_no_nan(true_pos, true_pos + false_neg)


class fScore(BaseConfusionMatrixMetric):
    def __init__(self, threshold=.5, name="f", beta=1, **kwargs):
        super().__init__(threshold=threshold, name=f"{name}{beta}", **kwargs)
        self.beta = beta

    def op(self, cm):
        true_pos = cm[1, 1]
        false_pos = cm[0, 1]
        false_neg = cm[1, 0]

        f = tf.math.divide_no_nan(self.beta ** 2 * false_neg + false_pos, (1 + self.beta ** 2) * true_pos)

        return 1 / (1 + f)

    def get_config(self):
        conf = super().get_config()
        conf.update({"beta": self.beta})
        return conf


class IOU(BaseConfusionMatrixMetric):
    def __init__(self, threshold=.5, name="iou", **kwargs):
        super().__init__(threshold=threshold, name=name, **kwargs)

    def op(self, cm):
        # IOU essentially counts everything but TN
        tp = cm[1, 1]
        denom = tp + cm[0, 1] + cm[1, 0]
        return tf.math.divide_no_nan(tp, denom)


# class MultichannelAUC(tf.keras.metrics.AUC):
#     def __init__(self,
#                  num_classes=1,
#                  mode="micro",
#                  reduce_axis=None,
#                  norm=False,
#                  curve='roc',
#                  summation_method='interpolation',
#                  **kwargs):
#         if "multi_label" in kwargs:
#             kwargs.pop("multi_label")
#
#         self.prg = False
#         self.norm = norm
#         self.curve_replacement = curve
#         if curve in ["prg", "PRG"]:
#             curve = 'pr'
#             self.curve_replacement = 'pr'
#             self.prg = True
#         self.summation_method_replacement = summation_method
#
#         super().__init__(multi_label=num_classes > 1,
#                          curve=curve,
#                          summation_method=summation_method,
#                          **kwargs)
#
#         self.num_classes = num_classes
#         self.mode = mode
#         self.reduce_axis = reduce_axis
#         # since the code below works only with -1, might as well hard-code it for now
#         self.channel_axis = -1 # if K.image_data_format() == "channels_last" else 1
#
#     def update_state(self, y_true, y_pred, sample_weight=None):
#         y_pred = tf.squeeze(y_pred)
#         y_true = tf.squeeze(y_true)
#
#         if self.reduce_axis is not None:
#             y_pred = tf.reduce_max(y_pred, axis=self.reduce_axis)
#             y_true = tf.reduce_max(y_true, axis=self.reduce_axis)
#         if y_pred.shape != y_true.shape:
#             y_pred = tf.reduce_max(y_pred, axis=[i for i, j in enumerate(y_pred.shape) if j not in y_true.shape])
#         if self.num_classes > 1:
#             # For ease of coding, self.channel_axis is effectively forced to -1
#             if self.channel_axis != -1:
#                 perm = list(range(len(y_true.shape)))
#                 perm.pop(self.channel_axis)
#                 perm.append(self.channel_axis)
#                 y_true = tf.transpose(y_true, perm=perm)
#                 y_pred = tf.transpose(y_pred, perm=perm)
#             y_true = tf.reshape(y_true, shape=(-1, y_true.shape[-1]))
#             y_pred = tf.reshape(y_pred, shape=(-1, y_true.shape[-1]))
#
#         if sample_weight is not None:
#             sample_weight = tf.clip_by_value(tf.reshape(sample_weight, shape=(-1, y_true.shape[-1])), K.epsilon(), 1-K.epsilon())
#
#         y_pred = tf.clip_by_value(y_pred, K.epsilon(), 1-K.epsilon())
#         super().update_state(y_true, y_pred, sample_weight=sample_weight)
#
#     def interpolate_pr_auc(self):
#         """
#         Slight modification to only take non-missing classes into account
#         """
#         dtp = self.true_positives[:self.num_thresholds - 1] - self.true_positives[1:]
#         p = self.true_positives + self.false_positives
#         dp = p[:self.num_thresholds - 1] - p[1:]
#         prec_slope = math_ops.div_no_nan(
#             dtp, math_ops.maximum(dp, 0), name='prec_slope')
#         intercept = self.true_positives[1:] - math_ops.multiply(prec_slope, p[1:])
#
#         safe_p_ratio = array_ops.where(
#             math_ops.logical_and(p[:self.num_thresholds - 1] > 0, p[1:] > 0),
#             math_ops.div_no_nan(
#                 p[:self.num_thresholds - 1],
#                 math_ops.maximum(p[1:], 0),
#                 name='recall_relative_ratio'),
#             array_ops.ones_like(p[1:]))
#
#         pr_auc_increment = math_ops.div_no_nan(
#             prec_slope * (dtp + intercept * math_ops.log(safe_p_ratio)),
#             math_ops.maximum(self.true_positives[1:] + self.false_negatives[1:], 0),
#             name='pr_auc_increment')
#
#         if self.multi_label:
#             by_label_auc = math_ops.reduce_sum(
#                 pr_auc_increment, name=self.name + '_by_label', axis=0)
#
#             if self.norm:
#                 pos = math_ops.div_no_nan(self.true_positives[0] + self.false_negatives[0],
#                                           self.true_positives[0] + self.false_negatives[0] + self.false_positives[0] + self.true_negatives[0])
#                 min_auc = 1 + math_ops.div_no_nan((1 - pos) * math_ops.log(1 - pos), pos)
#                 by_label_auc = math_ops.div_no_nan(by_label_auc - min_auc, (1 - min_auc))
#
#             if self.label_weights is None:
#                 # Evenly weighted average of the label AUCs.
#                 non_missing_classes = tf.cast(
#                     self.true_positives + self.false_negatives > 0.,
#                     by_label_auc.dtype
#                 )
#                 non_missing_classes = math_ops.reduce_max(
#                     non_missing_classes,
#                     axis=list(range(len(non_missing_classes.shape) - 1)),
#                     keepdims=True,
#                 )
#                 non_missing_classes = math_ops.reduce_sum(non_missing_classes)
#                 return math_ops.div_no_nan(math_ops.reduce_sum(by_label_auc), non_missing_classes, name=self.name)
#             else:
#                 # Weighted average of the label AUCs.
#                 if self.norm:
#                     pos = math_ops.div_no_nan(self.true_positives[0] + self.false_negatives[0],
#                                               self.true_positives[0] + self.false_negatives[0] + self.false_positives[
#                                                   0] + self.true_negatives[0])
#                     min_auc = 1 + math_ops.div_no_nan((1 - pos) * math_ops.log(1 - pos), pos)
#                     by_label_auc = math_ops.div_no_nan(by_label_auc - min_auc, (1 - min_auc))
#
#                 return math_ops.div_no_nan(
#                     math_ops.reduce_sum(
#                         math_ops.multiply(by_label_auc, self.label_weights)),
#                     math_ops.reduce_sum(self.label_weights),
#                     name=self.name)
#         else:
#             # how to norm here?
#             return math_ops.reduce_sum(pr_auc_increment, name='interpolate_pr_auc')
#
#     def result(self):
#         # Mostly taken from the tensorflow base code, but modified so that the average only takes into account
#         # labels with TP + FP + FN > 1.
#         if (self.curve_replacement in ['PR', 'pr'] and
#                 self.summation_method_replacement == 'interpolation'):
#             # This use case is different and is handled separately.
#             return self.interpolate_pr_auc()
#
#         # Set `x` and `y` values for the curves based on `curve` config.
#         recall = math_ops.div_no_nan(self.true_positives,
#                                      self.true_positives + self.false_negatives)
#         if self.curve_replacement in ['ROC', 'roc']:
#             fp_rate = math_ops.div_no_nan(self.false_positives,
#                                           self.false_positives + self.true_negatives)
#             x = fp_rate
#             y = recall
#         else:  # curve == 'PR'.
#             precision = math_ops.div_no_nan(
#                 self.true_positives, self.true_positives + self.false_positives)
#             if self.prg:
#                 pos = (self.true_positives + self.false_negatives) / (self.true_positives + self.false_negatives + self.true_negatives + self.false_positives)
#                 x = math_ops.maximum(0, math_ops.div_no_nan(recall - pos, (1-pos)*recall))
#                 y = math_ops.maximum(0, math_ops.div_no_nan(precision - pos, (1-pos)*precision))
#             else:
#                 x = recall
#                 y = precision
#
#         # Find the rectangle heights based on `summation_method`.
#         if self.summation_method_replacement == 'interpolation':
#             # Note: the case ('PR', 'interpolation') has been handled above.
#             heights = (y[:self.num_thresholds - 1] + y[1:]) / 2.
#         elif self.summation_method_replacement == 'minoring':
#             heights = math_ops.minimum(y[:self.num_thresholds - 1], y[1:])
#         else:  # self.summation_method = metrics_utils.AUCSummationMethod.MAJORING:
#             heights = math_ops.maximum(y[:self.num_thresholds - 1], y[1:])
#
#         # Sum up the areas of all the rectangles.
#         if self.multi_label:
#             riemann_terms = math_ops.multiply(x[:self.num_thresholds - 1] - x[1:],
#                                               heights)
#             by_label_auc = math_ops.reduce_sum(
#                 riemann_terms, name=self.name + '_by_label', axis=0)
#
#             if self.norm:
#                 pos = math_ops.div_no_nan(self.true_positives[0] + self.false_negatives[0],
#                                           self.true_positives[0] + self.false_negatives[0] + self.false_positives[0] + self.true_negatives[0])
#                 min_auc = 1 + math_ops.div_no_nan((1 - pos) * math_ops.log(1 - pos), pos)
#                 by_label_auc = math_ops.div_no_nan(by_label_auc - min_auc, (1 - min_auc))
#
#             if self.label_weights is None:
#                 # Unweighted average of the label AUCs.
#                 non_missing_classes = tf.cast(
#                     self.true_positives + self.false_negatives > 0.,
#                     by_label_auc.dtype
#                     )
#                 non_missing_classes = math_ops.reduce_max(
#                     non_missing_classes,
#                     axis=list(range(len(non_missing_classes.shape) - 1)),
#                     keepdims=True,
#                 )
#                 non_missing_classes = math_ops.reduce_sum(non_missing_classes)
#                 return math_ops.div_no_nan(math_ops.reduce_sum(by_label_auc), non_missing_classes, name=self.name)
#             else:
#                 # Weighted average of the label AUCs.
#                 return math_ops.div_no_nan(
#                     math_ops.reduce_sum(
#                         math_ops.multiply(by_label_auc, self.label_weights)),
#                     math_ops.reduce_sum(self.label_weights),
#                     name=self.name)
#         else:
#             return math_ops.reduce_sum(
#                 math_ops.multiply(x[:self.num_thresholds - 1] - x[1:], heights),
#                 name=self.name)
#
#     def get_config(self):
#         conf = super().get_config()
#         conf.update({'num_classes': self.num_classes,
#                      'mode': self.mode,
#                      'norm': self.norm,
#                      'curve_replacement': self.curve_replacement,
#                      'summation_method_replacement': self.summation_method_replacement})
#         return conf


class MultichannelAUC(tf.keras.metrics.AUC):
    def __init__(self, num_classes=1, mode="micro", reduce_axis=None, norm=False, **kwargs):
        if "multi_label" in kwargs:
            kwargs.pop("multi_label")

        self.prg = False
        self.norm = norm
        if "curve" in kwargs and kwargs['curve'] == "prg":
            kwargs['curve'] = "pr"
            self.prg = True

        super().__init__(multi_label=num_classes > 1, **kwargs)
        self.num_classes = num_classes
        self.mode = mode
        self.reduce_axis = reduce_axis
        # since the code below works only with -1, might as well hard-code it for now
        self.channel_axis = -1  # if K.image_data_format() == "channels_last" else 1
        self.shape_ = tf.constant([-1, self.num_classes])

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_pred = tf.squeeze(y_pred)
        y_true = tf.squeeze(y_true)

        if self.reduce_axis is not None:
            y_pred = tf.reduce_max(y_pred, axis=self.reduce_axis)
            y_true = tf.reduce_max(y_true, axis=self.reduce_axis)
        # if y_pred.shape != y_true.shape:
        #     y_pred = tf.reduce_max(y_pred, axis=[i for i, j in enumerate(y_pred.shape) if j not in y_true.shape])

        if self.num_classes > 1:
            # For ease of coding, self.channel_axis is effectively forced to -1
            if self.channel_axis != -1:
                perm = list(range(len(y_true.shape)))
                perm.pop(self.channel_axis)
                perm.append(self.channel_axis)
                y_true = tf.transpose(y_true, perm=perm)
                y_pred = tf.transpose(y_pred, perm=perm)
            y_true = tf.reshape(y_true, shape=self.shape_)
            y_pred = tf.reshape(y_pred, shape=self.shape_)

        if sample_weight is not None:
            sample_weight = tf.clip_by_value(tf.reshape(sample_weight, shape=self.shape_), K.epsilon(), 1-K.epsilon())

        y_pred = tf.clip_by_value(y_pred, K.epsilon(), 1-K.epsilon())
        super().update_state(y_true, y_pred, sample_weight=sample_weight)

    def interpolate_pr_auc(self):
        """
        Slight modification to only take non-missing classes into account
        """
        dtp = self.true_positives[:self.num_thresholds - 1] - self.true_positives[1:]
        p = self.true_positives + self.false_positives
        dp = p[:self.num_thresholds - 1] - p[1:]
        prec_slope = math_ops.div_no_nan(
            dtp, math_ops.maximum(dp, 0), name='prec_slope')
        intercept = self.true_positives[1:] - math_ops.multiply(prec_slope, p[1:])

        safe_p_ratio = array_ops.where(
            math_ops.logical_and(p[:self.num_thresholds - 1] > 0, p[1:] > 0),
            math_ops.div_no_nan(
                p[:self.num_thresholds - 1],
                math_ops.maximum(p[1:], 0),
                name='recall_relative_ratio'),
            array_ops.ones_like(p[1:]))

        pr_auc_increment = math_ops.div_no_nan(
            prec_slope * (dtp + intercept * math_ops.log(safe_p_ratio)),
            math_ops.maximum(self.true_positives[1:] + self.false_negatives[1:], 0),
            name='pr_auc_increment')

        if self.multi_label:
            by_label_auc = math_ops.reduce_sum(
                pr_auc_increment, name=self.name + '_by_label', axis=0)

            if self.norm:
                pos = math_ops.div_no_nan(self.true_positives[0] + self.false_negatives[0],
                                          self.true_positives[0] + self.false_negatives[0] + self.false_positives[0] + self.true_negatives[0])
                min_auc = 1 + math_ops.div_no_nan((1 - pos) * math_ops.log(1 - pos), pos)
                by_label_auc = math_ops.div_no_nan(by_label_auc - min_auc, (1 - min_auc))

            if self.label_weights is None:
                # Evenly weighted average of the label AUCs.
                non_missing_classes = tf.cast(
                    self.true_positives + self.false_negatives > 0.,
                    by_label_auc.dtype
                )
                non_missing_classes = math_ops.reduce_max(
                    non_missing_classes,
                    axis=list(range(len(non_missing_classes.shape) - 1)),
                    keepdims=True,
                )
                non_missing_classes = math_ops.reduce_sum(non_missing_classes)
                return math_ops.div_no_nan(math_ops.reduce_sum(by_label_auc), non_missing_classes, name=self.name)
            else:
                # Weighted average of the label AUCs.
                if self.norm:
                    pos = math_ops.div_no_nan(self.true_positives[0] + self.false_negatives[0],
                                              self.true_positives[0] + self.false_negatives[0] + self.false_positives[
                                                  0] + self.true_negatives[0])
                    min_auc = 1 + math_ops.div_no_nan((1 - pos) * math_ops.log(1 - pos), pos)
                    by_label_auc = math_ops.div_no_nan(by_label_auc - min_auc, (1 - min_auc))

                return math_ops.div_no_nan(
                    math_ops.reduce_sum(
                        math_ops.multiply(by_label_auc, self.label_weights)),
                    math_ops.reduce_sum(self.label_weights),
                    name=self.name)
        else:
            # how to norm here?
            return math_ops.reduce_sum(pr_auc_increment, name='interpolate_pr_auc')

    def result(self):
        # Mostly taken from the tensorflow base code, but modified so that the average only takes into account
        # labels with TP + FP + FN > 1.
        if (self.curve == metrics_utils.AUCCurve.PR and
                self.summation_method == metrics_utils.AUCSummationMethod.INTERPOLATION
        ):
            # This use case is different and is handled separately.
            return self.interpolate_pr_auc()

        # Set `x` and `y` values for the curves based on `curve` config.
        recall = math_ops.div_no_nan(self.true_positives,
                                     self.true_positives + self.false_negatives)
        if self.curve == metrics_utils.AUCCurve.ROC:
            fp_rate = math_ops.div_no_nan(self.false_positives,
                                          self.false_positives + self.true_negatives)
            x = fp_rate
            y = recall
        else:  # curve == 'PR'.
            precision = math_ops.div_no_nan(
                self.true_positives, self.true_positives + self.false_positives)
            if self.prg:
                pos = (self.true_positives + self.false_negatives) / (self.true_positives + self.false_negatives + self.true_negatives + self.false_positives)
                x = math_ops.maximum(0, math_ops.div_no_nan(recall - pos, (1-pos)*recall))
                y = math_ops.maximum(0, math_ops.div_no_nan(precision - pos, (1-pos)*precision))
            else:
                x = recall
                y = precision

        # Find the rectangle heights based on `summation_method`.
        if self.summation_method == metrics_utils.AUCSummationMethod.INTERPOLATION:
            # Note: the case ('PR', 'interpolation') has been handled above.
            heights = (y[:self.num_thresholds - 1] + y[1:]) / 2.
        elif self.summation_method == metrics_utils.AUCSummationMethod.MINORING:
            heights = math_ops.minimum(y[:self.num_thresholds - 1], y[1:])
        else:  # self.summation_method = metrics_utils.AUCSummationMethod.MAJORING:
            heights = math_ops.maximum(y[:self.num_thresholds - 1], y[1:])

        # Sum up the areas of all the rectangles.
        if self.multi_label:
            riemann_terms = math_ops.multiply(x[:self.num_thresholds - 1] - x[1:],
                                              heights)
            by_label_auc = math_ops.reduce_sum(
                riemann_terms, name=self.name + '_by_label', axis=0)

            if self.norm:
                pos = math_ops.div_no_nan(self.true_positives[0] + self.false_negatives[0],
                                          self.true_positives[0] + self.false_negatives[0] + self.false_positives[0] + self.true_negatives[0])
                min_auc = 1 + math_ops.div_no_nan((1 - pos) * math_ops.log(1 - pos), pos)
                by_label_auc = math_ops.div_no_nan(by_label_auc - min_auc, (1 - min_auc))

            if self.label_weights is None:
                # Unweighted average of the label AUCs.
                non_missing_classes = tf.cast(
                    self.true_positives + self.false_negatives > 0.,
                    by_label_auc.dtype
                    )
                non_missing_classes = math_ops.reduce_max(
                    non_missing_classes,
                    axis=list(range(len(non_missing_classes.shape) - 1)),
                    keepdims=True,
                )
                non_missing_classes = math_ops.reduce_sum(non_missing_classes)
                return math_ops.div_no_nan(math_ops.reduce_sum(by_label_auc), non_missing_classes, name=self.name)
            else:
                # Weighted average of the label AUCs.
                return math_ops.div_no_nan(
                    math_ops.reduce_sum(
                        math_ops.multiply(by_label_auc, self.label_weights)),
                    math_ops.reduce_sum(self.label_weights),
                    name=self.name)
        else:
            return math_ops.reduce_sum(
                math_ops.multiply(x[:self.num_thresholds - 1] - x[1:], heights),
                name=self.name)

    def get_config(self):
        conf = super().get_config()
        conf.update({"num_classes": self.num_classes, "mode": self.mode, "norm": self.norm})
        return conf


class ExpectedCalibrationError(tf.keras.metrics.Metric):
    def __init__(self, num_bins=100, multilabel=False, name="ece", **kwargs):
        super().__init__(name=name, **kwargs)
        self.num_bins = num_bins
        self.multilabel = multilabel
        thresholds = [(i + 1) * 1.0 / self.num_bins for i in range(self.num_bins - 1)]
        self._thresholds = np.array([0.0 - K.epsilon()] + thresholds +
                                    [1.0 + K.epsilon()])

        self.channel_axis = -1 if K.image_data_format() == "channels_last" else 1

        self._built = False
        self._shape = None
        if not self.multilabel:
            self._build(None)

    def _build(self, input_shape):
        # input_shape should be (batch, ..., classes)
        if self.multilabel:
            shape = (self.num_bins, input_shape[self.channel_axis])
        else:
            shape = (self.num_bins, )

        self.accuracy = self.add_weight(shape=shape,
                                        name="accuracy",
                                        initializer="zeros")
        self.confidence = self.add_weight(shape=shape,
                                          name="confidence",
                                          initializer="zeros")
        self.samples = self.add_weight(shape=shape,
                                       name="samples",
                                       initializer="zeros")
        self._built = True
        self._shape = input_shape
        super().build(input_shape)

    def update_state(self, y_true, y_pred, sample_weight=None):
        if not self._built:
            self._build(y_true.shape)

        if sample_weight is not None:
            y_pred *= sample_weight

        if not self.multilabel:
            y_pred = K.flatten(y_pred)
            y_true = K.flatten(y_true)
            samples = np.empty(shape=(self.num_bins, ))
            confidence = np.empty(shape=(self.num_bins, ))
            accuracy = np.empty(shape=(self.num_bins, ))
        else:
            samples = np.empty(shape=(self.num_bins, y_true.shape[self.channel_axis]))
            confidence = np.empty(shape=(self.num_bins, y_true.shape[self.channel_axis]))
            accuracy = np.empty(shape=(self.num_bins, y_true.shape[self.channel_axis]))

        for thr in range(self.num_bins):
            lower, upper = self._thresholds[thr], self._thresholds[thr+1]
            pos = tf.where(tf.math.logical_and(y_pred >= lower, y_pred < upper),
                           y_pred,
                           tf.zeros_like(y_pred))
            truth = tf.where(tf.math.logical_and(y_pred >= lower, y_pred < upper),
                             y_true,
                             tf.zeros_like(y_pred))
            if self.multilabel:
                sh = list(range(len(pos.shape)))
                sh.pop(self.channel_axis)
                sh = tuple(sh)
                samples[thr, :] = np.sum(pos > 0, axis=sh)
                accuracy[thr, :] = np.sum(truth, axis=sh)
                confidence[thr, :] = np.sum(pos, axis=sh)
            else:
                samples[thr] = np.sum(pos > 0)
                accuracy[thr] = np.sum(truth)
                confidence[thr] = np.sum(pos)

        self.accuracy.assign_add(accuracy)
        self.samples.assign_add(samples)
        self.confidence.assign_add(confidence)

    def reset_states(self):
        self._build(self._shape)

    def result(self):
        res = tf.abs(self.accuracy - self.confidence)
        res = tf.math.divide_no_nan(res, self.samples)
        res = tf.reduce_mean(res)

        return res


class DecisionUncertainty(tf.keras.metrics.Metric):
    def __init__(self, **kwargs):
        super().__init__(name="h", **kwargs)
        self.channel_axis = -1 if K.image_data_format() == "channels_last" else 1
        self.built = False
        self.denom = None

        self.H = self.add_weight(shape=(),
                                 dtype=self.dtype,
                                 name="H",
                                 initializer="zeros")
        self.samples = self.add_weight(shape=(),
                                       dtype=self.dtype,
                                       name = "samples",
                                       initializer="zeros")

    def entropy(self, x):
        x = x / tf.reduce_sum(x, axis=self.channel_axis, keepdims=True)
        x = tf.clip_by_value(x, K.epsilon(), 1-K.epsilon())
        x *= - tf.math.log(x) / self.denom
        return tf.reduce_sum(x, axis=self.channel_axis)

    def update_state(self, y_true, y_pred, sample_weight = None):
        if not self.built:
            self.denom = tf.math.log(tf.constant(y_pred.shape[self.channel_axis], dtype=y_pred.dtype))
            self.built = True

        shape = list(y_true.shape)
        shape.pop(self.channel_axis)
        samples = reduce(mul, shape)

        entropy = self.entropy(y_pred)
        entropy *= tf.reduce_max(y_true, self.channel_axis, keepdims=True)

        if sample_weight is not None:
            entropy *= sample_weight
            sample_weight = tf.reduce_sum(1. - sample_weight)
        else:
            sample_weight = 0.
        entropy = tf.reduce_sum(entropy)

        self.samples.assign_add(samples - sample_weight)
        self.H.assign_add(entropy)

    def result(self):
        return self.H / self.samples

# class EqualErrorRate(tf.keras.metrics.Metric):
#     def result(self):
#         pass
#
#     def __init__(self, **kwargs):
#         super().__init__(**kwargs)
#
#     def update_state(self, y_true, y_pred, sample_weight):
#         fpr, tpr, thresholds = roc_curve(K.flatten(y_true), K.flatten(tf.reduce_max(y_pred, axis=2)),
#                                          sample_weight=sample_weight)
#         fnr = 1 - tpr
#         eer = thresholds[np.argmin(np.absolute(fpr - fnr))]
#
#         return eer


