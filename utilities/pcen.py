import matplotlib.pyplot as plt
import librosa
import librosa.display as disp
import soundfile as sf
from math import exp
from utilities.functions import *
import tensorflow.keras.backend as K
from utilities.custom_layers import BoundedTruncNorm


def pcen(x, eps=1E-6, s=0.025, alpha=0.98, delta=2.0, r=0.5, last_state=None,
         return_last=False, time_axis=None, mask=None):
    """
    Computes the PCEN of the given spectrogram x.
    The spectrogram may be of arbitrary scaling and dimensions.
    Current defaults match the settings suggested on https://www.kaggle.com/c/freesound-audio-tagging-2019/discussion/91859,
    for much lower scales of signal.
    To match the librosa implementation (in theory) ensure x in ]-2**31, 2**31-1[ and set:
    eps=1E-6, s=0.025, alpha=0.98, delta=2, r=0.5

    :param x: numpy array or array-like. The input spectrogram.
    :param eps: positive float. Minuscule value to avoid dividing by 0.
    :param s: positive float. The smoothing coefficient. Set to small-ish value.
    :param alpha: positive float. The gain parameter. Should be close to 1 (but still lower).
    :param delta: positive float. The bias parameter.
    :param r: positive float. The power parameter. Recommendations are between .25 and .5
    :param last_state: can be used to initialise the smoother instead of taking the first frame of the spectrogram.
    If provided, should be an array of dimension (x.shape[0], )
    :param implementation: "scipy", "librosa" or None. If "scipy" or "librosa", use scipy.signal to create m. Else,
    iteratively create m starting from last_state (if provided) or the first frame of x. Notably, the librosa.core.pcen
    function functionally assumes a stationary state equal to the first frame of the spectrogram, hence the small
    differences obtained between the two implementations.
    :param return_last: bool. Whether (True) or not (False) to output the last state of the smoother. True essentially
    allows the transformation to be applied to long files.

    :return: tensor of the same dimensions as x
    """
    shape = list(x.shape)

    m_frames = []
    slc = [slice(None)] * len(shape)
    for f in range(shape[time_axis]):
        slc[time_axis] = slice(f, f+1)
        if last_state is None:
            last_state = x[tuple(slc)]
            m_frames.append(last_state)
            continue
        m_frame = (1 - s) * last_state + s * x[tuple(slc)]
        last_state = m_frame
        m_frames.append(m_frame)

    m = tf.concat(m_frames, axis=time_axis)

    smooth = tf.pow(eps + m, alpha)
    g = tf.math.divide_no_nan(x, smooth)
    pcen_ = tf.pow(g + delta, r) - tf.pow(delta, r)

    # apply mask
    if mask is not None:
        mask = tf.expand_dims(tf.cast(mask, dtype=pcen_.dtype), axis=len(mask.shape))
        pcen_ = pcen_ * mask

    if not return_last:
        last_state = None

    return pcen_, last_state


class PCEN(tf.keras.layers.Layer):
    def __init__(self,
                 alpha=0.98,
                 delta=2.0,
                 r=0.5,
                 eps=1E-6,
                 s=0.025,
                 absolute_sd=True,
                 learn_axis=None,
                 sd=1.0,
                 implementation=None,
                 return_last=False,
                 last=None,
                 name="PCEN",
                 **kwargs):
        """
        Implementation of PCEN as a trainable layer for Keras/Tensorflow. Trainability is possible at different levels,
        from single value parameters, to one value per frequency band or per time frame, or even per spectrogram
        coefficient.

        :param alpha: The gain parameter.
        :param delta: The bias parameter.
        :param r: The power parameter.
        :param eps: float. The soft threshold of the DRC part of the procedure.
        :param s: float. The smoothing coefficient (basically how fast the IIR filter will filter out static noise).
        Strictly between 0 and 1. Defaults to 0.025
        :param training: bool. Whether to allow the parameters to be learned (if True) or not. Defaults to True.
        :param learn_axis: Should be one of None, "frequency", or "both". If None, all the parameters are
        learnable but are single values. For other values, the parameters are allowed to vary along the corresponding
        axis (i.e. for "frequency", one value per frequency band is learned). Defaults to None.
        :param implementation: None, "scipy" or "librosa". If None, use the original, iterative implementation of the
        IIR filter defined in [1]. Else, use the same(-ish) implementation as librosa, based on scipy.signal.lfilter.
        Notably, if using scipy.signal.lfilter, only single values of s are accepted (giving s as an array attempts to
        create a filter of order the number of frequency bands in the spectrogram, which breaks lfilter).
        :param sd: float, list or dict. The standard deviation(s) to apply to the initialisers. Ignored if
        training = False or learn_axis = None. If provided as a dict, parameters not in the keys of the dict will be
        generated as if given sd[paramater] = 0. If provided as a list, throws error if length is not 1 or 5. If a list
        of length 5 is given, the respective sd values are applied in the order [alpha, delta, r, eps, s].
        :param absolute_sd: bool. Whether to use sd "as is" (resulting in all parameters being generated with the
        provided sd exactly), or multiply by the value of the parameter (resulting in larger parameters having larger
        sd). For best results, if True, sd should not be set higher than 0.5
        (which will then signify that the parameters will be initialised via a truncated normal within
        ]0; 2*value[ where value is the value of the respective parameter (eps, s, alpha, delta, r). For s and alpha however,
        2*value is bounded by 1.
        :param return_last: bool. Passed to the pcen function. If true, returns the last frame of the smoother, allowing
        streaming transformations
        :param kwargs:

        :return: PCEN-adjusted spectrogram.

        References:
        [1] Y. Wang, P. Getreuer, T. Hughes, R. F. Lyon, and R. A. Saurous,  “Trainable frontend for robust and
        far-field keyword spotting,” in Proc. IEEE ICASSP, 2017.
        """
        super(PCEN, self).__init__(name=name, **kwargs)

        self.time_axis = 2  # temporary

        self.supports_masking = True
        if not 0 < s < 1:
            raise ValueError(f"s={s} but should be stricly between 0 and 1")

        pcen_args = {"alpha": alpha, "delta": delta, "r": r, "eps": eps, "s": s}
        # This collects the name of the arguments provided with values below 0 and throws an error
        neg_pcen_args = [k for k in pcen_args if pcen_args[k] <= 0]
        if any(neg_pcen_args):
            length = len(neg_pcen_args)
            s = "" if length == 1 else "s"
            a = "a " if length == 1 else ""
            raise ValueError(f"Parameter{s} {', '.join(neg_pcen_args)} should be {a}positive float{s}.")

        # beware, channel_axis refers to the axis of the audio channels, NOT any convolutional filter
        self.channel_axis = 0 if K.image_data_format() == "channels_first" else -1
        self.time_axis = 3 + self.channel_axis
        self.frequency_axis = 2 + self.channel_axis

        self.absolute_sd = absolute_sd
        if isinstance(sd, dict):
            if len(sd) == 5:
                self.sd = sd
            else:
                self.sd = dict(zip(pcen_args.keys(), [0]*len(pcen_args)))
                self.sd.update(sd)
                print("Using sd = 0 for arguments not provided: {}".format(", ".join([k for k, v in self.sd.items() if v == 0])))
        elif isinstance(sd, list):
            # if sd is list of length 1, replicate it 5 times and use it for all parameters (4/len(sd) = 4)
            # if sd is list of length 5, use it as is (5/len(sd) = 1)
            if len(sd) == 1:
                self.sd = dict(zip(pcen_args.keys(), sd*len(pcen_args)))
            elif len(sd) == 5:
                self.sd = dict(zip(pcen_args.keys(), sd))
            else:
                raise ValueError("sd should be of length 1 or 5, but is of length {}".format(len(sd)))
        else:
            print("Using {} as common value of sd for all parameters".format(sd))
            self.sd = dict(zip(pcen_args.keys(), [sd]*5))
        self.implementation = implementation

        # if following librosa's definition, coerce learn_axis to None so that the parameters are scalars
        self.learn_axis = None if not self.trainable else learn_axis

        # work in log space for the parameter updates, then convert back into linear space for computation
        # ensures non-negativity of the parameters, as well as stability improvement/performance speedup
        self.eps = tf.math.log(eps)
        self.alpha = tf.math.log(alpha)
        self.delta = tf.math.log(delta)
        self.s = tf.math.log(s)
        self.r = tf.math.log(r)

        self.last = last
        self.return_last = return_last

    def _initialiser(self, mean, sd=None, upper=None, lower=None, absolute=True):
        """
        Truncated normal initialiser for generation in log space instead of linear space.
        Essentially it's just a truncnorm.rvs generator from scipy.stats parametrised to replicate Keras' own
        TruncatedNormal initiliaser (i.e. generate normal values around the mean, values more than 2 standard deviations
        away from the mean are redrawn until they fall within 2 standard deviations from the mean), that is then taken
        to log space (this ensures that the parameters will be strictly positive when mapped back to linear space).

        Returned object depends on self.training, being either a fixed tensor for the current backend (False), or an
        initialiser function (True).

        :param mean: float. The mean of the Truncated Normal distribution.
        :param sd: float. The standard deviation of the Truncated Normal distribution
        :param upper: float. Optional boundary on the lower end of the distribution. If generated numbers exceed the
        boundary value, they are redrawn.
        :param lower: float. Optional boundary on the upper end of the distribution. If generated numbers exceed the
        boundary value, they are redrawn.
        :param absolute: bool. If true, use sd as standard deviation, else use sd*mean as standard deviation.
        :return: A tensor (if self.training is False) or an initialiser function (if self.training is True)
        """
        if self.trainable and sd > 0.0:
            mean = exp(mean)
            sd = sd or self.sd
            sd = sd * mean if absolute else sd
            lower = mean - 2 * sd if lower is None else max(lower, mean - 2 * sd)
            upper = mean + 2 * sd if upper is None else min(upper, mean + 2 * sd)

            init = BoundedTruncNorm(a=lower, b=upper, dtype=self.dtype)
        else:
            # mean might be a tensor (of shape (1,)), which the initialiser does not expect
            try:
                mean = mean.numpy()
            except:
                mean = mean
            init = tf.keras.initializers.Constant(value=mean)

        return init

    def build(self, input_shape):
        # Create all the parameters of the PCEN as trainable variables
        training_shape = [1]*len(input_shape)
        if self.learn_axis == "frequency":
            training_shape[self.frequency_axis] = input_shape[self.frequency_axis]
        elif self.learn_axis == "both":
            print("This setting isn't supported currently, defaulting to training only on the frequency axis")
            # training_shape = (input_shape[self.frequency_axis], input_shape[self.time_axis])
            training_shape[self.frequency_axis] = input_shape[self.frequency_axis]

        # all parameters need to be non-negative, so we work in log space for training and take the exponential to
        # calculate the actual PCEN
        # alpha and s also both need to be under 1
        alpha_init = self._initialiser(mean=self.alpha,
                                       sd=self.sd["alpha"],
                                       lower=0,
                                       upper=1,  # gains above 1 make values explode
                                       absolute=self.absolute_sd)
        self.gain = self.add_weight(shape=training_shape if self.sd["alpha"] > 0 else tuple([1]*len(input_shape)),
                                    initializer=alpha_init,
                                    dtype=self.dtype,
                                    trainable=self.trainable,
                                    name="gain")

        delta_init = self._initialiser(mean=self.delta,
                                       sd=self.sd["delta"],
                                       lower=0,
                                       absolute=self.absolute_sd)
        self.bias = self.add_weight(shape=training_shape if self.sd["delta"] > 0 else tuple([1]*len(input_shape)),
                                    initializer=delta_init,
                                    dtype=self.dtype,
                                    trainable=self.trainable,
                                    name="bias")

        r_init = self._initialiser(mean=self.r,
                                   sd=self.sd["r"],
                                   lower=0,
                                   absolute=self.absolute_sd)
        self.power = self.add_weight(shape=training_shape if self.sd["r"] > 0 else tuple([1]*len(input_shape)),
                                     initializer=r_init,
                                     dtype=self.dtype,
                                     trainable=self.trainable,
                                     name="power")

        eps_init = self._initialiser(mean=self.eps,
                                     sd=self.sd["eps"],
                                     lower=0,
                                     absolute=self.absolute_sd)
        self.epsilon = self.add_weight(shape=training_shape if self.sd["eps"] > 0 else tuple([1]*len(input_shape)),
                                       initializer=eps_init,
                                       dtype=self.dtype,
                                       trainable=self.trainable,
                                       name="threshold")

        s_init = self._initialiser(mean=self.s,
                                   sd=self.sd["s"],
                                   lower=0, upper=1,
                                   absolute=self.absolute_sd)
        training_shape.pop(self.time_axis)
        self.m = self.add_weight(shape=training_shape if self.sd["s"] > 0 else tuple([1]*len(input_shape)),
                                 initializer=s_init,
                                 trainable=self.trainable,
                                 name="smoother",
                                 dtype=self.dtype)

        super(PCEN, self).build(input_shape)  # Be sure to call this at the end

    @tf.function
    def call(self, x, **kwargs):
        alpha = tf.math.exp(self.gain)
        eps = tf.math.exp(self.epsilon)
        delta = tf.math.exp(self.bias)
        r = tf.math.exp(self.power)
        s = tf.math.exp(self.m)

        x_split = tf.unstack(x, num=x.shape[self.time_axis], axis=self.time_axis)
        m_frames = []
        if self.last is None:
            last = x_split[0]
        for _ in x_split:
            m_frame = (1 - s) * last + s * _
            self.last = m_frame
            m_frames.append(m_frame)
        m = tf.stack(m_frames, axis=self.time_axis)

        smooth = tf.pow(eps + m, alpha)
        g = tf.math.divide_no_nan(x, smooth)

        pcen_ = tf.pow(g + delta, r) - tf.pow(delta, r)

        # # apply mask
        # if mask is not None:
        #     mask = tf.expand_dims(tf.cast(mask, dtype=pcen_.dtype), axis=len(mask.shape))
        #     pcen_ = pcen_ * mask

        if not self.return_last:
            self.last = None

        return pcen_

    def compute_output_shape(self, input_shape):
        return input_shape

    def compute_mask(self, inputs, mask=None):
        return mask

    def get_config(self):
        config = super(PCEN, self).get_config()
        config.update({
            "sd": self.sd,
            "absolute_sd": self.absolute_sd,
            "learn_axis": self.learn_axis,
            "implementation": self.implementation,
            "return_last": self.return_last,
            "last": self.last,
        })
        return config


# class RnnPcenCell(tf.keras.layers.Layer):
#     def __init__(self,
#                  alpha=0.98,
#                  delta=2.0,
#                  r=0.5,
#                  eps=1E-6,
#                  s=0.025,
#                  absolute_sd=True,
#                  learn_axis=None,
#                  sd=1.0,
#                  return_last=False,
#                  last=None,
#                  name="PCEN",
#                  **kwargs):
#         """
#         Implementation of PCEN as a trainable layer for Keras/Tensorflow. Trainability is possible at different levels,
#         from single value parameters, to one value per frequency band, or even per spectrogram
#         coefficient.
#
#         :param alpha: The gain parameter.
#         :param delta: The bias parameter.
#         :param r: The power parameter.
#         :param eps: float. The soft threshold of the DRC part of the procedure.
#         :param s: float. The smoothing coefficient (basically how fast the IIR filter will filter out static noise).
#         Strictly between 0 and 1. Defaults to 0.025
#         :param training: bool. Whether to allow the parameters to be learned (if True) or not. Defaults to True.
#         :param learn_axis: Should be one of None, int or list. If None, all the parameters are single values.
#         If int, parameters will be vectors of size input_shape[learn_axis]. If list, parameters will be arrays of shape
#         input_shape[*learn_axis]. Defaults to None.
#         :param sd: float, list or dict. The standard deviation(s) to apply to the initialisers. Ignored if
#         training = False or learn_axis = None. If provided as a dict, parameters not in the keys of the dict will be
#         generated as if given sd[paramater] = 0. If provided as a list, throws error if length is not 1 or 5. If a list
#         of length 5 is given, the respective sd values are applied in the order [alpha, delta, r, eps, s].
#         :param absolute_sd: bool. Whether to use sd "as is" (resulting in all parameters being generated with the
#         provided sd exactly), or multiply by the value of the parameter (resulting in larger parameters having larger
#         sd). For best results, if True, sd should not be set higher than 0.5
#         (which will then signify that the parameters will be initialised via a truncated normal within
#         ]0; 2*value[ where value is the value of the respective parameter (eps, s, alpha, delta, r). For s and alpha however,
#         2*value is bounded by 1.
#         :param return_last: bool. Passed to the pcen function. If true, returns the last frame of the smoother, allowing
#         streaming transformations
#         :param kwargs:
#
#         :return: PCEN-adjusted spectrogram.
#
#         References:
#         [1] Y. Wang, P. Getreuer, T. Hughes, R. F. Lyon, and R. A. Saurous,  “Trainable frontend for robust and
#         far-field keyword spotting,” in Proc. IEEE ICASSP, 2017.
#         """
#         super().__init__(name=name, **kwargs)
#
#         self.supports_masking = True
#
#         self.state_size = (800, None)
#         self.output_size = (800, None)
#
#         if not 0 < s < 1:
#             raise ValueError(f"s={s} but should be stricly between 0 and 1")
#
#         self.pcen_args = {"alpha": alpha, "delta": delta, "r": r, "eps": eps, "s": s}
#
#         # Throw an error if any parameter is below zero
#         neg_pcen_args = [k for k, v in self.pcen_args.items() if v <= 0]
#         if any(neg_pcen_args):
#             length = len(neg_pcen_args)
#             s = "" if length == 1 else "s"
#             a = "a " if length == 1 else ""
#             raise ValueError(f"Parameter{s} {', '.join(neg_pcen_args)} should be {a}positive float{s}.")
#         else:
#             # work in log space for the parameter updates, then convert back into linear space for computation
#             # ensures non-negativity of the parameters, as well as stability improvement/performance speedup
#             self.pcen_args = {k: tf.math.log(v) for k, v in self.pcen_args.items()}
#
#         # the channel axis should be functionally identical to the convolutional filter axis
#         self.channel_axis = 1 if K.image_data_format() == "channels_first" else -1
#
#         self.absolute_sd = absolute_sd
#         if isinstance(sd, dict) and self.sd.keys() == self.pcen_args.keys():
#             self.sd = sd
#         elif isinstance(sd, list) and len(sd) == 5:
#             self.sd = dict(zip(self.pcen_args.keys(), sd))
#         elif isinstance(sd, list) and len(sd) == 1:
#             self.sd = dict(zip(self.pcen_args.keys(), sd * len(self.pcen_args)))
#             print("Using {} as common value of sd for all parameters".format(sd))
#         elif isinstance(sd, (float, int)):
#             self.sd = dict(zip(self.pcen_args.keys(), [sd] * len(self.pcen_args)))
#             print("Using {} as common value of sd for all parameters".format(sd))
#         else:
#             raise ValueError("sd should be of length 1 or 5, but is of length {}".format(len(sd)))
#
#         self.learn_axis = learn_axis
#
#         self.last = last
#         self.return_last = return_last
#
#     def _initialiser(self, mean, sd=None, upper=None, lower=None, absolute=True):
#         """
#         Truncated normal initialiser tweaked for generation in log space instead of linear space.
#         Essentially it's just a truncnorm.rvs generator from scipy.stats parametrised to replicate Keras' own
#         TruncatedNormal initiliaser (i.e. generate normal values around the mean, values more than 2 standard deviations
#         away from the mean are redrawn until they fall within 2 standard deviations from the mean), that is then taken
#         to log space (this ensures that the parameters will be strictly positive when mapped back to linear space).
#
#         Returned object depends on self.training, being either a fixed tensor for the current backend (False), or an
#         initialiser function (True).
#
#         :param mean: float. The mean of the Truncated Normal distribution.
#         :param sd: float. The standard deviation of the Truncated Normal distribution
#         :param upper: float. Optional boundary on the lower end of the distribution. If generated numbers exceed the
#         boundary value, they are redrawn.
#         :param lower: float. Optional boundary on the upper end of the distribution. If generated numbers exceed the
#         boundary value, they are redrawn.
#         :param absolute: bool. If true, use sd as standard deviation, else use sd*mean as standard deviation.
#         :return: A tensor (if self.training is False) or an initialiser function (if self.training is True)
#         """
#         if self.trainable and sd > 0.:
#             mean = exp(mean)
#             sd = sd if absolute else sd * mean
#             lower = mean - 2 * sd if lower is None else max(lower, mean - 2 * sd)
#             upper = mean + 2 * sd if upper is None else min(upper, mean + 2 * sd)
#
#             init = BoundedTruncNorm(a=lower, b=upper, dtype=self.dtype)
#         else:
#             # mean might be a tensor (of shape (1,)), which the initialiser does not expect
#             if "numpy" in dir(mean):
#                 init = tf.keras.initializers.Constant(value=mean.numpy())
#             else:
#                 init = tf.keras.initializers.Constant(value=mean)
#
#         return init
#
#     def build(self, input_shape):
#         # Create all the parameters of the PCEN as trainable variables
#         training_shape = [1] * len(input_shape)
#         if isinstance(self.learn_axis, int):
#             training_shape[self.learn_axis] = input_shape[self.learn_axis]
#         elif isinstance(self.learn_axis, list):
#             for _ in self.learn_axis:
#                 training_shape[_] = input_shape[_]
#
#         # all parameters need to be non-negative, so we work in log space for training and take the exponential to
#         # calculate the actual PCEN
#         # alpha and s also both need to be under 1
#         self.pcen_variables = {k: self.add_weight(
#             shape=training_shape if self.sd[k] > 0. else tuple([1] * len(input_shape)),
#             initializer=self._initialiser(
#                 mean=self.pcen_args[k],
#                 sd=self.sd[k],
#                 lower=0,
#                 upper=1 if k in ['alpha', 's'] else None,
#                 absolute=self.absolute_sd
#             ),
#             dtype=self.dtype,
#             trainable=self.trainable,
#             name=k,
#         ) for k, v in self.pcen_args.items()}
#
#         super().build(input_shape)
#
#     def call(self, input, states):
#         variables = {k: tf.math.exp(v) for k, v in self.pcen_variables.items()}
#         new_state = variables['s'] * input + (1 - variables['s']) * states[-1]
#
#         out = tf.math.divide_no_nan(input, tf.pow(variables['eps'] + new_state, variables['alpha']))
#         out = tf.pow(out + variables['delta'], variables['r']) - tf.pow(variables['delta'], variables['r'])
#
#         return out, states + new_state
#
#
# def pcen_layer(eps=1E-6,
#                s=0.025,
#                alpha=0.98,
#                delta=2.0,
#                r=0.5,
#                absolute_sd=True,
#                learn_axis=None,
#                sd=1.0,
#                trainable=True,
#                return_last=False,
#                dynamic=False,
#                dtype=None,
#                last=None):
#     cell = RnnPcenCell(alpha=alpha,
#                        s=s,
#                        delta=delta,
#                        r=r,
#                        eps=eps,
#                        absolute_sd=absolute_sd,
#                        sd=sd,
#                        learn_axis=learn_axis,
#                        trainable=trainable,
#                        return_last=return_last,
#                        last=last)
#     return tf.keras.layers.RNN(cell=cell, return_sequences=True)


if __name__ == "__main__":
    # Import a toy file from the accompanying test_audio directory
    dirname = os.path.dirname(__file__)
    path = "../test_audio"
    files = sorted([_ for _ in os.listdir(os.path.join(dirname, path)) if "wav" in _ and not _.startswith(".")])
    filename = os.path.join(dirname, path, files[0])
    print(filename)
    y, sr = sf.read(file=filename, always_2d=True)
    y = tf.transpose(y)
    S = tf.signal.stft(y, frame_length=1200, fft_length=1200, frame_step=300, window_fn=tf.signal.hamming_window)
    filt = librosa.filters.mel(sr=sr, n_fft=1200, n_mels=128)
    S = tf.abs(tf.cast(S, dtype=tf.float32))**2
    S = tf.matmul(S, tf.transpose(filt))
    S = tf.transpose(S)

    # Some parameters for the PCEN (both as the layer and for comparison with librosa below
    gain = .99
    bias = 1.
    power = .3
    eps = 1E-9
    smooth = .02
    sd = 0.

    # Create a toy model with just a PCEN layer to test output
    inputs = tf.keras.layers.Input(shape=S.shape)
    x = PCEN(alpha=gain, delta=bias, r=power, s=smooth, eps=eps,
         sd=sd, absolute_sd=True,
         trainable=True, learn_axis="frequency",
         implementation=None, dynamic=True, dtype="float32")(inputs)
    test_model_none = tf.keras.Model(inputs=inputs, outputs=x)
    for l in test_model_none.layers:
        print(l.weights)

    # input to model.predict should be a 4-D tensor of shape (batch_size, channels, n_mels, width)
    # so for testing I have to add a first dimension of size 1 to simulate a batch
    # !!! note to self: tf.expand_dims is used to simulate the BATCH axis, not the channel axis
    P_custom = test_model_none.predict(x=tf.expand_dims(S, axis=0))

    # plots to check out the weight values at initialisation
    weights_custom = test_model_none.layers[-1].weights
    plt.figure()
    for i, w in enumerate(weights_custom):
        plt.subplot(len(weights_custom), 1, i + 1)
        plt.title(w.name)
        plt.plot(tf.math.exp(tf.squeeze(w)).numpy())
    plt.show()

    # then remove the first dimension
    P_custom = P_custom.squeeze(axis=0)

    # Compare to power_to_db and librosa.pcen
    print(tf.reduce_max(S))
    P_db = librosa.power_to_db(S, ref=np.max)
    P_librosa = librosa.pcen(S.numpy(), sr=sr, hop_length=300, eps=eps, gain=gain, bias=bias, b=smooth, power=power,
                             axis=1)  # axis defaults to -1, which does NOT work because we have a multi-channel output
    print("Types of the results: librosa: {}, layer: {}".format(P_librosa.dtype, P_custom.dtype))
    print("Minimum values: librosa: {}, layer: {}".format(np.min(P_librosa), np.min(P_custom)))
    print("Maximum absolute difference: {}".format(np.max(np.abs(P_librosa - P_custom))))
    print("Total absolute difference: {}".format(np.sum(np.abs(P_librosa - P_custom))))

    plots = [tf.unstack(S, axis=-1),
             tf.unstack(P_custom, axis=-1),
             tf.unstack(P_db, axis=-1),
             tf.unstack(P_librosa, axis=-1),
             tf.unstack(P_custom - P_librosa, axis=-1)]
    titles = ["Original", "Layer", "dB", "Librosa", "Layer - librosa"]

    plt.figure()
    for i, plot in enumerate(plots):
        # setting common title to two plots??
        plt.subplot(len(plots), 2, 2*i+1)
        disp.specshow(plot[0].numpy(), sr=sr, hop_length=300, fmax=sr / 2, y_axis="mel")
        plt.colorbar()
        plt.subplot(len(plots), 2, 2*(i+1))
        disp.specshow(plot[1].numpy(), sr=sr, hop_length=300, fmax=sr / 2, y_axis="mel")
        plt.colorbar()
    plt.tight_layout()
    plt.show()
