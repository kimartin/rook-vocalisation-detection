"""
Mostly random functions that are useful somewhere else in the project. No particular theme.
"""

import numpy as np
import tensorflow as tf
from functools import wraps, reduce
from time import time
import itertools
import pandas as pd
import os
from itertools import chain
from scipy import ndimage
import scipy.signal as sgn
from suffix_trees import STree


def timing(f, times=1000):
    """
    Decorate functions with this for easy timing estimations

    :param f: a callable object (/function) or list therefor
    :param times: how many times to run f and aggregate the results
    """

    @wraps(f)
    def wrap(*args, **kw):
        t = []
        for _ in range(times):
            ts = time()
            result = f(*args, **kw)
            te = time()
            t.append(te - ts)
        quartiles = np.percentile(t, [25, 50, 75])
        print(
            f"{f.__name__}: min {min(t)} - lq {quartiles[0]} - avg {sum(t) / times} - med {quartiles[1]} - uq {quartiles[2]} - max {max(t)}")

    return wrap


def assert_matched(x, y):
    """
    Checks if two lists x and y can be matched.
    A match is defined as an item in both lists being identical, except for patterns common to all items in a list.

    For instance:
        x = ["test_label1", "test_label2", "test_label3"]
        y = ["test_file1", "test_file2", "test_file3"]

        "label" is common to all items in x and file to all items in y, so they will be removed
        then the lists are identical:
        assert_matched(x, y) = True

    For instance of non-matches:
        x = ["test_label1", "test_label2"]
        y = ["test_file1", "test_file2", "test_file3"]
        'test_file3' has no match in x, so the function will return a list containing it:
        assert_match(x, y) = ['test_file3']

    :param x: list
    :param y: list
    :return: either True (if all elements in either list have a match in the other), or a list of items from both x and
    y that could not be matched
    """
    originals = x + y
    while True:
        # erase patterns common to all items in each list
        common_x = STree.STree(x).lcs()
        common_y = STree.STree(y).lcs()

        # keep iterating
        if len(common_x) < 2 or len(common_y) < 2:
            break

        x = [s.replace(common_x, '') for s in x]
        y = [s.replace(common_y, '') for s in y]

    # finds the complementary of the intersection of the two
    unmatched = set(x) ^ set(y)
    if len(unmatched) == 0:
        # all files from one list matched with one from the other
        return True
    else:
        # return the orignal names in either x or y that could not be matched
        return [_ for _ in originals if any([u in _ for u in unmatched])]


def unstack(a, axis=0):
    """
    Replicates tensorflow.unstack in numpy
    """
    return list(np.moveaxis(a, axis, 0))


def broadcastable(a, b):
    """
    Takes two arrays a and b and returns True if they are broadcastable, otherwise False.

    Broadcastable: each axis of a and b should of same size or 1
    """

    return all((m == n) or (m == 1) or (n == 1) for m, n in zip(a.shape[::-1], b.shape[::-1]))


def flatten(l):
    """
    Flattens a list of lists into a list.
    """
    return list(chain.from_iterable(l))


def round_robin(*iterables):
    """
    Implements a Round Robin distribution on a list of iterables.
    For 2 iterables, this corresponds to alternating concatenation
    Note that the output will only go as far as the shortest iterable.
    """
    return [i for items in zip(*iterables) for i in items]


def all_combinations(iterable, max=None):
    """
    Get all combinations of elements from an iterable.

    :param iterable: Iterable containing the elements to combine
    :param max: None or int. Maximum length of the combinations. If None, all combinations will be returned, up to and
    including the iterable itself.
    :return: list of iterables, each element
    """
    if max is None:
        max = len(iterable)
    comb = [itertools.combinations(iterable, i+1) for i in range(max)]
    return flatten(comb)


def top_k(iterable, k):
    return sorted(iterable)[-k:]


def arg_top_k(iterable, k, return_values=False):
    values = top_k(iterable, k)
    if return_values:
        ind, val = zip(*[(i, v) for i, v in enumerate(iterable) if v in values])
        return ind, val
    else:
        return [i for i, v in enumerate(iterable) if v in values]


def standard(a, axis=2):
    """
    Center-scales (mean 0, standard deviation 1) along axis or axes.
    :param a: array or tensor
    :param axis: int or list
    """
    return tf.math.divide_no_nan(a - tf.reduce_mean(a, axis=axis, keepdims=True),
                                 tf.math.reduce_std(a, axis=axis, keepdims=True))


def overlap(x, y, inclusive=True):
    """
    Test if a time range x overlaps with time range y
     :param x: tuple of the form (start, end)
     :param y: tuple of the form (start, end)
     :param inclusive: whether or not to consider edges of the ranges for overlaps
     :return: boolean, True if there is overlap and False otherwise
        """
    start_x, end_x = x
    start_y, end_y = y
    if inclusive:
        return start_x <= end_y and end_x >= start_y
    else:
        return start_x < end_y and end_x > start_y


def product_dict(**kwargs):
    """
    Gets all combinations from the given named arguments. Each combination is returned as dict with keys given by the
    names of the arguments, and values as one value from each of the lists in the arguments

    Exemple:
    list(product_dict(a = ['A', 'B'], b = [3, 4]))
    >>> [{'a': 'A', 'b': 3}, {'a': 'A', 'b': 4}, {'a': 'B', 'b': 3}, {'a': 'B', 'b': 4}]

    :param kwargs: arbitrary named arguments. Each element should be a list, even if only of length 1. The elements of
    each list can be of any number of different types.
    :return: generator, containing all possible combinations of the dicts in kwargs
    """
    keys = kwargs.keys()
    vals = kwargs.values()
    for instance in itertools.product(*vals):
        yield dict(zip(keys, instance))


def listOfDict_to_dictOfList(ld):
    """
    Hacky solution that converts a list of dicts to a dict of lists.
    All keys present in any of the input list of dicts are preserved.
    """
    return {k: [dic[k] for dic in ld if k in dic] for k in reduce(set.union, [set(ld.keys()) for ld in ld])}


def dictOfList_to_listOfDict(dl):
    """
    Returns a list of dicts from a dict of lists.
    Each dict in the output list has the same keys as dl and one value from the original lists.

    All the lists in dl.values() should have the same length, otherwise the output will be truncated to the shortest length.
    """
    return [dict(zip(dl, t)) for t in zip(*dl.values())]


def is_list(p):
    """Self-explanatory, really"""
    return isinstance(p, list)


def same_structure(a, b):
    """
    Recursively checks if a and b are of the same structure (e.g. if a and b are nested lists, checks each level in
    turn).
    """
    if not is_list(a) and not is_list(b):
        return True
    elif (is_list(a) and is_list(b)) and (len(a) == len(b)):
        return all(map(same_structure, a, b))
    return False


def find_overlaps(x, start="Start", end="End", separated_by=0.):
    """
    Finds the overlaps in ranges defined by the columns start and end of pd.DataFrame object x.
    This includes overlaps that include multiple rows at once.

    separated_by allows to extend or shrink ranges so that overlaps will be considered either more broadly or more
    stringently.
    separated_by > 0: overlaps will be considered even if the actual values are separated by up to its value
    separated_by < 0: overlaps will be considered only if the actual values really overlap by more than its value

    Example:
    x = pd.DataFrame({"Start": [0, 10, 20, 30, 40, 50, 60],
                      "End":   [9, 21, 28, 53, 42, 55, 70]
    find_overlaps(x)
            Start  End  group
                0    9      1
               10   21      2
               20   28      2
               30   53      3
               40   42      3
               50   55      3
               60   70      4

    :param x: a pd.DataFrame object
    :param start: str. Gives the column that defines the start of the range.
    :param end: str. Gives the column that defines the end of the range.
    :param separated_by: float. Value to add to consider overlaps (see above). Defaults to 0.
    :return: x with addition "group" column. The values in this column correspond to overlaps.
    """
    df = x.copy(deep=True)

    # Flatten start and end into a single column
    startdf = pd.DataFrame({'time': x[start] - separated_by / 2, 'what': 1})
    enddf = pd.DataFrame({'time': x[end] + separated_by / 2, 'what': -1})
    mergdf = pd.concat([startdf, enddf]).sort_values('time')

    # Count intervals currently going on over the entire column
    mergdf['running'] = mergdf['what'].cumsum()

    # Finds the starts where there is exactly one interval
    mergdf['newwin'] = mergdf['running'].eq(1) & mergdf['what'].eq(1)

    # Group
    mergdf['group'] = mergdf['newwin'].cumsum()

    # Adds the group back to the original data
    df['group'] = mergdf['group'].loc[mergdf['what'].eq(1)]
    return df


def merge_overlaps(x, start="Start", end="End", by=None, separated_by=0.):
    """
    Merges the overlaps found by find_overlaps.
    :param x, start, end: passed to find_overlaps
    :param by: None or list of columns of x.  Intended to define groups, where overlaps will be ignored. Currently
    ignored as it does not work as intended.
    :return:
    """
    # Merges the overlaps found by find_overlaps
    columns = [start, end]

    x = find_overlaps(x, start=start, end=end, separated_by=separated_by)

    dict_agg = {start: 'min', end: 'max'}

    if by is not None:
        dict_agg = dict(**{_: 'first' for _ in by}, **dict_agg)
        columns += by

    res = x.groupby(["group"]).agg(dict_agg)

    res = res.reset_index()[columns]
    return res


# Take the last saved checkpoint
def get_models(models, get_best_only=None, mode="max"):
    """
    :param models: str or list. Path of models to get
    :param get_best_only: If not None: column(s) to take into account for the best model.
    If string, only take that column. If list, take the column with the name closest to the END of the list.
    The model returned will be the best model according to that column. If None, take the last epoch.
    :param mode: str. One of "max" or "min" depending on whether higher (max) or lower (min) values are better.

    :return: str or list. The path(s) to the desired model(s), taken at the epoch specified by get_best_only.
    """
    if mode not in ['max', 'min']:
        raise ValueError(f"mode should be one of 'max' (if better performance yields higher values of the metric)"
                         f"or 'min' (if better performance yields lower values), but was {mode}")

    single_model = False
    if not isinstance(models, list):
        single_model = True
        models = [models]
    if get_best_only is not None:
        best = [pd.read_csv(os.path.join(m, "training.csv")) for m in models]
        cols = [[c for c in get_best_only if c in b.columns][-1] for b in best]
        if mode == "max":
            best = [b['epoch'].iloc[b[c].idxmax() + 1] for b, c in zip(best, cols)]
        else:
            best = [b['epoch'].iloc[b[c].idxmin() + 1] for b, c in zip(best, cols)]
        models = [f"{p}/checkpoints/ep{b:02d}.tf" for p, b in zip(models, best)]
    else:
        models = [f"{p}/checkpoints/{os.listdir(os.path.join(p, 'checkpoints'))[-1]}" for p in models]
    if single_model:
        models = models[0]
    return models


def restrict_kwargs(d, func):
    """
    Gets only the subset of dict d that is within the arguments of func.
    For obvious reasons, only works with named arguments
    """
    new_dict = {k: d[k] for k in d.keys() if k in func.__code__.co_varnames}
    return new_dict


def norm(x, axis=None):
    return (x - tf.reduce_min(x, axis=axis, keepdims=True)) / (
                tf.reduce_max(x, axis=axis, keepdims=True) - tf.reduce_min(x, axis=axis, keepdims=True))

def absmaxND(a, axis=None, keepdims=True):
    """
    Functionally equivalent but faster way to compute np.abs(a).max(axis=axis)
    """
    amax = a.max(axis, keepdims=keepdims)
    amin = a.min(axis, keepdims=keepdims)
    return np.where(-amin > amax, amin, amax)


def preemphasis(x, pre, normalise=True):
    """
    Applies a pre-emphasis filter to array x.

    Note that x is assumed to be of the shape [..., samples], similar to how Tensorflow works with sound samples for STFT

    Arguments:
        - x: array-like. Should be of shape [..., samples]
        - pre: float. pre-emphasis coefficient
        - normalise: bool. If True (default), the returned array is normalised to the same range as the original x.
            Note that this normalisation is designed to avoid adding an offset to the returned array, by first operating
            on the absolute values then reapplying the sign of the array at each sample.
            Finally, the FIRST sample is set to the mean of each instance to avoid offsets.

    Returns:
        - the pre-emphasised array, of the same shape and type as x
    """
    if normalise:
        max_ = absmaxND(x, axis=-1, keepdims=True)
        x[..., 1:] -= pre * x[..., :-1]

        with np.errstate(divide='ignore', invalid='ignore'):
            x /= max_
            x[~np.isfinite(x)] = 0.
    else:
        x[..., 1:] -= pre*x[..., :-1]

    return x


# Helper functions
def onsets_offsets(arr):
    elements, nelements = ndimage.label(arr)
    if nelements == 0:
        return np.array([[0], [0]])
    onsets, offsets = np.array(
        [
            np.where(elements == element)[0][np.array([0, -1])] + np.array([0, 1])
            for element in np.unique(elements)
            if element != 0
        ]
    ).T
    return np.array([onsets, offsets])


def top_k_values(array, k):
    idx = np.argsort(array)[-k:][::-1]
    arr = np.zeros_like(array)
    arr[idx] = 1.
    return arr


def top_k(array, k, axis=-1):
    return np.apply_along_axis(top_k_values, axis, array, k)

def top_1(array, axis=-1):
    x = np.zeros_like(array)
    idx = np.max(array, axis=axis, keepdims=True)
    x[array == idx] = 1.
    return x


def highest_above_second(arr, threshold=2, axis=-1):
    sorted_arr = np.sort(arr, axis=axis)
    slc = [slice(None)] * arr.ndim
    slc[axis] = -1
    highest = sorted_arr[tuple(slc)]
    slc[axis] = -2
    second = sorted_arr[tuple(slc)]

    diff = 1 - second / highest
    return diff > 1./threshold


def create_labels_from_prediction(pred,
                                  threshold=None,
                                  min_frames=5,
                                  min_to_keep=.5,
                                  ):
    """
    Function to take the output predictions from a model (either directly or after loading from a .pickle file.

    It essentially simply gets the runs above the provided threshold argument, with some fancy arguments to remove:
        - short detections (min_frames is the lower limit in length for detections to be accepted in the labels)
        - low detection (min_to_keep is the value a detection must reach at least once to be accepted)
    :return: tuple of onsets and offsets. Each element is either an array or a list of arrays depending on pred.ndim > 1
    """
    if threshold is not None:
        if not isinstance(threshold, (float, int, list, np.ndarray)) and not callable(threshold):
            raise ValueError('Please provide threshold as one of: scalar/callable or list/array thereof.'
                             'Additionally, if you wish to use multiple thresholds, provide exactly as many as pred.shape[-1]')

        if hasattr(threshold, '__len__') and len(threshold) == 1:
            threshold = threshold[0]
        elif hasattr(threshold, '__len__') and pred.ndim > 1 and len(threshold) != pred.shape[-1]:
            raise ValueError(f'Provided {len(threshold)} but only {pred.shape[-1]} classes exist in pred. '
                             f'Please provide as many thresholds as there are classes in pred')

    if pred.ndim > 1:
        mask = pred.T
        if hasattr(threshold, '__len__'):
            mask = [p > thr if not callable(thr) else callable(thr) for p, thr in zip(mask, threshold)]
        elif threshold is not None:
            mask = [p > threshold if not callable(threshold) else threshold(p) for p in mask]
        onsets, offsets = zip(*[onsets_offsets(_) for _ in mask])

        # remove short detections
        long_detection = [np.array([f - o > min_frames for o, f in zip(on, off)]).astype('bool') for on, off in zip(onsets, offsets)]
        onsets = [on[long] for on, long in zip(onsets, long_detection)]
        offsets = [off[long] for off, long in zip(offsets, long_detection)]

        # remove low detections
        high_detection = [np.array([np.max(pred[o:f]) > min_to_keep for o, f in zip(on, off)]).astype('bool') for on, off in zip(onsets, offsets)]
        onsets = [on[hi] for on, hi in zip(onsets, high_detection)]
        offsets = [off[hi] for off, hi in zip(offsets, high_detection)]

    else:
        mask = pred
        if threshold is not None:
            mask = pred > threshold if not callable(threshold) else threshold(pred)
        onsets, offsets = onsets_offsets(mask)

        # remove short detections
        long_detection = np.array([off - on > min_frames for on, off in zip(onsets, offsets)]).astype('bool')
        onsets = onsets[long_detection]
        offsets = offsets[long_detection]

        # remove low detections
        high_detection = np.array([np.max(pred[o:f]) > min_to_keep for o, f in zip(onsets, offsets)]).astype('bool')
        onsets = onsets[high_detection]
        offsets = offsets[high_detection]

    return onsets, offsets


# Main function
def create_labels_from_network(pred,
                               threshold=0.5,
                               min_to_keep=0.,
                               frame_dur=1./80.,
                               names=None,
                               min_frames=5,
                               keep_vocs=True,
                               use_intervals=True,
                               refine=None
                               ):

    if not isinstance(threshold, dict):
        threshold = {k: threshold for k in pred.keys()}

    if not isinstance(min_to_keep, dict):
        min_to_keep = {k: min_to_keep for k in pred.keys()}

    pred = {k: np.squeeze(np.reshape(v, newshape=(-1, v.shape[-1]))) for k, v in pred.items()}

    det_on, det_off = create_labels_from_prediction(
        pred=pred['detection'],
        threshold=threshold['detection'],
        min_to_keep=min_to_keep['detection'],
        min_frames=min_frames
    )
    if refine is not None:
        pass

    if names is None:
        if use_intervals:
            df = pd.DataFrame({
                'V1': [det_on],
                'V2': [det_off],
                'V3': [''],
            })
            df
        else:
            df = pd.DataFrame({
                    'V1': [det_on, det_off],
                    'V3': ['voc', 'voc-end']
                })

    else:
        from time import time
        # suppress predictions outside of detected vocalisations
        pred_id = np.zeros_like(pred['identification'])

        for on, off in zip(det_on, det_off):
            pred_id[on:off] = pred['identification'][on:off]

        # Restrict to top predictions AND predictions sufficiently higher than the next lower
        pred_id *= top_1(pred['identification'], axis=-1)
        if callable(threshold['identification']):
            pred_id *= np.expand_dims(threshold['identification'](pred['identification']), axis=-1)
        else:
            pred_id *= np.max(pred['identification'] > threshold['identification'], axis=-1, keepdims=True)

        id_ons, id_offs = create_labels_from_prediction(
            pred=pred_id,
            threshold=.05,
            min_to_keep=min_to_keep['identification'],
            min_frames=min_frames,
        )

        if use_intervals:
            df = pd.DataFrame(
                {
                    'V1': id_ons,
                    'V2': id_offs,
                    'V3': names
                }
            )
            if keep_vocs:
                df = pd.concat(
                    [
                        df,
                        pd.DataFrame({
                            'V1': [det_on],
                            'V2': [det_off],
                            'V3': [''],
                        }),
                    ]
                )
        else:
            df = pd.DataFrame(
                {
                    'V1': id_ons + id_offs,
                    'V3': names + [f'{n}-end' for n in names],
                }
            )
            if keep_vocs:
                df = pd.concat([
                    df,
                    pd.DataFrame({
                        'V1': [det_on, det_off],
                        'V3': ['voc', 'voc-end']
                    })
                ])
            df.insert(loc=1, value=df['V1'], column='V2')

    df = df.set_index(['V3']).apply(pd.Series.explode).sort_values(by='V1')
    df = df.reset_index()
    df = df.sort_values(by='V1')
    df = df.drop_duplicates(subset=['V1', 'V2'], keep=False)
    df[['V1', 'V2']] *= frame_dur
    df = df.reindex(sorted(df.columns), axis=1)

    return df