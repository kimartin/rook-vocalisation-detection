from scipy.signal import correlate, correlation_lags
import soundfile as sf
import numpy as np
import re
# import tensorflow as tf
import os
import pandas as pd
from random import random
from joblib import Parallel, delayed
from math import ceil


def distance_weighted_average(x, remove_outlier=0):
    """
    Distance-weighted average of vector x.
    """
    weights = np.zeros(shape=(len(x), len(x)))

    for i in range(1, len(x)):
        for j in range(i):
            weights[i, j] = x[i] - x[j]
            weights[j, i] = weights[i, j]

    if np.sum(np.abs(weights)) == 0.:
        return np.mean(x)
    else:
        with np.errstate(divide='ignore', invalid='ignore'):
            weights = (len(x) - 1) / np.sum(np.abs(weights), axis=0)
            if remove_outlier > 0:
                mask = sorted(np.argsort(weights)[:remove_outlier])
                weights[mask] = 0.
            out = np.nan_to_num(np.sum(weights * x) / np.sum(weights))
        return out


def flatten(l):
    """
    Flattens a list of lists into a list, respecting the order
    """
    return [i for item in l for i in item]


def find_overlaps(df, start="Start", end="End", separated_by=0., name="group"):
    """
    From a pd.DataFrame object, given two columns start and end which defines intervals of values, returns the object with
    additional column "group". Rows with the same value in "group" overlap.

    The separated_by argument allows controlling what is considered an overlap:
        - 0: groups only rows that actually overlap
        - > 0: groups rows so long as they are separated by less than its value
        - < 0: only groups rows that overlap by more than its value (use cases are somewhat unclear, but the
        implementation makes no assumption that separated_by >= 0.)

    The algorithm works by essentially sorting all the start and end values togther, and then go through and count how
    many intervals are currently open at each index. Groups are defined by windows where exactly 1 interval is open,
    starting at the start value of this interval.
    """
    # df = x.copy(deep=True)
    startdf = pd.DataFrame({'time': df[start] - separated_by / 2, 'what': 1})
    enddf = pd.DataFrame({'time': df[end] + separated_by / 2, 'what': -1})
    mergdf = pd.concat([startdf, enddf]).sort_values('time')
    mergdf['running'] = mergdf['what'].cumsum()
    mergdf['newwin'] = mergdf['running'].eq(1) & mergdf['what'].eq(1)
    mergdf['group'] = mergdf['newwin'].cumsum()
    df[name] = mergdf['group'].loc[mergdf['what'].eq(1)] - 1
    return df


def convert_str_time_to_seconds(x, sep=None):
    """
    Convert time in hour-minute-second format to seconds
    :param x: str. Time to convert. Assumed to be of the format 'HH{sep}MM{sep}SS'.
    :param sep: None or str. Separator argument.
    :return: float. Time in seconds (with the origin at midnight).
    """
    if sep is None:
        h, m, s = x[:2], x[2:4], x[4:]
    else:
        h, m, s = x.split(sep=sep)
    return 3600 * float(h) + 60 * float(m) + float(s)


def hms(x, sep=None):
    """
    Convert time in seconds to the hour-minute-second format, optionally separated by a character
    given in argument sep.
    :param x: float. Time in seconds
    :param sep: None or str. Separator argument (e.g. ':' for 'HH:MM:SS' format).
    :return: str. Time in hour-minute-second format.
    """
    h = f'{int(x // 3600):02d}'
    m = f'{int((x % 3600) // 60):02d}'
    s = f'{int(x % 60):02d}'
    if sep is None:
        sep = ''
    return sep.join([h, m, s])


def cluster_wav_files(x,
                      time_pattern,
                      impossible_overlaps=None):
    # Get starting times based on the names of the files
    times = [re.search(pattern=time_pattern, string=_).group(0) for _ in x]

    # Convert the starting times to seconds
    starts = np.array([convert_str_time_to_seconds(_) for _ in times])
    # Deduce the ending times in seconds
    ends = starts + np.array([sf.info(_).duration for _ in x])

    # Find the groups of files that overlap one another
    x = find_overlaps(
        pd.DataFrame({
            'file': x,
            'start': [hms(_, sep=':') for _ in starts],
            'end': [hms(_, sep=':') for _ in ends],
            'start_seconds': starts,
            'end_seconds': ends
        }),
        start='start_seconds',
        end='end_seconds',
        name='group',
    )

    # Compute the starting (resp. ending) time for each group the first starting (resp. last ending) time of the files in the group
    x['group_start'] = x.groupby(['group'])['start_seconds'].transform('min').apply(hms, sep=':')
    x['group_end'] = x.groupby(['group'])['end_seconds'].transform('max').apply(hms, sep=':')

    # If a group contains multiple files that match a single pattern in impossible_overlaps, the group is staggered
    # and so cannot be overlapped as easily
    staggered = {i: max([sum([pat in f for f in g['file']]) for pat in impossible_overlaps]) > 1 for i, g in x.groupby(['group'])}
    x['staggered'] = [staggered[_] for _ in x['group']]

    # Sort by starting times
    x = x.sort_values(['start_seconds']).reset_index(drop=True)

    # Select columns
    dat = x[['file', 'group', 'staggered', 'group_start', 'group_end', 'start', 'end']]

    return dat


def ccf(x, max_offset=np.inf):
    """
    Compute the cross_correlation lags between elements of x.
    Concretely: returns the lags between x[0] and x[1], x[2], ..., x[-1] corresponding to the highest absolute correlation.
    Each lag is therefore relative to x[0]

    :param x: 2D numerical array, of shape (n_channels, n_samples). Alternatively, a list of such
    :param max_offset: scalar or array. max_offset[i] represents the maximum offset allowed between x[0] and x[i], in
    samples. For ease of use, if provided as an array-like object, should have the same number of values as len(x), but
    the first one will be ignored
    :return: Cross-correlation lags relative to x[0], as an int array
    """
    # If max_offset is provided as a scalar, fill an array with it
    try:
        max_offset[0]
    except TypeError:
        max_offset = np.full(shape=(len(x), len(x)), fill_value=max_offset)

    idx = np.array([i for i, _ in enumerate(x) if np.std(_) > 0])

    x = [np.nan_to_num((_ - np.mean(_)) / np.std(_)) for _ in x]

    ccf_mat = np.inf * np.ones(shape=(len(x),))
    ccf_mat[idx[0]] = 0
    for i in range(1, len(idx)):
        ccf_i = np.abs(correlate(x[idx[i]], x[idx[i-1]], method='fft'))
        lags = correlation_lags(len(x[idx[i]]), len(x[idx[i-1]]))
        ccf_i[np.abs(lags) > max_offset[idx[i], idx[i-1]]] = -1
        ccf_mat[idx[i]] = lags[np.nanargmax(ccf_i)]
    ccf_mat[idx] = np.nancumsum(ccf_mat[idx])

    return ccf_mat.astype('int')


def get_audio_offsets(wavs,
                      frame_length=1,
                      frame_step=None,
                      preemphasis=False,
                      max_offset=np.inf,
                      ):
    if preemphasis:
        wavs = [_[1:] - _[:-1] for _ in wavs]

    if frame_length > 1:
        wavs = [np.mean(
            np.abs(
                tf.signal.frame(_,
                                frame_length=frame_length,
                                frame_step=frame_step,
                                axis=-1,
                                pad_end=True)),
            axis=-1
        ) for _ in wavs]

    ccf_mat = ccf(wavs, max_offset=max_offset)
    ccf_mat[np.isfinite(ccf_mat)] -= np.min(ccf_mat[np.isfinite(ccf_mat)])
    ccf_mat *= frame_length

    return ccf_mat


def apply_offsets_to_sounds(x, offsets, filename, mode='full'):
    sr = [sf.info(_).samplerate for _ in x]
    if len(set(sr)) > 1:
        raise ValueError('Files have different sampling rates, cannot combine meaningfully.')
    sr = sr[0]
    if mode == 'valid':
        # Length of the synced file
        length = np.min(np.array([len(_) for _ in x]) - offsets).astype('int')
        # Apply offsets
        x = [sf.read(_, always_2d=True, start=off, frames=length)[0] for _, off in zip(x, offsets)]
        x = flatten([list(_.T) for _ in x])
    else:
        # Length of the synced file
        x = [sf.read(_, always_2d=True)[0] for _ in x]
        x = flatten([list(_.T) for _ in x])
        length = np.max([len(_) + off for _, off in zip(x, offsets)]).astype('int')
        paddings = [(off, length - (off + len(_))) for _, off in zip(x, offsets)]
        x = [np.pad(array=_, pad_width=pad, mode='constant') for _, pad in zip(x, paddings)]
    x = np.stack(x, axis=1)

    # Write out
    sf.write(file=filename, data=x, samplerate=sr)


def length_of_unified_file(x, offsets=None, mode='full'):
    """
    Based on a list of sound files and a list of offsets, compute the length of the unified file obtained by
    synchronising the files in x using the offsets.

    :param x:
    :param offsets: List of int giving the offsets for each CHANNEL of each file in x. Note that this list should
    :param mode: 'full' or 'valid'.
    :return: length of the unified file, in samples
    """
    if not isinstance(offsets, np.ndarray):
        offsets = np.array(offsets)

    if len(x) == len(offsets):
        x = np.array([sf.info(file).frames for file in x])
    else:
        x = np.array(flatten([[sf.info(file).frames] * sf.info(file).channels for file in x]))

    if mode == 'valid':
        length = np.min(x - offsets).astype('int')
    else:
        length = np.max(x + offsets).astype('int')
    return length


def audio_sync(files,
               starts=0,
               sync_duration=60,
               preemphasis=True,
               n_compute=1,
               mode="valid",
               filename=None,
               max_offset=np.inf,
               remove_outlier=0
               ):
    """
    Synchronisation of multiple wav files, with per-channel synchronisation if the provided files are multichannel.

    :param files:
    :param starts: specify times when the files start, in seconds. Used to determine overlaps.
    :param sync_duration: Duration of the segments to use for cross-correlation, in seconds.
    :param preemphasis: bool. If True, applies a pre-emphasis filter defined by y[n+1] = x[n+1] - x[n].
    :param n_compute: int. Number of times the cross-correlation should be run. The function behaves slightly
    differently depending on whether n_compute > 1:
        n <= 1: perform one cross-correlation, using the ENTIRE range defined using starts, stops, sync_start and
        sync_stop.
        n > 1: perform n cross-correlations using samples taken from each file. For a given cross-correlation operation
        the samples are collected by taking a random integer between starts + sync_start and sync_stop - sync_duration,
        and extracting sync_length seconds worth of data.
        (for instance: sync_start = 0, sync_stop = 3600, sync_duration=60, n_compute=5: 5 * 1 minute of data will be
        taken from each file using randomised starts between 0 and 3600 seconds)
    :param max_offset: maximum acceptable offset, in samples. Use if a priori knowledge exists regarding the true
    maximum offset value. Can be provided as a scalar, np.inf, or a list/array of length equal to the total number of
    audio channels across all files.
        For instance, a reasonable use case is for multichannel files recorded simultaneously, where it may be
        reasonable to prevent runaway offsets by contraining the offset value to a small integer.
    :param mode: str, 'valid' or 'full'. Only has an effect if filename is not None, and controls the data written:
    'valid' writes only the INTERSECTION of the files, while 'full' writes each file in full.
    :param filename: None or string. If not None, gives the path to write out the synchronised file.
        Note that for some reason wav files cannot be written entirely if they go over ~6GB in size. In this case,
        this will SILENTLY DISCARD the data at the end of the file, so be careful.
    :return: return the offsets (in samples) that can be directly used to synchronise the files.
        If filename is not None, a synchronised file is written at the provided path using the input files.
    """

    # Get info on the files
    infos = [sf.info(_) for _ in files]
    sr = [_.samplerate for _ in infos]
    if len(set(sr)) > 1:
        raise ValueError('Files have different sampling rates, cannot combine.')
    sr = sr[0]
    channels = [_.channels for _ in infos]
    durations = [_.duration for _ in infos]

    # Number of cross-correlation operations
    n_compute = max(1, n_compute)

    # Maximum offset in samples
    if max_offset is not np.inf:
        max_offset[np.isfinite(max_offset)] *= sr

    # Relative starts
    starts = np.max(starts) - np.array(starts)

    # Broadcast starts values to each channel of each wav (if necessary)
    if hasattr(starts, '__len__'):
        if len(starts) == len(files):
            starts = np.array(starts)
        elif len(starts) == 1:
            starts = np.array(starts * len(files))
        else:
            raise ValueError(f'Cannot broadcast starts to each wav: starts is length {len(starts)}, '
                             f'and there are {len(files)} wav files.')
    else:
        starts = np.array([starts] * len(files))

    sync_starts = (np.linspace(0,
                               np.min(durations) - np.max(starts) - sync_duration,
                               n_compute) * sr).astype('int')
    starts = (starts * sr).astype('int')
    sync_length = int(round(sync_duration * sr))

    offsets_list = []
    for sync in sync_starts:
        # Read in the data
        extracts = [sf.read(f,
                            start=st + sync,
                            frames=sync_length,
                            always_2d=True)[0] for f, st in zip(files, starts)]
        # separates the channels in each wav file
        extracts = flatten([list(w.T) for w in extracts])
        offsets = get_audio_offsets(extracts,
                                    preemphasis=preemphasis,
                                    max_offset=max_offset,
                                    )
        offsets_list.append(offsets)
    if n_compute == 1:
        offsets = offsets_list[0]
    else:
        offsets = np.apply_along_axis(distance_weighted_average,
                                      axis=0,
                                      arr=np.array(offsets_list).astype('int64'),
                                      remove_outlier=remove_outlier)
    offsets = offsets.round().astype('int')

    if mode == 'full':
        # Correct based on the starting time for the files
        offsets += np.array(flatten([[st] * ch for st, ch in zip(starts, channels)]))
        offsets = np.max(offsets) - offsets

    if filename is not None:
        apply_offsets_to_sounds(x=files, offsets=offsets, mode=mode, filename=filename)

    return offsets


if __name__ == '__main__':
    os.chdir('E:/Database_Backup/data/2020_Killian/Rooks/raw/audio/strasbourg_aviary')

    path = 'C:/Users/killian/Desktop/test_audio'
    path1 = 'C:/Users/killian/Desktop/audio_sync.tsv'
    path2 = 'C:/Users/killian/Desktop/audio_sync_with_offsets.tsv'

    # groups with multiple files sharing one of these patterns have more complex overlap patterns
    # they are currently skipped for manual synchronisation
    impossible_overlaps = ['SM4A_', 'SM4B_', 'SM4C_', 'SM4CAMA_', 'SM4CAMB_', 'SM4CAMC_']

    # regex patterns for the starting time of the files (which should be in the file name)
    time_pattern = '(?<=_)[0-9]{6}(?=[^0-9])'
    # regex pattern for the date of the file (which should be in the file name
    date_pattern = '[0-9]{8}'

    # get all dates
    dates = [re.search(pattern=date_pattern, string=_).group(0)
             for _ in os.listdir() if _.endswith('wav') and not _.startswith('._')]
    dates = sorted(list(set(dates)))

    df_list = []
    for date in dates:
        files = sorted([f for f in os.listdir() if date in f and f.endswith('wav')])
        # group together files that overlap (both same date and overlap in time)
        groups = cluster_wav_files(files,
                                   time_pattern=time_pattern,
                                   impossible_overlaps=impossible_overlaps)
        groups.insert(loc=1, column='date', value=date)

        df_list.append(groups)

    df = pd.concat(df_list, axis=0)
    df.to_csv(path1,
              sep='\t',
              mode='a',
              index=False,
              header=not os.path.exists(path1))

    def func(group, path=None, sr=48000, sync_duration=900):
        date = group['date'].iloc[0]
        time = group['group_start'].iloc[0].replace(':', '')
        group['group_name'] = f'{date}_{time}.wav'

        if os.path.exists(path) \
                and pd.read_csv(path, sep='\t')['group_name'].astype('str').eq(group['group_name'].iloc[0]).any():
            return None

        if group['staggered'].all():
            group['channels'] = [np.arange(sf.info(_).channels) for _ in group['file']]
            group = group.explode('channels')
            group['offset'] = None
            group['computed_length'] = None

        else:
            overlap_starts = np.array([convert_str_time_to_seconds(_, sep=':') for _ in group['start']])
            overlap_ends = np.array([convert_str_time_to_seconds(_, sep=':') for _ in group['end']])

            sync_duration = min(sync_duration, np.min(overlap_ends - overlap_starts))

            # max_offset = np.inf * np.ones(shape=(2 * group.shape[0], 2 * group.shape[0]))
            # for i in range(2 * group.shape[0]):
            #     for j in range(2 * group.shape[0]):
            #         if abs(i - j) == 1 and i // 2 == j // 2:
            #             max_offset[i, j] = 1.
            #         elif i == j:
            #             max_offset[i, j] = 0.

            offsets = audio_sync(
                group['file'],
                starts=overlap_starts,
                sync_duration=sync_duration,
                mode='full',
                n_compute=1,
                remove_outlier=0,
                max_offset=np.inf,
                # max_offset=max_offset,
                filename=f'C:/Users/killian/Desktop/test_audio/{date}_{time}.flac' if random() < 10 / len(dates) else None
            )

            group['channels'] = [np.arange(sf.info(_).channels) for _ in group['file']]
            group = group.explode('channels')
            group['offset'] = offsets
            group['computed_length'] = length_of_unified_file(group['file'],
                                                              offsets=offsets,
                                                              mode='full') / sr
            group['expected_length'] = convert_str_time_to_seconds(group['group_end'].iloc[0], sep=':') - convert_str_time_to_seconds(group['group_start'].iloc[0], sep=':')
            return group

    n_jobs = 24
    batch_size = 1
    grouped_df = df.groupby(['date', 'group'])
    with Parallel(n_jobs=n_jobs, batch_size=batch_size, pre_dispatch=2 * batch_size, verbose=100) as p:
        df_list = p(delayed(func)(group, path2) for _, group in df.groupby(['date', 'group']))

    df_list = pd.concat([_ for _ in df_list if _ is not None], ignore_index=True)
    df_list.to_csv(path2, sep='\t', index=False, header=not os.path.exists(path2), mode='a')






