"""
More or less random collection of layers as classes

"""


from time import time
import math

from scipy.stats import truncnorm

import tensorflow as tf
import tensorflow.python.keras.backend as K
import tensorflow_probability as tfp

from utilities.functions import flatten


class BoundedTruncNorm(tf.initializers.Initializer):
    def __init__(self, a, b, dtype, log=True, seed=None, **kwargs):
        super(BoundedTruncNorm, self).__init__(**kwargs)
        self.a = a
        self.b = b
        self.dtype = dtype
        self.log = log
        self.seed = seed

    def __call__(self, shape, dtype=None, **kwargs):
        init = truncnorm.rvs(size=shape, a=self.a, b=self.b, random_state=self.seed)
        init = tf.cast(init, dtype=dtype)
        if self.log:
            init = tf.math.log(init)
        return init

    def get_config(self):
        config = super(BoundedTruncNorm, self).get_config()
        config.update({
            "a": self.a,
            "b": self.b,
            "dtype": self.dtype,
            "log": self.log,
            "seed": self.seed
        })
        return config


class LearnedDRC(tf.keras.layers.Layer):
    def __init__(self, alpha=0., **kwargs):
        self.alpha = alpha
        self.__name__ = "LearnedDRC"
        super(LearnedDRC, self).__init__(**kwargs)

    def build(self, input_shape):
        self.param = self.add_weight(shape=(1, ),
                                     name="alpha",
                                     initializer=tf.keras.initializers.Constant(value=self.alpha),
                                     dtype=self.dtype,
                                     trainable=self.trainable,
                                     )

        super(LearnedDRC, self).build(input_shape)

    def call(self, x, **kwargs):
        return tf.pow(x, tf.math.sigmoid(self.param))

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self):
        config = super(LearnedDRC, self).get_config()
        config.update({
            "alpha": self.alpha
        })
        return config


class RootCompression(tf.keras.layers.Layer):
    """
    Performs the operation y = x^(1/pow), where pow is a (possibly learnable) hyperparameter
    NB: only defined for non-negative inputs!
    """

    def __init__(self, power=3., learnable=False, constraints=None, **kwargs):
        super().__init__(**kwargs)
        self.power = power
        self.learnable = learnable
        self.constraints = constraints
        self.root = 1 / self.power

    def call(self, x, **kwargs):
        return tf.pow(tf.abs(x), self.root)

    def build(self, input_shape):
        self.root = self.add_weight(shape=(),
                                    initializer=tf.keras.initializers.Constant(self.root),
                                    constraint=self.constraints,
                                    trainable=self.learnable,
                                    dtype=self.dtype,
                                    name="root")
        super().build(input_shape)

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self):
        conf = super().get_config()
        conf.update({
            "power": self.power,
            "learnable": self.learnable,
            "constraints": self.constraints
        })
        return conf


@tf.function
def step_relu(x):
    return tf.maximum(0.01, tf.floor(x) + tf.nn.sigmoid(100*(tf.math.mod(x, 1) - 0.5)))


@tf.function
def mish(x):
    return x * tf.nn.tanh(tf.nn.softplus(x))


class CombHSine(tf.keras.layers.Layer):
    def __init__(self,
                 beta=.1,
                 **kwargs):
        super().__init__(**kwargs)
        self.beta = beta
        self.weight = self.add_weight(
            name="beta",
            shape=None,
            dtype=self.dtype,
            initializer=tf.keras.initializers.Constant(value=self.beta),
            constraint=tf.keras.constraints.NonNeg()
        )

    def call(self, x, **kwargs):
        x *= self.weight
        return tf.math.sinh(x) + tf.math.asinh(x)


class PELU(tf.keras.layers.Layer):
    """
    Implements Parameterized Exponential Linear Units (PELU) activation:
        PELU(x) = c * x if x > 0 else a * exp(x/b) - 1
    where a, b and c are non-negative constants.

    This implementation allows a, b and c to be learned as network weights during training.
    """
    def __init__(self,
                 alpha_initializer="ones",
                 alpha_regularizer=None,
                 alpha_constraint="nonneg",
                 beta_initializer="ones",
                 beta_regularizer=None,
                 beta_constraint="nonneg",
                 c_initializer="ones",
                 c_regularizer=None,
                 c_constraint="nonneg",
                 shared_axes=None,
                 **kwargs):
        super().__init__(**kwargs)
        self.supports_masking = True

        self.alpha_initializer = tf.keras.initializers.get(alpha_initializer)
        self.alpha_regularizer = tf.keras.regularizers.get(alpha_regularizer)
        self.alpha_constraint = tf.keras.constraints.get(alpha_constraint)

        self.beta_initializer = tf.keras.initializers.get(beta_initializer)
        self.beta_regularizer = tf.keras.regularizers.get(beta_regularizer)
        self.beta_constraint = tf.keras.constraints.get(beta_constraint)

        self.c_initializer = tf.keras.initializers.get(c_initializer)
        self.c_regularizer = tf.keras.regularizers.get(c_regularizer)
        self.c_constraint = tf.keras.constraints.get(c_constraint)

        if shared_axes is None:
            self.shared_axes = None
        elif not isinstance(shared_axes, (list, tuple)):
            self.shared_axes = [shared_axes]
        else:
            self.shared_axes = list(shared_axes)

    def build(self, input_shape):
        param_shape = list(input_shape[1:])
        if self.shared_axes is not None:
            for i in self.shared_axes:
                param_shape[i - 1] = 1
        self.a = self.add_weight(
            shape=param_shape,
            name='a',
            initializer=self.alpha_initializer,
            regularizer=self.alpha_regularizer,
            constraint=self.alpha_constraint
        )
        self.b = self.add_weight(
            shape=param_shape,
            name='b',
            initializer=self.beta_initializer,
            regularizer=self.alpha_regularizer,
            constraint=self.alpha_constraint
        )
        self.c = self.add_weight(
            shape=param_shape,
            name='c',
            initializer=self.alpha_initializer,
            regularizer=self.alpha_regularizer,
            constraint=self.alpha_constraint
        )
        axes = {}
        if self.shared_axes:
            for i in range(1, len(input_shape)):
                if i not in self.shared_axes:
                    axes[i] = input_shape[i]
        # self.input_spec = tf.keras.layers.InputSpec(ndim=len(input_shape), axes=axes)
        super().build(input_shape)

    def call(self, x, **kwargs):
        pos = self.c * x
        neg = self.a * (tf.math.exp(x / self.b) - 1)
        return tf.where(x > 0, pos, neg)

    def get_config(self):
        conf = super().get_config()
        conf.update({
            'alpha_initializer': tf.keras.initializers.serialize(self.alpha_initializer),
            'alpha_regularizer': tf.keras.regularizers.serialize(self.alpha_regularizer),
            'alpha_constraint': tf.keras.constraints.serialize(self.alpha_constraint),
            'beta_initializer': tf.keras.initializers.serialize(self.beta_initializer),
            'beta_regularizer': tf.keras.regularizers.serialize(self.beta_regularizer),
            'beta_constraint': tf.keras.constraints.serialize(self.beta_constraint),
            'c_initializer': tf.keras.initializers.serialize(self.c_initializer),
            'c_regularizer': tf.keras.regularizers.serialize(self.c_regularizer),
            'c_constraint': tf.keras.constraints.serialize(self.c_constraint),
            'shared_axes': self.shared_axes,
        })
        return conf


class IndependentComponents(tf.keras.layers.Layer):
    def __init__(self, dropout_rate=.2, renorm=False, **kwargs):
        super().__init__(**kwargs)
        self.dropout_rate = dropout_rate
        self.renorm = renorm

        self.dp = tf.keras.layers.SpatialDropout2D(rate=self.dropout_rate)
        self.bn = tf.keras.layers.BatchNormalization(renorm=self.renorm)

    def build(self, input_shape):
        self.dp.build(input_shape)
        self.bn.build(input_shape)
        super().build(input_shape)

    def call(self, inputs, **kwargs):
        return self.bn(self.dp(inputs))

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self):
        conf = super().get_config()
        conf.update({'dropout_rate': self.dropout_rate, 'renorm': self.renorm})


class MaxOut(tf.keras.layers.Layer):
    """
    Implements max-out activation, taking a list of inputs of arbitrary length (but of the same shape) and returning the
    element-wise maximum.
    """
    def __init__(self, axis=-1, **kwargs):
        super().__init__(**kwargs)
        self.axis = axis

    def get_config(self):
        conf = super().get_config()
        conf.update({"axis": self.axis})
        return conf

    def call(self, inputs, **kwargs):
        inputs = tf.stack(inputs, axis=-1)
        return tf.reduce_max(inputs, axis=-1)


class LearnedNormPool2D(tf.keras.layers.AvgPool2D):
    def __init__(self,
                 pool_size=(2, 2),
                 power=2.,
                 strides=None,
                 padding='valid',
                 data_format=None,
                 **kwargs):
        super().__init__(pool_size=pool_size,
                         strides=strides,
                         padding=padding,
                         data_format=data_format,
                         **kwargs)
        self.power = power

    def build(self, input_shape):
        self.power_var = self.add_weight(name="p",
                                         shape=(),
                                         initializer=tf.keras.initializers.Constant(tf.math.log(self.power)),
                                         trainable=self.trainable)

    def call(self, x):
        pow = tf.math.exp(self.power_var)
        x = tf.pow(tf.abs(x), pow)
        x = super().call(x)
        x = tf.pow(x, 1 / pow)
        return x


class LogMeanExpPool2D(tf.keras.layers.AvgPool2D):
    def __init__(self,
                 a=1.,
                 pool_size=(2, 2),
                 strides=None,
                 padding='valid',
                 data_format=None,
                 **kwargs):
        super().__init__(pool_size=pool_size,
                         strides=strides,
                         padding=padding,
                         data_format=data_format,
                         **kwargs)
        self.a = a

    def call(self, x):
        return tf.math.log(super().call(tf.math.exp(self.a * x))) / self.a



class GlobalWeightedRankPooling(tf.keras.layers.Layer):
    def __init__(self, r=.5, *args, **kwargs):
        super(GlobalWeightedRankPooling, self).__init__(*args, **kwargs)
        self.r = r

        self.channel_axis = -1 if K.image_data_format() == "channels_last" else 1

    def build(self, input_shape):
        self.weight = self.add_weight(shape=(1, ), initializer=tf.keras.initializers.Constant(value=self.r),
                                      trainable=self.trainable)
        super(GlobalWeightedRankPooling, self).build(input_shape)  # Be sure to call this at the end

    def func(self, x):
        x = tf.transpose(x)
        x = tf.map_fn(lambda a: tf.sort(K.flatten(a), direction="DESCENDING"), x)
        size = x.shape[1]

        Zr = (tf.pow(self.weight, size) - 1) / (self.weight - 1)
        powers = tf.range(size, dtype=x.dtype)
        r_array = tf.pow(self.weight, powers)

        values = tf.multiply(x, r_array)
        values = tf.reduce_sum(values, axis=-1) / Zr

        return values

    def call(self, x, **kwargs):
        return tf.map_fn(self.func, x)

    def compute_output_shape(self, input_shape):
        return (1, )


class MultiActivation(tf.keras.layers.Layer):
    def __init__(self,
                 activations,
                 **kwargs):
        super().__init__(**kwargs)
        self.activations = activations

    def build(self, input_shape):
        # shape = tuple([len(self.activations), *[1 for _ in input_shape]])
        self.coefficients = self.add_weight(name="coefficients",
                                            shape=(len(self.activations), ),
                                            dtype=self.dtype,
                                            constraint=tf.keras.constraints.NonNeg(),
                                            )

        super().build(input_shape)

    def call(self, inputs, **kwargs):
        out = tf.stack([activation(inputs) for activation in self.activations], axis=0)
        out = tf.tensordot(out, self.coefficients, axes=[0, 0])
        return out

    def get_config(self):
        conf = super().get_config()
        conf.update({"activations": self.activations})
        return conf


class GLU(tf.keras.layers.Layer):
    """
    Gated Linear Unit layer:
        GLU(x) = Conv(x) * sigmoid(Conv(x))
    Note that the two conv operations have separate weights, but are implemented as a single Conv layer with twice the
    depth.

    Base arguments are similar to the Conv layers in Tensorflow:
        filters,
        kernel_size,
        strides,
        dilation_rate
        padding
    Other arguments:
        activation: can either be passed similarly to the Conv layer, as a string or a callable function; can also
            be passed as a layer object (not instantiated), with additional arguments passed as a dict to activation_args
        conv_type: specifies the actual conv operation (defaults to Conv2D)
        con_kwargs: optional dict containing additional arguments to the conv layer
        axis: integer specifying the channel axis. Only provided as an option to override the default value determined
            by tf.keras.backend.image_data_format().
    Additional arguments will be absorbed and passed to the Layer class
    """
    def __init__(self,
                 filters,
                 kernel_size,
                 strides=(1, 1),
                 dilation_rate=1,
                 padding="same",
                 activation=None,
                 activation_args=None,
                 conv_kwargs=None,
                 conv_type=tf.keras.layers.Conv2D,
                 axis=None,
                 **kwargs):
        super().__init__(**kwargs)
        self.axis = axis or (-1 if K.image_data_format() == "channels_last" else 1)
        self.filters = filters
        self.kernel_size = kernel_size
        self.strides = strides
        self.dilation_rate = dilation_rate
        self.padding = padding
        self.activation = activation
        self.activation_args = activation_args or dict()
        self.conv_kwargs = conv_kwargs or dict()
        self.conv_type = conv_type

        if self.conv_type == "1D" or len(self.kernel_size) == 1:
            conv = tf.keras.layers.Conv1D
        elif self.conv_type == "2D" or len(self.kernel_size) == 2:
            conv = tf.keras.layers.Conv2D
        else:
            conv = tf.keras.layers.Conv3D
        self.conv = conv(filters=2*self.filters,
                         kernel_size=self.kernel_size,
                         dilation_rate=self.dilation_rate,
                         activation=None,
                         strides=self.strides,
                         padding=self.padding,
                         **self.conv_kwargs)

        if self.activation is not None and isinstance(self.activation, type(tf.keras.layers.Layer)):
            self.activation = self.activation(**self.activation_args)

    def build(self, input_shape):
        self.conv.build(input_shape)
        # if activation if passed as a layer object
        if self.activation is not None and isinstance(self.activation, type(tf.keras.layers.Layer)):
            self.activation.build(input_shape)
        super().build(input_shape)

    def call(self, inputs, **kwargs):
        x = self.conv(inputs)
        x1 = x[..., :self.filters]
        x2 = x[..., self.filters:]

        sig = tf.nn.sigmoid(x1)
        act = self.activation(x2)
        out = sig * act

        return out

    def compute_output_shape(self, input_shape):
        output_shape = list(input_shape)
        output_shape[1] = (output_shape[1] - self.kernel_size[0]) // self.strides[0] + 1
        output_shape[2] = (output_shape[2] - self.kernel_size[1]) // self.strides[1] + 1
        return output_shape

    def get_config(self):
        conf = super().get_config()
        conf_update = dict(
            filters=self.filters,
            kernel_size=self.kernel_size,
            strides=self.strides,
            dilation_rate=self.dilation_rate,
            padding=self.padding,
            activation=self.activation,
            activation_args=self.activation_args,
            conv_kwargs=self.conv_kwargs,
            conv_type=self.conv_type,
            axis=self.axis,
        )
        conf.update(conf_update)
        return conf


class ModifiedHighWay(GLU):
    """
    HighWay activation layer. Similar to GLU, but adds a second term that allows information to flow through with
     only a linear transformation (modified from the original version for dimension matching):

    H(x) = sigmoid(ConvA(x)) * activation(ConvB(x)) + (1 - sigmoid(ConvA(x)) * ConvB(x)
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def call(self, inputs, **kwargs):
        x = self.conv(inputs)
        x1 = x[..., :self.filters]
        x2 = x[..., self.filters:]

        # x1, x2 = tf.split(x, num_or_size_splits=2, axis=self.axis)

        sig = tf.nn.sigmoid(x1)
        act = self.activation(x2, **self.activation_args)
        out = sig * act + (1 - sig) * x2

        return out


class Clip(tf.keras.layers.Layer):
    """
    This layer takes a list of tensor inputs of length at least 2.

    Functionally an element-wise minimum, taken over the first element of the list. Other elements should be broadcastable
    with the first element (but do not need be broadcastable with other elements).

    Output shape is the same as the first element's shape.
    """
    def call(self, inputs, **kwargs):
        res = inputs[0]
        for clip in inputs[1:]:
            res = tf.minimum(res, clip)
        return res


class SqueezeExcite(tf.keras.layers.Layer):
    """
    SqueezeExcite layer from the Squeeze and Excitation network paper.

    Operation corresponds to:
        SE(x) = x * sigmoid(ConvB(activation(ConvA(x))
    Both convolutions are pointwise (kernel_size of 1 and ConvA acts as a bottleneck, reducing depth by a factor of r.
    """
    def __init__(self,
                 r=16,
                 activation=None,
                 activation_args=None,
                 conv_kwargs=None,
                 **kwargs):
        super().__init__(**kwargs)

        self.r = r
        self.activation = activation
        self.activation_args = activation_args or dict()
        self.conv_kwargs = conv_kwargs or dict()

        self.channel_axis = -1 if K.image_data_format() == "channels_last" else 1
        self.pool_axis = [1, 2] if self.channel_axis == -1 else [2, 3]

    def build(self, input_shape):
        filters = input_shape[self.channel_axis]

        if self.activation is not None and isinstance(self.activation, type(tf.keras.layers.Layer)):
            self.conv1 = tf.keras.layers.Conv2D(
                filters=int(filters // self.r),
                activation=None,
                kernel_size=1,
                **self.conv_kwargs
            )
            self.conv1 = self.activation(**self.activation_args)(self.conv1)
        else:
            self.conv1 = tf.keras.layers.Conv2D(
                filters=int(filters // self.r),
                activation=self.activation,
                kernel_size=1,
                **self.conv_kwargs
            )

        self.conv2 = tf.keras.layers.Conv2D(
            filters=filters,
            activation="sigmoid",
            kernel_size=1,
            **self.conv_kwargs
        )

        super().build(input_shape)

    def call(self, inputs, **kwargs):
        out = tf.reduce_mean(inputs,
                             axis=self.pool_axis,
                             keepdims=True)
        out = self.conv1(out)
        out = self.conv2(out)

        return out * inputs

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self):
        conf = super().get_config()
        conf.update({
            "r": self.r,
            "activation": self.activation,
            "activation_args": self.activation_args,
            "conv_kwargs": self.conv_kwargs,
        })
        return conf

class Quantile(tf.keras.layers.Layer):
    def __init__(self, quantiles, axis=-1, **kwargs):
        super().__init__(**kwargs)
        self.quantiles = quantiles
        self.axis = axis

    def build(self, input_shape):
        self.idx = tf.cast(tf.multiply(tf.constant(self.quantiles, dtype=self.dtype),
                                       tf.cast(input_shape[self.axis], self.dtype)),
                           tf.int32)
        super().build(input_shape)

    def call(self, inputs, **kwargs):
        x = tf.sort(inputs, axis=self.axis)
        q = tf.gather(x, indices=self.idx, axis=self.axis)
        return q

    def compute_output_shape(self, input_shape):
        output_shape = input_shape
        output_shape[self.axis] = len(self.quantiles)
        return output_shape

    def get_config(self):
        conf = super().get_config()
        conf.update({
            "quantiles": self.quantiles,
            "axis": self.axis,
        })


class MyGlobalAveragePooling2D(tf.keras.layers.GlobalAvgPool2D):
    """
    Functionally AveragePooling2D, but with the added keepdims argument.
    If True, summarised axes will remain and be left at size 1.
    """
    def __init__(self,
                 keepdims=False,
                 **kwargs):
        super().__init__(**kwargs)
        self.keepdims=keepdims

    def call(self, inputs, **kwargs):
        if self.data_format == 'channels_last':
            return K.mean(inputs, axis=[1, 2], keepdims=self.keepdims)
        else:
            return K.mean(inputs, axis=[2, 3], keepdims=self.keepdims)

    def get_config(self):
        conf = super().get_config()
        conf.update({"keepdims": self.keepdims})
        return conf


class ChannelWiseAttention(tf.keras.layers.Layer):
    """
    Variant of Self-attention that focuses on the channel axis instead of the sequence axis (e.g. temporal), with
    optional restriction of the window to focus on.

    :param k: int. Length of the window to focus on. Ignored if negative, in which case no windowing is performed.
    An error is raised if k is positive and even (this includes 0). Defaults to -1
    :param axis: int. Axis to focus on. Defaults to -2 for the original use case of temporal windowing with batch
    data format (batch, freq_bins, time_bins, channels).
    :param use_scale: bool. Like for the Tensorflow Attention, if True adds a learnable scaling parameter
    """
    def __init__(self, k=-1, axis=-2, use_scale=True, **kwargs):
        super().__init__(**kwargs)

        self.axis = axis
        self.use_scale = use_scale
        self.channel_axis = -1 if K.image_data_format() == "channels_last" else 1
        self.k = k

        if self.k > 0 and self.k % 2 == 0:
            raise ValueError("k should be either strictly below 0 or an odd integer (as a neighbourhood of k frames,"
                             "including the target frame")

    def build(self, input_shape):
        """Creates scale variable if use_scale==True."""
        if self.use_scale:
            self.scale = self.add_weight(
                name='scale',
                shape=(),
                initializer=tf.keras.initializers.ones(),
                dtype=self.dtype,
                trainable=True)
        else:
            self.scale = None

        super().build(input_shape)

    def _pad_framed(self, x):
        # pads inputs so as to preserve original dimensions along axis
        paddings = [(0, 0)] * len(x.shape)
        paddings[self.axis] = (self.k // 2, self.k // 2)
        padded = tf.pad(x, paddings=paddings)

        # frames the signal in windows of length k with step 1
        framed = tf.signal.frame(padded, frame_length=self.k, frame_step=1, axis=self.axis)

        return framed

    def call(self, inputs, **kwargs):
        q = inputs[0]
        k = inputs[1]
        v = inputs[2] if len(inputs) > 2 else k

        if self.k > 0:
            q = self._pad_framed(q)
            k = self._pad_framed(k)

        # computes the attention scores using Dot product score function
        scores = tf.matmul(q, k, transpose_a=True)
        print(q.shape, k.shape, scores.shape)

        # scale
        if self.scale is not None:
            scores *= self.scale

        scores = tf.nn.softmax(scores)

        # applies scores (axis added to obtain the correct dims)
        if self.k > 0:
            res = tf.matmul(scores, tf.expand_dims(v, axis=-1))
            res = tf.squeeze(res, axis=[-1])
        else:
            res = tf.matmul(scores, v, transpose_b=True)
            # permute only the last two axes of res
            res = tf.linalg.matrix_transpose(res)

        return res

    def get_config(self):
        conf = super().get_config()
        conf.update(
            {
                "k": self.k,
                "axis": self.axis,
                "use_scale": self.use_scale,
             }
        )
        return conf


class AttentionActivation(tf.keras.layers.Activation):
    def __init__(self, r=1, inner_activation=None, batchnorm=True, **kwargs):
        super().__init__(**kwargs)
        self.r = r
        self.inner_activation = inner_activation
        self.channel_axis = -1 if K.image_data_format() == "channels_last" else 1
        self.batchnorm = batchnorm

        self.conv1 = None
        self.conv2 = None
        self.layers = []

    def build(self, input_shape):
        self.conv1 = tf.keras.layers.Conv2D(filters=input_shape[self.channel_axis] // self.r,
                                            kernel_size=1,
                                            activation=self.inner_activation)
        self.conv2 = tf.keras.layers.Conv2D(filters=input_shape[self.channel_axis],
                                            kernel_size=1,
                                            activation='sigmoid')
        self.layers = [self.conv1, self.conv2]
        if self.batchnorm:
            self.bn1 = tf.keras.layers.BatchNormalization(axis=self.channel_axis)
            self.layers.insert(1, self.bn1)
            self.bn2 = tf.keras.layers.BatchNormalization(axis=self.channel_axis)
            self.layers.insert(-1, self.bn2)
        super().build(input_shape)

    def call(self, x, **kwargs):
        y = x
        for layer in self.layers:
            y = layer(y)
        return x * y

    def get_config(self):
        conf = super().get_config()
        conf.update({
            'r': self.r, 'inner_activation': self.inner_activation, 'batchnorm': self.batchnorm
        })


def generic_attention_module(x,
                             batchnorm=True,
                             renorm=False,
                             type="spatial"):
    assert type in ["spatial", "channel"], \
        ValueError("type should be either 'spatial' or 'channel' to add the appropriate module")
    assert batchnorm in [True, False, "in", "out"], \
        ValueError("batchnorm must be either True (all Conv2D followed by BN), "
                   "False (no BN), 'in' (BN only before the attention layer), or"
                   "'out' (BN only after the attention layer")

    channel_axis = -1 if K.image_data_format() == "channels_last" else 1

    input_shape = x.shape[1:]
    filters = x.shape[channel_axis] // 2
    target_shape = [-1, filters] if channel_axis == -1 else [filters, -1]

    reshape_layer = tf.keras.layers.Reshape(target_shape=target_shape)
    reshape_back_layer = tf.keras.layers.Reshape(target_shape=input_shape)

    q = tf.keras.layers.Conv2D(filters=filters,
                               kernel_size=(1, 1),
                               activation="linear")(x)
    v = tf.keras.layers.Conv2D(filters=filters,
                               kernel_size=(1, 1),
                               activation="linear")(x)
    k = tf.keras.layers.Conv2D(filters=filters,
                               kernel_size=(1, 1),
                               activation="linear")(x)

    if batchnorm in [True, "in"]:
        q = tf.keras.layers.BatchNormalization(renorm=renorm, axis=channel_axis)(q)
        v = tf.keras.layers.BatchNormalization(renorm=renorm, axis=channel_axis)(v)
        k = tf.keras.layers.BatchNormalization(renorm=renorm, axis=channel_axis)(k)

    q, v, k = reshape_layer(q), reshape_layer(v), reshape_layer(k)

    perm_layer = tf.keras.layers.Permute(dims=(2, 1))
    if (type == "channel" and channel_axis == -1) or (type == "spatial" and channel_axis == 1):
        q = perm_layer(q)
        v = perm_layer(v)
        k = perm_layer(k)

    scores = tf.keras.layers.Attention(use_scale=False)([q, v, k])

    if (type == "channel" and channel_axis == -1) or (type == "spatial" and channel_axis == 1):
        scores = tf.keras.layers.Permute(dims=(2, 1))(scores)

    scores = reshape_back_layer(scores)
    out = tf.keras.layers.Conv2D(filters=filters,
                                 kernel_size=(1, 1),
                                 activation="linear")(scores)
    if batchnorm in [True, "end"]:
        out = tf.keras.layers.BatchNormalization(renorm=renorm, axis=channel_axis)(out)

    out = tf.keras.layers.Add()([x, out])

    return out


class TripletAttention(tf.keras.layers.Layer):
    def __init__(self, kernel_size, conv_kwargs=None, **kwargs):
        super().__init__(**kwargs)
        self.kernel_size = kernel_size
        self.conv_kwargs = dict() if conv_kwargs is None else conv_kwargs

    def build(self, input_shape):
        self.conv0 = tf.keras.layers.Conv2D(kernel_size=self.kernel_size,
                                            filters=1,
                                            activation='sigmoid',
                                            **self.conv_kwargs)
        self.conv1 = tf.keras.layers.Conv2D(kernel_size=self.kernel_size,
                                            filters=1,
                                            activation='sigmoid',
                                            **self.conv_kwargs)
        self.conv2 = tf.keras.layers.Conv2D(kernel_size=self.kernel_size,
                                            filters=1,
                                            activation='sigmoid',
                                            **self.conv_kwargs)
        super().build(input_shape)

    @staticmethod
    def zpooling(x, axis):
        return tf.concat([tf.reduce_mean(x, axis, keepdims=True), tf.reduce_max(x, axis, keepdims=True)], axis=axis)

    def call(self, x, **args):
        perm0 = self.zpooling(x, axis=-1)
        perm1 = self.zpooling(tf.transpose(x, perm=[0, 1, 3, 2]), axis=-1)
        perm2 = self.zpooling(tf.transpose(x, perm=[0, 3, 2, 1]), axis=-1)

        conv0 = self.conv0(perm0)
        conv1 = self.conv1(perm1)
        conv2 = self.conv2(perm2)

        conv1 = tf.transpose(conv1, perm=[0, 1, 3, 2])
        conv2 = tf.transpose(conv2, perm=[0, 3, 2, 1])

        out = tf.stack([x * conv0, x * conv1, x * conv2], axis=-1)
        return tf.reduce_mean(out, axis=-1)



def SSA_module(x, use_scale=True, name=None):
    channel_axis = -1 if K.image_data_format() == "channels_last" else 1

    out_list = [x]

    # Time attention
    if x.shape[2] > 1:
        x_t = tf.keras.layers.Conv2D(filters=x.shape[channel_axis] // 2,
                                     kernel_size=(1, 1),
                                     activation=None)(x)
        x_t = tf.keras.layers.Permute(dims=(2, 3, 1))(x_t)
        x_t = tf.keras.layers.Attention(use_scale=use_scale)([x_t, x_t, x_t])
        x_t = tf.keras.layers.Permute(dims=(3, 1, 2))(x_t)
        x_t = tf.keras.layers.Conv2D(filters=x.shape[channel_axis],
                                     kernel_size=(1, 1),
                                     activation=None)(x_t)
        out_list.append(tf.keras.layers.Add()([x_t, x]))

    # Frequency attention
    if x.shape[1] > 1:
        x_f = tf.keras.layers.Conv2D(filters=x.shape[channel_axis] // 2,
                                     kernel_size=(1, 1),
                                     activation=None)(x)
        x_f = tf.keras.layers.Permute(dims=(1, 3, 2))(x_f)
        x_f = tf.keras.layers.Attention(use_scale=use_scale)([x_f, x_f, x_f])
        x_f = tf.keras.layers.Permute(dims=(1, 3, 2))(x_f)
        x_f = tf.keras.layers.Conv2D(filters=x.shape[channel_axis],
                                     kernel_size=(1, 1),
                                     activation=None)(x_f)
        out_list.append(tf.keras.layers.Add()([x_f, x]))

    out = tf.keras.layers.Concatenate(name=name)(out_list)

    return out


class Expand(tf.keras.layers.Layer):
    def __init__(self, axis=-1, **kwargs):
        super().__init__(**kwargs)
        self.axis = axis

    def call(self, x, **kwargs):
        return tf.expand_dims(x, axis=self.axis)

    def compute_output_shape(self, input_shape):
        output_shape = list(input_shape)
        output_shape.insert(self.axis, 1)
        return output_shape

    def get_config(self):
        conf = super().get_config()
        conf.update({"axis": self.axis})
        return conf


class Squeeze(tf.keras.layers.Layer):
    def __init__(self, axis=None, **kwargs):
        super(Squeeze, self).__init__(**kwargs)
        self.axis = axis

    def call(self, x, **kwargs):
        return K.squeeze(x, axis=self.axis)

    def compute_output_shape(self, input_shape):
        output_shape = tuple(filter(lambda x: x != 1, input_shape))
        return output_shape

    def get_config(self):
        conf = super(Squeeze, self).get_config()
        conf.update({"axis": self.axis})
        return conf


class MatMul(tf.keras.layers.Layer):
    def __init__(self, mask, name='matmul', **kwargs):
        super().__init__(name=name, **kwargs)

        self.mask = self.add_weight(shape=mask.shape,
                                    dtype=self.dtype,
                                    name='mask',
                                    trainable=False,
                                    initializer=tf.keras.initializers.Constant(value=tf.cast(mask, self.dtype)))
        # self.mask = tf.cast(mask, self.dtype)
        # For now, effectively forced to -1
        # self.channel_axis = -1 if K.image_data_format() == "channels_last" else 1

    def compute_output_shape(self, input_shape):
        output_shape = input_shape
        output_shape[-1] = self.mask.shape[-1]

    def call(self, inputs, **kwargs):
        out = tf.matmul(inputs, tf.cast(self.mask, dtype=inputs.dtype))
        return out

    def get_config(self):
        conf = super().get_config()
        # conf.update({'mask': self.mask})
        return conf


class SplitAxis(tf.keras.layers.Layer):
    def __init__(self, groups, axis=-1, **kwargs):
        super().__init__(**kwargs)
        self.groups = groups
        self.axis = axis

    def call(self, x, **kwargs):
        x = tf.split(x, num_or_size_splits=self.groups, axis=self.axis)
        x = tf.stack(x, axis=self.axis)
        return x

    def compute_output_shape(self, input_shape):
        output_shape = input_shape
        output_shape[self.axis] = output_shape[self.axis] // self.groups
        output_shape.insert(self.axis, self.groups)
        return output_shape

    def get_config(self):
        conf = super().get_config()
        conf.update({
            "groups": self.groups,
            "axis": self.axis
        })
        return conf


class Print(tf.keras.layers.Layer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def call(self, x, **kwargs):
        print(tf.where(tf.math.is_nan(x)))
        return x


class Indicator(tf.keras.layers.Layer):
    """
    Implements indicator function as a layer, specifically:
        x, y: y if x > 0 else 0

    Works for each sample separately, and with an optional axis argument to ignore one or more other axis/axes.

    Expects a list of length 2, e.g.: Indicator(axis=-1)([x, y])

    Lists of length > 2 will work, but all elements after the first 2 are ignored.
    """
    def __init__(self, axis=-1, **kwargs):
        if "channel_axis" in kwargs:
            kwargs.pop("channel_axis")
        super().__init__(**kwargs)
        self.axis = axis
        self.channel_axis = -1 if K.image_data_format() == "channels_last" else 1

    def build(self, input_shape):
        # all but batch and chosen axis
        self.axes = list(range(1, len(input_shape)))
        self.axes.pop(self.axis)
        super().build(input_shape)

    def call(self, x, **kwargs):
        return tf.where(tf.reduce_sum(x[0], axis=self.axes, keepdims=True) > 0., x[1], tf.zeros_like(x[1]))

    def get_config(self):
        conf = super().get_config()
        conf.update({"axis": self.axis})
        return conf

    def compute_output_shape(self, input_shape):
        return input_shape


class OvODecodingLayer(tf.keras.layers.Layer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.channel_axis = -1 if K.image_data_format() == "channels_last" else 1

        self._built = False
        self.M = None
        self.k = None

    @staticmethod
    def one_vs_one_code_matrix(K):
        cols = list(range(K * (K - 1) // 2))
        rows_neg = flatten([list(range(j + 1, K)) for j in range(K)])
        rows_pos = flatten([[j] * (K - j - 1) for j in range(K - 1)])

        indices = tf.constant([rows_pos + rows_neg,
                               cols + cols],
                              dtype=tf.int64)
        indices = tf.transpose(indices)
        values = tf.ones(shape=(K * (K - 1) // 2,))
        values = tf.concat([values, -values], axis=0)

        M = tf.sparse.SparseTensor(indices=indices,
                                   values=values,
                                   dense_shape=(K, K * (K - 1) // 2))
        M = tf.sparse.reorder(M)

        return tf.sparse.to_dense(M)

    def _build(self, input_shape):
        self.k = int((1 + math.sqrt(1 + 8 * input_shape[self.channel_axis])) / 2)
        self.M = self.one_vs_one_code_matrix(self.k)
        self._built = True

    def decoding(self, x):
        if self.channel_axis == -1:
            print(self.M)
            decoded = tf.matmul(x, self.M, transpose_b=True)
        else:
            decoded = tf.matmul(self.M, x, transpose_b=True)
        return decoded

    def call(self, x, **kwargs):
        if not self._built:
            self._build(x.shape)

        return self.decoding(x)

    def compute_output_shape(self, input_shape):
        output_shape = list(input_shape)
        output_shape[self.channel_axis] = self.k
        return output_shape


class MultiLossLearnableWeighting(tf.keras.layers.Layer):
    """
    This layer is intended to add a weighted sum of multiple losses to a model with the weighting coefficients learned
    during model training.

    To work, the model should be passed the ground truth corresponding to each output along the regular inputs.
    """

    def __init__(self, losses, **kwargs):
        self.nb_outputs = len(losses)
        self.loss_list = losses
        self.is_placeholder = True
        super().__init__(**kwargs)

    def build(self, input_shape):
        self.vars = [self.add_weight(name=f'var_{i}',
                                     shape=(1, ),
                                     initializer="ones",
                                     constraint="nonneg",
                                     trainable=True) for i in range(self.nb_outputs)]
        super().build(input_shape)

    def multi_loss(self, ys_true, ys_pred):
        if not (len(ys_true) == self.nb_outputs and len(ys_pred) == self.nb_outputs):
            raise ValueError(f"Lists of outputs did not match the expected lengths: "
                             f"len(y_true)={len(ys_true)} and len(y_pred)={len(ys_pred)}, "
                             f"but nb_outputs={self.nb_outputs}")
        print([(ys_true[k], ys_pred[k]) for k in self.loss_list.keys()])
        loss_values = [loss(ys_true[k], ys_pred[k]) for k, loss in self.loss_list.items()]

        weighted = [loss / (2 * var**2) for loss, var in zip(loss_values, self.vars)]
        reg_term = tf.math.log(K.prod([1+v**2 for v in self.vars]))

        return tf.reduce_mean(weighted) + reg_term

    def call(self, inputs, **kwargs):
        ys_true = inputs[0]
        ys_pred = inputs[1]
        loss = self.multi_loss(ys_true, ys_pred)
        self.add_loss(loss, inputs=inputs)
        # We won't actually use the output.
        return ys_pred

    def get_config(self):
        conf = super().get_config()
        conf.update({
            'loss_list': self.loss_list,
            'nb_outputs': self.nb_outputs
        })