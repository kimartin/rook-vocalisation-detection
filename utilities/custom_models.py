"""
Collection of functions to implement a few different models for trying things out, not used in the experiments
"""

import tensorflow as tf
from tensorflow.keras.layers import BatchNormalization, Conv2D, Dense, SpatialDropout2D, \
    Input, Dropout, Concatenate, Add, ReLU, Flatten, Softmax, \
    MaxPooling2D
from utilities.custom_layers import GLU, ModifiedHighWay, SqueezeExcite
import tensorflow.keras.backend as K
from tensorflow.keras.models import Model


def D2_module(model,
              k,
              layers,
              kernel_size,
              activation,
              activation_args=dict(),
              dropout_rate=0.,
              gate=False,
              renorm=False,
              conv_kwargs=dict(),
              c=1.,
              bottleneck=4,
              conc=True,
              r=0,
              **kwargs):

    if dropout_rate > 0.:
        model = SpatialDropout2D(rate=dropout_rate)(model)

    def conv_op(model, gate, filters, dilation_rate=1, r=r):
        if gate == "glu":
            x = GLU(filters=filters,
                    kernel_size=kernel_size,
                    activation=activation,
                    activation_args=activation_args,
                    dilation_rate=dilation_rate,
                    # padding="same",
                    conv_type=f"{len(kernel_size)}D",
                    conv_kwargs=conv_kwargs
                    )(model)
        elif gate == "highway":
            x = ModifiedHighWay(filters=filters,
                                kernel_size=kernel_size,
                                activation=activation,
                                activation_args=activation_args,
                                dilation_rate=dilation_rate,
                                conv_type=f"{len(kernel_size)}D",
                                # padding="same",
                                conv_kwargs=conv_kwargs
                                )(model)
        else:
            x = Conv2D(filters=filters,
                       kernel_size=kernel_size,
                       activation=activation,
                       dilation_rate=dilation_rate,
                       padding="same",
                       **conv_kwargs
                       )(model)
        if r > 0:
            x = SqueezeExcite(r=r)(x)

        x = BatchNormalization(renorm=renorm)(x)
        return x

    if model.shape[-1] > bottleneck * k:
        model = Conv2D(filters=bottleneck * k,
                       kernel_size=1,
                       activation=None,
                       **conv_kwargs)(model)

    x_list = conv_op(model, gate=gate, filters=k * layers, dilation_rate=1)
    if layers > 1:
        x_list = [x_list[..., (i*k):((i+1)*k)] for i in range(layers)]

    for i in range(layers - 1):
        tmp = conv_op(x_list[i], gate=gate, filters=k * (layers - i - 1), dilation_rate=int(2 ** (i + 1)))
        for j in range(layers - i - 1):
            x_list[j + i + 1] = x_list[j + i + 1] + tmp[..., (j * k):((j + 1) * k)]

    if conc:
        # x = Concatenate()([model] + x_list)
        # exclude the straight run-through
         x = Concatenate()(x_list)
    else:
        x = x_list[-1]

    if c < 1.:
        x = Conv2D(filters=c * x.shape[-1],
                   kernel_size=1,
                   activation=None,
                   **conv_kwargs)(x)

    return x


def D3block(model,
            blocks,
            layers,
            k,
            kernel_size,
            activation,
            activation_args=None,
            c=.2,
            bottleneck=4,
            gate=False,
            renorm=False,
            conv_kwargs=dict(),
            dropout_rate=0.,
            conc=True,
            r=0,
            **kwargs):

    if activation_args is None:
        activation_args = dict()

    for _ in range(blocks):
        if conc:
            x_tmp = D2_module(model,
                              layers=layers,
                              kernel_size=kernel_size,
                              k=k,
                              activation=activation,
                              activation_args=activation_args,
                              gate=gate,
                              renorm=renorm,
                              conv_kwargs=conv_kwargs,
                              dropout_rate=dropout_rate,
                              c=c,
                              conc=True,
                              bottleneck=bottleneck,
                              r=r,
                              **kwargs)
            model = Concatenate()([model, x_tmp])
            if r > 0:
                model = SqueezeExcite(r=r)(model)
        else:
            model = D2_module(model,
                              layers=layers,
                              kernel_size=kernel_size,
                              k=k,
                              activation=activation,
                              activation_args=activation_args,
                              gate=gate,
                              renorm=renorm,
                              conv_kwargs=conv_kwargs,
                              dropout_rate=dropout_rate,
                              c=c,
                              bottleneck=bottleneck,
                              conc=False,
                              **kwargs)
            if r > 0:
                model= SqueezeExcite(r=r)(model)

    return model


# Dense(R)Net
def DenseRNet(x, l=10, k=(32, 64, 128), n=3, s=3, activation=ReLU, **kwargs):
    """
    Creates a DenseR block (REF TO ADD)

    :param x: input to the network
    :param l: number of components in a block
    :param k: list of int. Growth rate for each successive block. Length defines the number of blocks
    :param n: number of conv layers in a block
    :param s: kernel_size for the conv layers
    :param blocks: ignored if k is a list of length > 1, otherwise gives the number of blocks.
    :return:
    """
    channel_axis = -1 if K.image_data_format() == "channels_last" else 1
    axis = 1 if channel_axis == -1 else 2
    x = BatchNormalization(axis=axis, dynamic=True)(x)

    x = Conv2D(filters=32, kernel_size=(7, 7), strides=(2, 1), padding="same", dynamic=True)(x)
    x = activation()(x)
    x = MaxPooling2D(pool_size=(2, 1))(x)

    for _ in k[:-1]:
        x = denseR_block(x, l, _, n, s, activation=activation, **kwargs)
        x = transition(x, **kwargs)
    x = denseR_block(x, l, k[-1], n, s, activation=activation, **kwargs)
    x = Conv2D(filters=..., kernel_size=(1,1), **kwargs)(x)
    x = BatchNormalization()(x)
    x = activation()(x)

    x = BatchNormalization()(x)
    x = activation()(x)
    x = Flatten()(x)
    x = activation()(x)
    x = BatchNormalization()(x)
    x = Softmax()(x)

    return x


def denseR_block(x, l, k, n, s, activation=ReLU, **kwargs):
    axis = -1 if K.image_data_format() == "channels_last" else 1
    y = Conv2D(filters=k, kernel_size=(1, 1), **kwargs)(x)
    y = BatchNormalization()(y)
    y = activation()(y)
    y = basic_component(y, k, n, s, **kwargs)
    x_list = [x, y]
    for _ in range(l-1):
        y = Concatenate(axis=axis)(x_list)
        y = Conv2D(filters=k, kernel_size=(1, 1), **kwargs)(y)
        y = BatchNormalization()(y)
        y = activation()(y)
        y = basic_component(y, k, n, s, **kwargs)
        x_list.append(y)
    return Concatenate(x_list, axis=axis)


def basic_component(x, k, n, s, rate=.5, activation=ReLU, **kwargs):
    y = Conv2D(filters=k, kernel_size=(s, s), padding="same", **kwargs)(x)
    y = BatchNormalization()(y)
    y = activation()(y)
    y = Dropout(rate=rate)(y)
    for _ in range(n-1):
        y = Conv2D(filters=k, kernel_size=(s, s), padding="same", **kwargs)(y)
        y = BatchNormalization()(y)
        y = activation()(y)
        y = Dropout(rate=rate)(y)
    return Add()([x, y])


def transition(x, k, pool_type="max", activation=ReLU, **kwargs):
    x = Conv2D(filters=k, kernel_size=(1, 1), padding="same", **kwargs)(x)
    x = activation()(x)
    if pool_type == "max":
        x = MaxPooling2D(pool_size=(2, 1))(x)
    else:
        x = Conv2D(filters=k, kernel_size=(2, 1), strides=(2, 1), padding="same", **kwargs)(x)
        x = activation()(x)
    return x


# BirdVoxModel
def BirdVoxModel(shape1=(128, 104, 1), shape2=(32, 9, 1)):
    """
    Creates the BirdVocModel from Lostanlen et al 2019
    :param shape1: Input shape of the main branch
    :param shape2: Input shape of the auxiliary branch
    :return:
    """

    inputA = Input(shape=shape1)
    inputB = Input(shape=shape2)

    # Main branch
    x = Conv2D(24, kernel_size=(5, 5), activation="relu", kernel_regularizer="l2")(inputA)
    x = MaxPooling2D(pool_size=(2, 4))(x)
    x = Conv2D(24, kernel_size=(5, 5), activation="relu", kernel_regularizer="l2")(x)
    x = MaxPooling2D(pool_size=(2, 4))(x)
    x = Conv2D(48, kernel_size=(5, 5), activation="relu", kernel_regularizer="l2")(x)
    x = Dense(64, activation="relu")(x)

    # Auxiliary branch
    aux = Conv2D(8, kernel_size=(32, 1))(inputB)
    aux = Dense(64, activation="relu")(aux)

    # Combine
    # using the Adaptive Threshold formulation
    combined = tf.keras.layers.Add()([x, aux])
    out = Dense(1, activation="sigmoid")(combined)

    return Model(inputs=[inputA, inputB], outputs=out)


if __name__ == "__main__":
    inputs = tf.keras.layers.Input(shape=(20, 20, 1))
    x = D2_module(inputs,
                  k=2,
                  layers=3,
                  kernel_size=(3, 3),
                  activation=None,
                  conv_kwargs=dict(kernel_initializer="ones"),
                  renorm=False,
                  conc=False,
                  c=1)
    model = tf.keras.Model(inputs=inputs, outputs=x)
    #print(model.output_shape)
    print(model(tf.ones(shape=(1, 20, 20, 1))))

