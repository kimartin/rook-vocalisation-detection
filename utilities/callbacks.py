import collections
import csv
import io
import datetime
from shutil import copy
from time import time

import numpy as np
import six
import tensorflow as tf
import tensorflow.python.keras.backend as K
from tensorflow.python.lib.io import file_io
from tensorflow.python.util.compat import collections_abc


class WarmUp(tf.keras.callbacks.Callback):
    def __init__(self, e, lr_max=None, lr_min=None):
        """
        Implements learning rate warmup at the beginning of training. Learning rate will start at lr2 (the lowest) and
        linearly increase to lr1 (the highest) over e epochs.

        Note that this won't work for compound optimizers (e.g. LookAhead from tensorflow_addons)

        :param e: Number of epochs over which the warmup will occur.
        :param lr_min: Lower end of the learning rate range. The model will begin training at this value. Defaults to
        lr_max / 1000 if unspecified.
        :param lr_max: Upper end of the learning range.
        """
        super(WarmUp, self).__init__()
        self.e = e
        self.lr_max = lr_max
        self.lr_min = lr_min or lr_max / 1000
        self.epochs = 0

    def on_train_begin(self, logs=None):
        try:
            K.set_value(self.model.optimizer.lr, K.get_value(self.lr_min))
        except AttributeError:
            K.set_value(self.model.optimizer.inner_optimizer.lr, K.get_value(self.lr_min))

    def on_train_batch_end(self, batch, logs=None):
        if self.epochs < self.e:
            t = (self.epochs * self.params["steps"] + batch) / (self.e * self.params["steps"])
            lr = (self.lr1 - self.lr2) * t + self.lr2

            try:
                K.set_value(self.model.optimizer.lr, K.get_value(self.lr))
            except AttributeError:
                K.set_value(self.model.optimizer.inner_optimizer.lr, K.get_value(self.lr))

    def on_epoch_end(self, epoch, logs=None):
        self.epochs += 1


class LearningRateRangeFinder(tf.keras.callbacks.Callback):
    def __init__(self, filename, e, lr_max, lr_min=None, separator=',', append=False, **kwargs):
        """
        Implements a learning rate range test (Smith et al 2015) to determine possible values of the learning rate for a
        given model.

        Logs learning rate and loss after every training batch over a number of epochs to a csv file.
        After each batch, the learning rate of the model is updated following the formula:
            lr(batch+1) = lr(batch) * (lr_max/lr_min)

        :param filename: gives the path of the file to save values to
        :param e: number of epochs to test over.
        :param lr_max: Maximum value of the learning rate to test (note that if the model diverges and returns NaN loss,
        the test will abort early)
        :param lr_min: Minimum value of the learning rate to test
        :param separator: Determines the separator to use to save the file (e.g. ',' for a CSV file)
        :param append: False writes a new file, potentially overwriting previous results. True appends to previous
        results
        :param kwargs: Absorbs additional arguments.
        """
        super(LearningRateRangeFinder, self).__init__()

        self.sep = separator
        self.filename = filename
        self.append = append
        self.writer = None
        self.keys = None
        self.append_header = True
        if six.PY2:
            self.file_flags = 'b'
            self._open_args = {}
        else:
            self.file_flags = ''
            self._open_args = {'newline': '\n'}
        self.e = e
        self.lr_max = lr_max
        self.lr_min = lr_min or K.epsilon()

        self.epochs = 0

    def on_train_begin(self, logs=None):
        if self.append:
            if file_io.file_exists(self.filename):
                with open(self.filename, 'r' + self.file_flags) as f:
                    self.append_header = not bool(len(f.readline()))
            mode = 'a'
        else:
            mode = 'w'
        self.csv_file = io.open(self.filename,
                                mode + self.file_flags,
                                **self._open_args)
        try:
            K.set_value(self.model.optimizer.lr, K.get_value(self.lr_min))
        except AttributeError:
            K.set_value(self.model.optimizer.inner_optimizer.lr, K.get_value(self.lr_min))

    def on_train_batch_end(self, batch, logs=None):
        logs = logs or {}

        # def handle_value(k):
        #     is_zero_dim_ndarray = isinstance(k, np.ndarray) and k.ndim == 0
        #     if isinstance(k, six.string_types):
        #         return k
        #     elif isinstance(k, collections_abc.Iterable) and not is_zero_dim_ndarray:
        #         return '"[%s]"' % (', '.join(map(str, k)))
        #     else:
        #         return k

        if self.keys is None:
            self.keys = sorted(logs.keys())

        if self.model.stop_training:
            # We set NA so that csv parsers do not fail for this last epoch.
            logs = dict((k, logs[k]) if k in logs else (k, 'NA') for k in self.keys)

        if not self.writer:

            class CustomDialect(csv.excel):
                delimiter = self.sep

            fieldnames = ['epoch', 'batch', 'total', 'lr', *[_ for _ in logs.keys() if "loss" in _]]

            self.writer = csv.DictWriter(
                self.csv_file,
                fieldnames=fieldnames,
                dialect=CustomDialect)
            if self.append_header:
                self.writer.writeheader()

        t = (self.epochs * self.params["steps"] + batch)/(self.e * self.params["steps"])
        lr = self.lr_min * (self.lr_max/self.lr_min) ** t
        try:
            K.set_value(self.model.optimizer.lr, K.get_value(lr))
        except AttributeError:
            K.set_value(self.model.optimizer.inner_optimizer.lr, K.get_value(lr))

        row_dict = collections.OrderedDict({
            'epoch': self.epochs,
            'batch': batch,
            'lr': lr,
            'total': batch + self.epochs * self.params["steps"],
            **{k: v for k, v in logs.items() if "loss" in k},
        })
        self.writer.writerow(row_dict)
        self.csv_file.flush()

    def on_epoch_end(self, epoch, logs=None):
        self.epochs += 1
        if epoch >= self.e:
            self.model.stop_training = True

    def on_train_end(self, logs=None):
        self.csv_file.close()
        self.writer = None


class PlotModel(tf.keras.callbacks.Callback):
    """
    Saves a PNG file plotting the model's architecture at the beginning of training.

    Note that the image may not be true if the models has intricate connectivity patterns. (e.g. weird skip connections
    will be ignored, but still exist if printing model.summary() )
    """
    def __init__(self, to_file="model.png", **kwargs):
        super().__init__()
        self.to_file = to_file
        self.kwargs = kwargs

    def on_train_begin(self, logs=None):
        tf.keras.utils.plot_model(model=self.model, to_file=self.to_file, show_shapes=True, **self.kwargs)


class LateStopping(tf.keras.callbacks.EarlyStopping):
    """
    Slight variant on Early Stopping, with two differences:
        - can only activate after at least min_epochs have elapsed to let the network explore a bit
        - if it would activate, the network can complete another cycle (e.g. if using a cyclical learning rate

    Note that if the best epoch is reached before min_epochs, it is still considered as the best and compared to later
    epochs.
    """
    def __init__(self, min_epochs, cycle=1, **kwargs):
        super().__init__(**kwargs)
        self.min_epochs = min_epochs
        self.cycle = cycle if cycle >= 1 else 1

    def on_epoch_end(self, epoch, logs=None):
        current = self.get_monitor_value(logs)
        if current is None:
            return
        if self.monitor_op(current - self.min_delta, self.best):
            self.best = current
            self.wait = 0
            if self.restore_best_weights:
                self.best_weights = self.model.get_weights()
        else:
            if epoch > self.min_epochs:
                self.wait += 1
            if self.wait >= self.patience and epoch % self.cycle == 0:
                self.stopped_epoch = epoch
                self.model.stop_training = True
                if self.restore_best_weights:
                    if self.verbose > 0:
                        print('Restoring model weights from the end of the best epoch.')
                    self.model.set_weights(self.best_weights)


class FileLog(tf.keras.callbacks.Callback):
    """
    Saves a copy of a file at the beginning of training.
    The exact path to the file must be provided (but should only need to be added once per project).

    NB: main use is to save e.g. a config file that should be kept for reproduction.
    An easy use is to initialize the callback within the file to save and to specify the desired path:
        FileLog(path = __file__, to = path/to/file...)

    Note that to only needs to point to a directory, but also that the file cannot be renamed.
    """
    def __init__(self, path, to):
        super().__init__()
        self.path = path
        self.to = to

    def on_train_begin(self, logs=None):
        copy(self.path, self.to)


class AugmentedLogger(tf.keras.callbacks.CSVLogger):
    def __init__(self, **kwargs):
        super(AugmentedLogger, self).__init__(**kwargs)

        self.filename = self.filename.replace("\\.csv", f"_{datetime.datetime.now().strftime('%Y%m%d_%H%M%S')}.csv")
        self.epoch_time = None
        # self.total_time = None

    # def on_train_begin(self, logs=None):
    #     super().on_train_begin()
    #     self.total_time = time()

    def on_epoch_begin(self, epoch, logs=None):
        self.epoch_time = time()

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}

        def handle_value(k):
            is_zero_dim_ndarray = isinstance(k, np.ndarray) and k.ndim == 0
            if isinstance(k, six.string_types):
                return k
            elif isinstance(k, collections_abc.Iterable) and not is_zero_dim_ndarray:
                return '"[%s]"' % (', '.join(map(str, k)))
            else:
                return k

        if self.keys is None:
            self.keys = sorted(logs.keys())

        if self.model.stop_training:
            # We set NA so that csv parsers do not fail for this last epoch.
            logs = dict((k, logs[k]) if k in logs else (k, 'NA') for k in self.keys)

        if not self.writer:

            class CustomDialect(csv.excel):
                delimiter = self.sep

            # fieldnames = ['epoch', 'time', 'total_time'] + self.keys
            fieldnames = ['epoch', 'time'] + self.keys

            # if six.PY2:
            #     fieldnames = [unicode(x) for x in fieldnames]

            self.writer = csv.DictWriter(
                self.csv_file,
                fieldnames=fieldnames,
                dialect=CustomDialect)
            if self.append_header:
                self.writer.writeheader()

        row_dict = collections.OrderedDict({
            'epoch': epoch,
            'time': int(round(time() - self.epoch_time)),
            # 'total_time': time() - self.total_time
        })
        row_dict.update((key, handle_value(logs[key])) for key in self.keys)
        self.writer.writerow(row_dict)
        self.csv_file.flush()


class PerformanceLossWeighting(tf.keras.callbacks.Callback):
    """
    For multitask learning with several losses.
    Weighs the loss by the model's performance on each task at the previous iteration.
    The initial weights are the user-specified loss weights when compiling the model (or equal weights if not specified)
    
    monitor: performance metric for each task to monitor
    min_weight_prop: prevents the minimum loss weight from going below this proportion of the sum of weights (for instance,
        if min_weight_prop = .1, no loss will be weighted below 10% of the total weights
    alpha: auto-regressive coefficient for the loss update rule
    gamma: focal coefficient for the loss update rule

    The loss weight update rule is:
        weight(task, t+1) = alpha * performance(task, t) + (1 - alpha) * weight(task, t)
        weight(task, t+1) = - (1 - weight(task, t+1)) ** gamma * log(weight(task, t+1)
        weight(task, t+1) = weight(task, t+1) / sum(weights(tasks, t+1))
    """
    def __init__(self, monitor, alpha=.1, gamma=0., min_weight_prop=.1):
        super().__init__()
        self.monitor = monitor
        self.loss_weights = None
        self.alpha = alpha
        self.gamma = gamma
        self.min_weight_prop = min_weight_prop

        self.memory = None
        self.keys = None

    def on_train_begin(self, logs=None):
        if self.loss_weights is None:
            self.loss_weights = self.model.compiled_loss._user_loss_weights

    def on_train_batch_end(self, batch, logs=None):
        loss_weights = K.get_value(self.model.compiled_loss._user_loss_weights)
        if self.memory is None and logs is not None:
            self.memory = {key: [v for k, v in logs.items() if key in k and self.monitor in k][0] for key in loss_weights.keys()}
        elif logs is not None:
            self.memory = {key: self.alpha * [v for k, v in logs.items() if key in k and self.monitor in k][0] + (1 - self.alpha) * mem for key, mem in self.memory.items()}

            new_loss_weights = {key: - (1 - mem) ** self.gamma * tf.math.log(mem) for key, mem in self.memory.items()}
            new_loss_weights = {k: v / sum(new_loss_weights.values()) for k, v in new_loss_weights.items()}

            # This is to ensure that no weight is below self.min_weight_prop, to avoid some losses not contributing
            # total aimed weight with the low weights set at self.min_weight_prop
            coef = (1 - sum(1 for i in new_loss_weights.values() if i < self.min_weight_prop) * .1)
            # sum of the weights above self.min_weight_prop at this step
            coef /= sum(i for i in new_loss_weights.values() if i >= self.min_weight_prop)

            new_loss_weights = {key: max(v * coef, self.min_weight_prop) for key, v in new_loss_weights.items()}
            new_loss_weights = {k: v.numpy() if "numpy" in dir(v) else v for k, v in new_loss_weights.items()}

            self.model.compiled_loss._user_loss_weights = new_loss_weights

    def on_epoch_end(self, epoch, logs=None):
        print(K.get_value(self.model.compiled_loss._user_loss_weights))


class InitialLossWeighting(tf.keras.callbacks.Callback):
    """
    Multitask loss weighting rule based on the initial loss value. NOT PROPERLY TESTED
    """
    def __init__(self,
                 alpha=.5,
                 min_weight_prop=.1):
        super().__init__()
        self.loss_weights = None
        self.alpha = alpha
        self.min_weight_prop = min_weight_prop

        self.memory = None
        self.keys = None

    def on_epoch_begin(self, epoch, logs=None):
        self.loss_weights = None

    def on_train_batch_end(self, batch, logs=None):
        if self.loss_weights is None:
            self.loss_weights = K.get_value(self.model.compiled_loss._user_loss_weights)
        else:
            loss_weights = K.get_value(self.model.compiled_loss._user_loss_weights)
            new_loss_weights = {key: (loss_weights[key] / self.loss_weights[key])**self.alpha for key in loss_weights.keys()}

            new_loss_weights = {key: value / sum(new_loss_weights.values()) for key, value in new_loss_weights.items()}
            new_loss_weights = {k: v.numpy() if "numpy" in dir(v) else v for k, v in new_loss_weights.items()}

            self.model.compiled_loss._user_loss_weights = new_loss_weights

    def on_epoch_end(self, epoch, logs=None):
        print(K.get_value(self.model.compiled_loss._user_loss_weights))








