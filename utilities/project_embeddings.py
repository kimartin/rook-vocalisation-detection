"""
Predict and save predictions on new data.
"""
import os
import tensorflow as tf
import pickle
import re
from tqdm import tqdm

import config
from model.audio_generator import VariableOutputAudioGenerator

from utilities.custom_layers import mish
from utilities.pcen import PCEN
from utilities.functions import flatten, get_models
from utilities.output_manipulation import format_outputs

# Common root
root = "C:/Users/killian/Desktop/database_rooks"

# model(s) path
models = [f"{root}/runs/temp"]
models = flatten([[os.path.join(m, f) for f in os.listdir(m)] for m in models])

# data path
data_path = f"{root}/data/test"

# output path
out_path = f"{root}/embeddings"

# Retrieve model
model_path = get_models(models=models,
                        get_best_only=None,
                        # get_best_only=["val_ap",
                        #                "val_detection_ap",
                        #                "val_sexing_ap",
                        #                "val_identification_ap"],
                        )


# Genarator arguments
pred_params = config.val_params
pred_params.update(
    dict(
        duration=10,
        chunk_ovlp=0.,
    )
)
gen = VariableOutputAudioGenerator
gen_args = dict(n_channels=config.n_channels,
                **config.multiclass,
                **pred_params)

# For multiple models and comparison with true labels (if any are available)
if any([_.endswith('tsv') for _ in os.listdir(data_path)]):
    local_gen = gen(data=data_path,
                    **gen_args,
                    **{'identification': True,
                       'presence': True,
                       'detection': True,
                       'sexing': True},
                    sexing_mask=config.sexing_mask
                    )
    for m in tqdm(model_path, total=len(model_path)):
        pattern = re.search(pattern='[0-9]+_[0-9]+', string=m)

        if not f'{pattern.group()}_ovlp{int(pred_params["chunk_ovlp"]*100)}_test.pickle' in os.listdir(out_path):
            model = tf.keras.models.load_model(m,
                                               custom_objects={
                                                   "PCEN": PCEN,
                                                   "mish": mish,
                                               },
                                               compile=False)

            def make_embedding_model(model,
                                     embedding_name='embedding',
                                     keep_all=True):
                new_model_outs = {layer.name: layer.output for layer in model.layers if embedding_name in layer.name}

                if keep_all:
                    model_outs = {layer.name.split(sep='/')[0]: layer for layer in model.outputs}
                    new_model_outs.update(model_outs)

                return tf.keras.Model(inputs=model.inputs,
                                      outputs=new_model_outs)

            model = make_embedding_model(model, embedding_name='embedding', keep_all=True)

            _, true, predictions = format_outputs(model, data=local_gen)
            print({k: v.shape for k, v in true.items()})
            print({k: v.shape for k, v in predictions.items()})

            # save a copy
            with open(os.path.join(out_path,
                                   f'{pattern.group()}_ovlp{int(pred_params["chunk_ovlp"] * 100)}.pickle'),
                      'wb') as connection:
                pickle.dump(obj=[true, predictions], file=connection, protocol=pickle.HIGHEST_PROTOCOL)
        else:
            with open(os.path.join(out_path,
                                   f'{pattern.group()}_ovlp{int(pred_params["chunk_ovlp"] * 100)}_test.pickle'),
                      'rb') as connection:
                true, pred = pickle.load(connection)

        # true = {k: np.reshape(v, (-1, v.shape[-1] if v.ndim > 1 else 1)) for k, v in true.items()}
        # pred = {k: np.reshape(v, (-1, v.shape[-1] if v.ndim > 1 else 1)) for k, v in pred.items()}
        # pred = np.reshape(predictions['embedding_identification'], (-1, predictions['embedding_identification'].shape[-1]))
        #
        # embed_id = pred['embedding_identification']
        # pred_id = pred['identification']
        #
        # names = config.multiclass['classes']
        # col_key = plt.get_cmap('tab20', lut=20)
        # col_key = dict(zip(names, col_key.colors[np.array([0, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 14, 16, 18, 19]), :-1]))
        #
        # times = np.arange(true.shape[0])[np.sum(true, axis=-1) == 1]
        # hover_data = pd.DataFrame({'time': (times / 80).astype('str')})
        #
        # embed_id = embed_id[np.sum(true, axis=-1) == 1, :]
        # pred_id = pred_id[np.sum(true, axis=-1) == 1, :]
        # true = true[np.sum(true, axis=-1) == 1, :]
        # names_true = np.array(names)[np.nanargmax(true, axis=-1)]
        # names_pred = np.array(names)[np.nanargmax(pred_id, axis=-1)]
        #
        # hover_data['name'] = names_true
        #
        # fig, axs = plt.subplots(ncols=2)
        #
        # reducer = UMAP(n_neighbors=30,
        #                n_components=2,
        #                metric="euclidean",
        #                min_dist=0.25,
        #                random_state=42).fit(pred)
        #
        # # show(obj=uplt.interactive(reducer,
        # #                           # color_key=col_key,
        # #                           labels=names_true,
        # #                           background='black',
        # #                           point_size=1,
        # #                           hover_data=hover_data))
        #
        # uplt.points(reducer,
        #             color_key=col_key,
        #             background='black',
        #             labels=names_true,
        #             ax=axs[0])
        # # lines = [(reducer.embedding_[:-1, 0][np.diff(times) == 1], reducer.embedding_[1:, 0][np.diff(times) == 1]),
        # #          (reducer.embedding_[:-1, 1][np.diff(times) == 1], reducer.embedding_[1:, 1][np.diff(times) == 1])]
        # # axs[0].plot(*lines,
        # #             linewidth=.1, color='white')
        # uplt.points(reducer,
        #             color_key=col_key,
        #             background='black',
        #             labels=names_pred,
        #             ax=axs[1])
        # plt.show()






