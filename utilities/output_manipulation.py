import re
import tensorflow as tf
import numpy as np
from utilities.functions import listOfDict_to_dictOfList


def format_outputs(model,
                   data,
                   labels=None,
                   return_labels=False,
                   ):
    """
    Get the output(s) of a (list of) model(s) and format them into a single tensor per output.

    Note: this function assumes the model predictions are dict instances.

    :param model: Tensorflow Model instance, the model used to predict on data. Can also take a list of the same to
    return the average of the predictions of each model.
    :param data: Data on which to perform predictions. Can be a generator, an array or a list of arrays.
    :param labels: Ground truth labels. Overrides data if provided, so should stay None if data is a generator.
    :return: Tuple of length 3, containing the inputs to the network, the ground truth labels and the network
    predictions. In case of multi-output model, the latter two have the same structure as the network outputs (in
    particular, if the network outputs a dict, dicts will be returned)
    """
    if not isinstance(model, list):
        model = [model]

    if return_labels:
        if labels:
            inputs = data
            y_true = labels
            y_pred = [m.predict(x=data) for m in model]
        else:
            inputs, y_true = zip(*[d[:2] for d in data])
            y_true = listOfDict_to_dictOfList(y_true)
            y_true = {k: np.concatenate(v, axis=0) for k, v in y_true.items()}
            y_pred = [m.predict(x=data,) for m in model]
    else:
        inputs = [d[0] for d in data]
        y_true = None
        y_pred = [m.predict(x=data) for m in model]

    if len(model) == 1:
        y_pred = y_pred[0]
    else:
        y_pred = listOfDict_to_dictOfList(y_pred)
        y_pred = {k: np.mean(np.stack(v, axis=0), axis=0) for k, v in y_pred.items()}

    # Get indices of non-zero inputs (corresponding to padded samples) for later removal
    inputs = np.concatenate(inputs, axis=0)
    ind = np.squeeze(np.where(np.sum(inputs, axis=tuple(range(1, len(inputs.shape)))) > 0))
    inputs = inputs[ind]

    # Organize predictions (and preserve last axis even for 1-class outputs
    y_pred = {k: np.squeeze(v[ind], axis=tuple(i for i, d in enumerate(v.shape[:-1]) if d == 1)) for k, v in y_pred.items()}

    # Organize labels
    if y_true is not None:
        y_true = {k: np.squeeze(v[ind], axis=tuple(i for i, d in enumerate(v.shape[:-1]) if d == 1)) for k, v in y_true.items()}

    return inputs, y_true, y_pred


def make_embedding_model(model,
                         embedding_layer_names=["embedding"],
                         keep_original_output=True):
    """
    From a model object, create a new model object that returns not only the input model's outputs, but also a collection
    of outputs from the layers of the input models that have names containing one of the elements of embedding_layer_names.
    The outputs of the new model will be a dict of the original outputs with the corresponding layer names, with
    the additional layer outputs and their corresponding names.

    Create a new model that returns not
    :param model: A Keras / Tensorflow model object.
    :param embedding_layer_names: list or str. Gives the pattern(s) of layer names to include as additional outputs
    for the new model.
    :param keep_original_output: bool. If True, the new model's outputs will keep the original model's outputs. If False,
    only the layers found with embedding_layer_names will be included in the new model's outputs.
    :return: A Keras / Tensorflow model object.
    """

    if not isinstance(embedding_layer_names, list):
        embedding_layer_names = [embedding_layer_names]

    embeds = [layer.output for layer in model.layers if any([_ in layer.name for _ in embedding_layer_names])]
    if len(embeds) == 0:
        raise ValueError(f"No embedding layer found, i.e with name containing one of {embedding_layer_names}")

    # Should get the same name as the original outputs, otherwise get the actual output names?
    embeds = dict(zip([re.split(pattern="/", string=e.name)[0] for e in embeds], embeds))

    if keep_original_output:
        old_outputs = dict(zip([re.split(pattern="/", string=e.name)[0] for e in model.outputs],
                               model.outputs))
        # Create the new model that returns the embeddings in addition to its former outputs
        new_model = tf.keras.Model(inputs=model.inputs, outputs={**embeds, **old_outputs})
    else:
        new_model = tf.keras.Model(inputs=model.inputs, outputs=embeds)

    return new_model


def separate_embeddings_and_outputs(model,
                                    data,
                                    embedding_layer_names=['embedding'],
                                    labels=None):
    """
    Simple wrapper to separate the outputs and embeddings in a model create with make_embedding_model
    """
    _, labels, fit_features = format_outputs(model,
                                             data=data,
                                             labels=labels,
                                             )
    preds = {k: v for k, v in fit_features.items() if not any([_ in k for _ in embedding_layer_names])}
    embeddings = {k: v for k, v in fit_features.items() if any([_ in k for _ in embedding_layer_names])}

    # make embeddings.keys() match preds.keys() if at all possible
    for k_p in list(preds.keys()):
        for k_e in list(embeddings.keys()):
            if k_p in k_e:
                embeddings[k_p] = embeddings.pop(k_e)

    return labels, preds, embeddings
