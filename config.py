"""
Config file to parametrize model training and inference
"""

import os
import tensorflow as tf
import datetime
import tensorflow_addons as tfa
from tensorflow.keras import mixed_precision

from utilities.custom_layers import mish, Clip
from utilities.callbacks import PlotModel, AugmentedLogger, LateStopping, FileLog, InitialLossWeighting, \
    PerformanceLossWeighting
import gctf

#### Data paths ####
os.chdir("C:/Users/killian/Desktop/database_rooks")
data = "data"
path = "runs"
training = "training"
validation = "validation"
test = "test"

#### Model parameter loop setup
# NB: each parameter can be passed as a list. One model will be trained per combination of all the resulting lists
loop_parameters = dict(
    # frontend type:
    # "PoolConv", "MSD" or "D3"
    model_type=["MSD"],

    # "PCEN", "learnDRC", "dB/Decibel", "root"
    first_layer=["PCEN"],

    # any activation function can be passed here, but has to be a callable object and not an instance
    # e.g. tf.keras.activations.Relu instead of tf.keras.activations.Relu(**kwargs)
    # for activations with arguments, use functools.partials, e.g.: partial(tf.nn.leaky_relu, alpha=0.01)
    # strings can also be used as in Tensorflow (e.g. 'relu')
    activation=[
        mish,
        # 'relu'
    ],

    # possible values: "max", "rms", "avg"/"average" or a float for learned norm pooling
    pool_type=["conv"],

    # First conv3D or not: True/False
    # conv3d=[False],

    # whether or not to replace conv/activation pairs with gated layers: False, "glu" or "highway"
    # Conv2D: regular convolution
    # GLU = Conv_A(X) * sigmoid(Conv_B(X))
    # highway = Conv_A(X) + sigmoid(Conv_B(X)) + X * (1-sigmoid(Conv_B(x))
    conv_type=[
        tf.keras.layers.Conv2D
    ],

    # heads to add at the end of the network
    heads=[
        # Configurations in the paper, uncomment to train a corresponding network
        # ["detection"],
        # ["sexing"],
        # ["identification"],
        # ["presence"],
        ["identification", "detection", "presence", "sexing"],

        # Other configurations
        # [],
        # ["identification", "detection"],
        # ["identification", "presence"],
        # ["identification", "sexing"],
        # ["identification", "detection", "presence"],
        # ["identification", "detection", "sexing"],
        # ["identification", "sexing", "presence"],
    ],

    # optional outputs conditioning via elementwise combinations
    # can be None or a layer as a CLASS (to be instantiated within the network)
    combination_op=[
        # None,
        tf.keras.layers.Multiply,
        # tf.keras.layers.Minimum
    ],

    # variant: True adds the intermediate module between frontend and heads (one module, shared between heads)
    variant=[
        # False,
        True,
        # {'detection': False, 'identification': True, 'presence': True, 'sexing': False}
    ],

    # Fixes the seed used for all random processes during training. Given a non-None value, repeated runs with the
    # same parameters will behave the EXACT same way
    seed=[42],

)

#### Training policy ####
policy = mixed_precision.Policy('mixed_float16')
mixed_precision.set_global_policy(policy)

#### Layer parameters ####
# General
layer_kwargs = dict(
    trainable=True,
    dtype=tf.float32
)
# Conv layer specific
conv_kwargs = dict(
    kernel_regularizer=tf.keras.regularizers.l2(l2=1e-4),
    kernel_initializer="he_normal"
)

# PCEN initialisation
pcen_params = dict(
    alpha=.5,
    delta=1.,
    r=.3,
    eps=1E-9,
    s=.025,
    sd=0.5,
    learn_axis="frequency",
    absolute_sd=True,
    return_last=False
)
learn_drc_params = dict(alpha=1./3.)


#### Generator parameters ####
# General
params = dict(batch_size=32,
              wl=0.05,                          # window length in seconds
              ovlp=75,                          # overlap between successive windows
              duration=10,                      # duration of chunks in seconds
              window=tf.signal.hamming_window,  # window function
              n_mels=80,                        # number of mel coefficients
              dtype="float32",                  # type of generator output
              audio_format='wav|flac',          # pattern matching the sound files extension
              labels_format="tsv",              # pattern matching the label files extension
              power=2.,
              pre=1.,
              # fmin=200,
              )
# Task-specific
binary = dict(
    col=None
)
multiclass = dict(
    col="Source",
    exclude=["Inc", "Pls"],
    multilabel_class=[],
    classes=["Aristotle", "Balbo", "Bashir", "Braad", "Brain",
             "Bussel", "Cassandra", "Connelly", "Elie", "Feisty",
             "Fry", "Gigi", "Huxley", "Jolene", "Jonas", "Kafka",
             "Leo", "Merlin", "Osiris", "Pomme", "Siobhan", "Tom"]
    # classes=["Balbo", "Bashir", "Braad", "Brain", "Elie",
    #          "Feisty", "Gigi", "Jolene", "Jonas", "Kafka",
    #          "Merlin", "Osiris", "Pomme", "Siobhan", "Tom"]
)
sexing_mask = [[1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1],
               [0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0]]
# sexing_mask = [[1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1],
#                [0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0]]
n_channels = 6
n_classes = len(multiclass['classes'])

# Training generator
train_params = dict(**params,
                    negative_prop=0.2,
                    k=1.,   # >f 1: oversample randomly; # < 1: undersample randomly
                    )
# Validation generator
val_params = dict(**params,
                  chunk_ovlp=0,      # overlap between successive chunks
                  )


#### Model hyperparameters ####
# loss_arguments
loss_args = dict(
    # focal loss exponent
    gamma=2.,
    # cosine similarity penalty weight. Float between 0 and 1. Final loss will be loss * (1 + cos * penalty)
    cos=0.,
    # class weighting coefficient
    beta=0.,
    # label smoothing coefficient. Float between 0 and 1. Instead of 0/1, labels will be trained on as ls/1-ls
    label_smoothing=.05
)

# architecture arguments
model_parameters = dict(
    # expanded = False: sparrow architecture, True: larger sparrow achitecture (essentially all layers twice deeper),
    # 'heads': only applied to heads and last layer of frontend
    expanded=True,
    add_recurrent=True,
    add_attention=False,
    skip_connection=False,
    dropout_rate=0.2,
    renorm=True,
    final_activation="sigmoid",
    n=n_classes,
    # defines the actual combinations in the form of a dict, where keys are the outputs conditioned on the respective values
    # here for instance: the 'identification' output is conditioned on both 'detection' and 'detection' outputs, and
    # the 'sexing' output is conditioned on the 'detection' output
    combinations={"identification": ['presence', 'sexing'], 'sexing': ['detection']},
    add_reconstruct=False
)

#### Run hyperparameters
train_args = dict(epochs=50,
                  verbose=1)
# length of cycle for learning rate in epochs
lr_cycle = 6


# Optimiser
def optimiser_initialisation(steps_per_epoch,
                             cycle_length,
                             min_lr=1e-6,
                             max_lr=1e-3,
                             training="lr"):
    """
    Instantiates an optimiser for training, using the provided arguments

    :param steps_per_epoch: Number of iterations in one epoch. NB: can just use the __len__ method of a generator
    :param cycle_length: length of the learning rate cycling schedule, in epochs.
    :param min_lr: minimum learning rate value
    :param max_lr: maximum learning rate value
    :param training: 'lr' or anything else. 'lr' allows use of the function for a learning rate boundary test
    :return:
    """

    # defines the learning rate schedule
    lr_schedule = tfa.optimizers.Triangular2CyclicalLearningRate(
        initial_learning_rate=min_lr,
        maximal_learning_rate=max_lr,
        # step_size is between the two lr bounds in either direction, so we halve it to keep cycle_length consistent
        step_size=(cycle_length // 2) * steps_per_epoch,
    )

    # Inner optimizer: AdamW with gradient NORMALIZATION
    inner = tfa.optimizers.AdamW(learning_rate=min_lr if training == "lr" else lr_schedule, weight_decay=1e-4)
    inner.get_gradients = gctf.centralized_gradients_for_optimizer(inner)

    # Add the LookAhead mechanism
    optimiser = tfa.optimizers.Lookahead(inner)

    # Stochastic Weight Averaging
    optimiser = tfa.optimizers.SWA(optimiser=optimiser, start_averaging=0, average_period=steps_per_epoch*cycle_length)

    return optimiser


#### Augmentation ####
# NB: for augmentation, instances have shape (channels, time_bins, freq_bins)
channel_axis = 0
time_axis = 1
freq_axis = 2
augmenter = None
#     Augmenter(
#     p=.75,
#     transforms=dict(
#         shuffle_channels=partial(shuffle_slices, size=2, axis=channel_axis),
#         time_stretch=partial(array_stretch, n=10, axis=time_axis, p=.2),
#         frequency_stretch=partial(array_stretch, n=5, axis=freq_axis, p=.2),
#         random_volume_change=partial(random_volume_change, p=6),
#         replace_background=partial(replace_background, axis=channel_axis),
#     ),
#     k=3,
#     return_transforms=False
# )


# Callback arguments
callback_args = dict(
    mode="max",
    patience=15,
    min_delta=0,
    min_epochs=18,
    cycle=lr_cycle,
    save_best_only=True,
    save_weights_only=False,
    tensorboard=False,
    # loss_weighting_alpha=0.,
    # loss_weighting_gamma=1
)


#### Callbacks #####
def callbacks(path,
              patience,
              monitor="val_loss",
              mode="auto",
              min_delta=1e-4,
              verbose=0,
              min_epochs=18,
              cycle=1,
              save_weights_only=True,
              save_best_only=False,
              loss_weighthing_monitor=None,
              loss_weighting_alpha=0.,
              loss_weighting_gamma=0,
              tensorboard=False,
              ):

    path = os.path.join(path, datetime.datetime.now().strftime('%Y%m%d_%H%M%S'))
    # Creates the "checkpoints" folder to save snapshots in
    checkpoint = path + "/checkpoints/"
    if not os.path.exists(checkpoint):
        os.makedirs(checkpoint)

    # Stops in case of NA values
    nan_terminate = tf.keras.callbacks.TerminateOnNaN()

    # Stops when the network stops improving
    # NB: LateStopping is a modification of the EarlyStopping callback that adds two arguments:
    # min_epochs prevents stopping the training before that many epochs have passed
    # cycle only allows stopping on multiples of its value (e.g. if conditions are met to stopped on epoch 20 but cycle = 6,
    # the training will be stopped at epoch 24 if no better performance is reached)
    early_stop = LateStopping(monitor=monitor,
                              patience=patience,
                              min_delta=min_delta,
                              mode=mode,
                              verbose=verbose,
                              cycle=cycle,
                              min_epochs=min_epochs)
    # early_stop = tf.keras.callbacks.EarlyStopping(monitor=monitor,
    #                                               patience=patience,
    #                                               min_delta=min_delta,
    #                                               mode=mode,
    #                                               verbose=verbose)

    # Log run information
    csv_logger = AugmentedLogger(filename=os.path.join(path, "training.csv"))
    # Log a plot of the model
    plot = PlotModel(to_file=os.path.join(path, "model.png"))
    # Log the config file
    conf_log = FileLog(path=__file__, to=path)

    #
    # Checkpoint every epoch
    checkpoint = tf.keras.callbacks.ModelCheckpoint(
        filepath=checkpoint + "/ep{epoch:02d}.tf",
        save_weights_only=save_weights_only,
        monitor=monitor,
        mode=mode,
        save_best_only=save_best_only)

    callback_list = [
        early_stop,
        csv_logger,
        nan_terminate,
        plot,
        conf_log,
        checkpoint,
    ]

    # optionally allows creation of a Tensorboard dashboard to monitor the network
    # NB: currently does not quite work
    if tensorboard:
        callback_list.append(
            tf.keras.callbacks.TensorBoard(log_dir=os.path.join(path, "logs"), profile_batch='2, 10')
        )

    # tested two different loss weighting schemes for multitask learning
    if loss_weighthing_monitor is not None:
        dlw = PerformanceLossWeighting(
            monitor=loss_weighthing_monitor,
            alpha=loss_weighting_alpha,
            gamma=loss_weighting_gamma
        )
        callback_list.append(dlw)
    elif loss_weighting_alpha > 0.:
        dlw = InitialLossWeighting(
            alpha=loss_weighting_alpha
        )
        callback_list.append(dlw)

    return callback_list
