"""
Implements a variety of generators for training neural networks on audio data with corresponding annotations.
Features include : random sampling from long recordings, RANDOM sub- and over-sampling (which can be done class-wise if classes are found in the labels), additional negative sampling (i.e. extracting samples with only the negative class present) augmentation, transformation from audio clip to spectrogram to mel-spectrogram, and generation of label data from annotations. Optionally, masks can be generated if some classes are included in the exclude argument
"""

import re
import os
import tensorflow as tf
from tensorflow.keras.utils import Sequence
import tensorflow.keras.backend as K
import soundfile as sf
import librosa
import numpy as np
import math
import pandas as pd
from collections import Counter
from utilities.functions import merge_overlaps, flatten, preemphasis, dictOfList_to_listOfDict, listOfDict_to_dictOfList


class BaseAudioGenerator(Sequence):
    def __init__(self,
                 data,
                 batch_size,
                 duration,
                 wl,
                 ovlp,
                 window,
                 n_mels,
                 labels=None,
                 remove_offset=True,
                 fmin=0, fmax=np.Inf,
                 pre=0.,
                 power=1.,
                 augment_function=None,
                 training=False,
                 pad_type="zeros",
                 dtype="float32",
                 labels_format="tsv",
                 audio_format="wav",
                 n_channels=-1,
                 exclude=None,
                 multilabel_class=None,
                 negative_prop=0.,
                 phase=False,
                 k=1.,
                 **kwargs):
        """
        The base audio generator class from which all others in this script inherit. Should not be used by itself.

        Generates melspectrograms and labels to be fed into a neural network.
        Takes audio files as well as label files containing the start and end times of the vocalisations of interest.

        Generation of a batch involves randomly taking chunks out of all the audio files given as data, remembering the
        time stamps, then labelling them according to intersection with the given labels.

        :param data: list or str. Paths to each of the files corresponding to the dataset. If list, pass as is. If passed as str,
        checks if it's the path to a directory; if it is, take all the files within that directory, else pass as list of length 1.
        Files are then split into audio files (i.e. those with the extension given in "audio_format") or label files (i.e. those with
        the extension given in the "label_format" argument)
        :param batch_size: int. Batch size for training.
        :param col: str. Column of data carrying the class labels.
        :param duration: float or list. Determines the duration of the audio chunks. If given as a single float, every
        chunk in every batch will be of the same duration. If given as a list, a drawing will be done every batch to
        determine the duration of the chunks: for a list of length 2, draw from a uniform distribution bounded by the
        two values; for a list of length > 2, draw one of the values.
        :param wl: int. Length of a single frame of the melspectrogram, in samples.
        :param ovlp: float. The overlap between successive frames. If 0<ovlp<1, used as is. If 1<=ovlp, divided
        by 100 (considered as percent of wl)
        :param window: str of function object. The window function to use for the melspectrogram.
        :param n_mels: int. Number of mel-frequency bands in the melspectrogram. Mostly corresponds to dimensionality
        reduction (if set lower than or equal to wl * sr // 2 ).
        :param fmin: int. Minimum frequency (in Hz) for the melspectrogram.
        :param fmax: int. Maximum frequency (in Hz) for the melspectrogram. If not provided, will be determined from the
        sample rate sr of the files (such that fmax = int(min(sr)/2) ).
        :param power: int or float. Power to apply to the spectrogram prior to the conversion to melspectrogram.
        :param remove_offset: bool. Whether (True) or not (False) to remove the DC offset from samples (in practice,
        substract the mean of the sample from its values).
        :param augment_function: The function to use to perform data augmentation on X and y. Should be a function with
        all its parameters set beforehand, except for X and y, and returns a tuple of corresponding lists of augmented X
        and y. Defaults to None, in which case no augmentation is performed
        :param training: Whether or not the generator is intended for training (True) or validation/testing/prediction (False).
        :param kwargs: Further optional arguments
        """
        # determine channel axis for expansion at the end
        self.pad_type = pad_type
        self.channel_axis = 0 if K.image_data_format() == "channels_first" else -1
        self.time_axis = 2 if self.channel_axis == -1 else 3

        # generator parameters
        self.batch_size = batch_size
        self.duration = duration
        self.training = training
        self.augment_func = augment_function if augment_function is not None and self.training else None
        self.exclude = exclude or []
        self.multilabel_class = multilabel_class or []

        # data parameters
        self.k = k
        self.labels_format = labels_format
        self.audio_format = audio_format

        # audio
        self.data = data
        if isinstance(self.data, str):
            if bool(re.search(pattern=self.audio_format, string=self.data)):
                self.data = [self.data]
            else:
                self.data = [os.path.join(self.data, _) for _ in os.listdir(self.data)]
        self.data_names = [_ for _ in self.data if bool(re.search(pattern=self.audio_format, string=_)) and not _.startswith("._")]
        self.data_names = sorted(self.data_names)
        # prepares the connections
        # self.data_objects = [sf.SoundFile(name) for name in self.data_names]
        # self.data_objects = dict(zip(self.data_names, self.data_objects))

        # labels
        self.label_names = labels or data
        if isinstance(self.label_names, str):
            if bool(re.search(pattern=self.labels_format, string=self.label_names)):
                self.label_names = [self.label_names]
            else:
                self.label_names = [os.path.join(self.label_names, _) for _ in os.listdir(self.label_names)]
        self.label_names = [_ for _ in self.label_names if bool(re.search(pattern=self.labels_format, string=_)) and not _.startswith("._")]
        self.label_names = sorted(self.label_names) if len(self.label_names) > 0 else None

        # get relevant informations from the audio files
        self.data_info = [sf.info(file=_) for _ in self.data_names]
        self.lengths = dict(zip(self.data_names, [self.data_info[_].duration for _ in range(len(self.data_names))]))
        self.total = sum(self.lengths.values())
        # self.sr = dict(zip(self.data_names, [self.data_info[_].samplerate for _ in range(len(self.data_names))]))
        self.sr = [self.data_info[_].samplerate for _ in range(len(self.data_names))]
        if len(set(self.sr)) > 1:
            raise ValueError("Files of multiple sample rates are not currently supported")
        self.sr = self.sr[0]
        self.channels = dict(zip(self.data_names, [self.data_info[_].channels for _ in range(len(self.data_names))]))
        self.max_channels = max(n_channels, max(self.channels.values()))

        # make sure to match each audio file to a corresponding label file
        # here: find the date of each file
        dates = [re.search(pattern="[0-9]{8}", string=_).group() for _ in self.data_names]
        self.data_names = [_ for _ in self.data_names if any([re.search(pattern=i, string=_) for i in dates])]
        if self.label_names is not None and len(self.data_names) != len(self.label_names):
            raise ValueError(f"{len(self.data_names)} audio files and {len(self.label_names)} label files have been "
                             f"found, but the same number of each should be present. ")

        # create label data
        self.reserve_labels = None
        self.labels = None
        if self.label_names is not None:
            self.reserve_labels = [pd.read_table(_, engine="python") for _ in self.label_names]
            self.reserve_labels = [df.assign(name=self.data_names[i]) for i, df in enumerate(self.reserve_labels)]
            # concatenate into one data frame
            self.reserve_labels = pd.concat(self.reserve_labels, ignore_index=True, axis=0)
            self.reserve_labels.columns = map(str.lower, self.reserve_labels.columns)
            # define lower and upper bounds for start and end of chunks
            self.reserve_labels['max_start'] = self.reserve_labels['name'].map(self.lengths)
            self.reserve_labels['max_start'] = np.minimum(self.reserve_labels['start'],
                                                          self.reserve_labels['max_start'] - self.duration)
            self.reserve_labels['min_end'] = np.maximum(0,
                                                        self.reserve_labels['end'] - self.duration)
            self.labels = pd.DataFrame()
            # shuffle rows
            # self.on_epoch_end()

        # parameters for melspectrogram
        self.pre = pre
        self.phase = phase
        self.remove_offset = remove_offset
        self.wl = int(wl * self.sr)
        self.ovlp = ovlp if 0. < ovlp < 1. else ovlp / 100
        self.step = int(self.wl * (1-self.ovlp))
        self.window = window
        self.n_mels = n_mels
        self.power = power
        self.fmin = fmin
        self.fmax = min(fmax, self.sr // 2)
        self.dtype = dtype
        self.filters = librosa.filters.mel(sr=self.sr,
                                           n_fft=self.wl,
                                           n_mels=self.n_mels,
                                           fmin=self.fmin,
                                           fmax=self.fmax,
                                           htk=True)
        self.filters = tf.transpose(self.filters)

        self.fps = self.sr // self.step
        self.frame_dur = 1 / self.fps
        self.frames = self.duration * self.fps

        # Spectrogram largest shape
        # self.max_channels = 1
        self.max_shape = [self.max_channels, self.frames, self.wl // 2 + 1]

        # Set proportions of positive and negative samples per batch (ignored if self.col is None)
        self.negative_prop = negative_prop if self.training else 0.
        self.positive_prop = 1. - self.negative_prop
        self.positive_samples = int(self.batch_size * self.positive_prop)

        # Negative samples
        self.negative_samples = []
        if self.reserve_labels is not None and self.negative_prop > 0.:
            # concatenate into one data frame
            for n in self.data_names:
                interval_merge = merge_overlaps(self.reserve_labels.loc[self.reserve_labels.name == n],
                                                start="start", end="end")
                neg_samples = pd.DataFrame({
                    "name": [n] * (interval_merge.shape[0] + 1),
                    "start": [0] + interval_merge["end"].tolist(),
                    "end": interval_merge["start"].tolist() + [self.lengths.get(n)]
                })
                neg_samples = neg_samples[neg_samples["end"] - neg_samples["start"] > self.duration]
                self.negative_samples.append(neg_samples)
            self.negative_samples = pd.concat(self.negative_samples, ignore_index=True, axis=0)

    def __len__(self):
        """
        Returns the number of batches in an epoch of training.

        :return: int number of batches in an epoch
        """
        return math.ceil(self.reserve_labels.shape[0] * self.k / self.batch_size / self.positive_prop)

    def on_epoch_end(self):
        if self.reserve_labels is not None:
            self.labels = self.reserve_labels.sample(frac=self.k, replace=self.k > 1.).reset_index(drop=True)
            self.labels['epoch_start'] = np.random.uniform(low=self.labels['max_start'],
                                                           high=self.labels['min_end'],
                                                           size=self.labels.shape[0])

            if self.negative_prop > 0.:
                n = int(self.__len__() * self.batch_size - self.labels.shape[0])
                negative_labels = self.negative_samples.sample(n=n, replace=True)
                negative_labels['epoch_start'] = np.random.uniform(low=negative_labels['start'],
                                                                   high=negative_labels['end'] - self.duration,
                                                                   size=negative_labels.shape[0])
                negative_labels['class_id'] = -1
                self.labels = pd.concat([self.labels, negative_labels], ignore_index=True)

            self.labels = self.labels.sample(frac=1., replace=False).reset_index(drop=True)

    def target_read(self, name, start, duration):
        chunk, _ = sf.read(file=name,
                           start=int(self.sr * start),
                           frames=int(self.sr*duration),
                           always_2d=True,
                           dtype=self.dtype,
                           fill_value=0.)
        chunk = chunk.T
        return chunk

    def chunk_batch(self, duration, index):
        """
        This method should extract a mini-batch of data based on the duration and index of the mini-batch

        :return: tuple of 3 lists containing the names, offsets and data extracted
        """
        raise NotImplementedError("chunk_batch method not implemented")

    def label_gen(self, name, dur, start):
        """
        Defines the label generation method. Should take all the specified arguments.
        Overwrite by subclassing.
        """
        raise NotImplementedError("label_gen method not implemented")

    def time_frequency_transform(self, chunk):
        """
        This the method that creats the time-frequency representation of a given sample.
        The base method computes the magnitude spectrogram of an audio clip (with the default self.power = 1).

        :return: a tensor representing the time-frequency representation of a sample
        """
        chunk = tf.signal.stft(chunk,
                               frame_length=self.wl,
                               fft_length=self.wl,
                               frame_step=self.step,
                               window_fn=self.window,
                               pad_end=True)
        chunk = tf.pow(tf.abs(chunk), self.power)

        return chunk

    def __getitem__(self, index):
        """
        Generate one batch of data

        :returns: a tuple of 3 tensors contaning the mini-batch of spectrograms, the respective ground truth labels, and
        optionally masks that will be passed to sample_weight in loss computation
        """
        # Generate the raw batch
        names, starts, chunks = self.chunk_batch(self.duration, index)

        # Pre-emphasis
        if self.pre > 0.:
            # chunks = preemphasis(chunks, pre=self.pre, normalise=True)
            chunks = [preemphasis(c, pre=self.pre, normalise=True) for c in chunks]

        # STFT
        # chunks = self.time_frequency_transform(chunks)
        chunks = [self.time_frequency_transform(c) for c in chunks]

        # Labels
        if self.labels is None:
            y = None
            masks = None
        else:
            y, masks = zip(*list(map(self.label_gen, names, starts, [self.duration] * len(names))))

        # Data augmentation on the spectrograms, the labels, and the masks
        if self.augment_func and self.training:
            chunks, y, masks = self.augment_func(chunks, y, masks)

        if y is not None:
            y_ = np.zeros(shape=(self.batch_size, *y[0].shape), dtype=self.dtype)
            for i, sample in enumerate(y):
                y_[i] = sample
            y = y_
            y = tf.transpose(y, perm=[0, 2, 1])
            y = tf.expand_dims(y, axis=1)

        if masks is not None:
            if all([m is None for m in masks]) or y is None:
                masks = None
            else:
                masks_ = np.ones(shape=(self.batch_size, *([m.shape for m in masks if m is not None][0])), dtype=self.dtype)
                for i, m in enumerate(masks):
                    if m is not None:
                        masks_[i] = m
                masks = masks_

        chunks = tf.stack(chunks, axis=0)

        # Apply Mel Filterbank
        # chunks.shape: (batch_size, channels, time, freq)
        chunks = tf.matmul(chunks, self.filters)

        # get labels into the correct shapes
        chunks = tf.transpose(chunks, perm=[0, 3, 2, 1])

        return chunks, y, masks


class TrainingGenerator(BaseAudioGenerator):
    def __init__(self,
                 col=None,
                 classes=None,
                 training=True,
                 *args, **kwargs):
        super().__init__(training=training, *args, **kwargs)
        self.col = col.lower() if col else None

        # Class proportions
        if self.reserve_labels is not None:
            self.class_prop = self.reserve_labels.assign(duration=lambda row: row.end - row.start)
            if self.col:
                self.class_prop = self.class_prop.groupby(self.col)
            self.class_prop = self.class_prop.agg(samples=pd.NamedAgg(column="duration", aggfunc="count"),
                                                  duration=pd.NamedAgg(column="duration", aggfunc="sum"))
            # Drop excluded and multilabel classes
            self.class_prop = self.class_prop.drop(index=self.exclude + self.multilabel_class)
            if self.col:
                self.class_prop["prop"] = self.class_prop["duration"] / self.class_prop["duration"].sum()
            else:
                self.class_prop["prop"] = self.class_prop["duration"] / self.total

        # Class labels
        self.classes = None
        if classes:
            self.classes = sorted(classes)
        elif self.col is not None and self.reserve_labels is not None:
            self.classes = list(self.class_prop.index)
        if self.classes:
            self.classes = [c for c in self.classes if c not in [*self.exclude, *self.multilabel_class]]

        # if self.multilabel_class and self.labels is not None:
        #     # replace a row containing a multiclass label by multiple rows corresponding
        #     # to each class individually, otherwise copying the values (notably Starts and Ends)
        #     # self.reserve_labels = self.reserve_labels.assign(matches=self.labels["comment"].str.findall("|".join(self.classes)))
        #     self.reserve_labels = self.reserve_labels.explode('matches').fillna('')
        #     self.reserve_labels[self.col] = np.where(self.reserve_labels[self.col].isin(self.multilabel_class),
        #                                              self.reserve_labels['matches'],
        #                                              self.reserve_labels[self.col])

        # Number classes
        self.n_classes = len(self.classes) if self.classes else 1  # 1 for binary classification
        if self.reserve_labels is not None and self.classes is not None:
            self.class_id = dict(zip(self.classes, range(self.n_classes)))
            self.class_id.update(dict(zip(self.exclude, [-1] * len(self.exclude))))
            self.reserve_labels["class_id"] = self.reserve_labels[self.col].map(self.class_id).astype("int")

        # Prepare the labels
        self.on_epoch_end()

    def chunk_batch(self, duration, index):
        """
        :param duration: float, duration in seconds of the chunks
        :param index: int, index of batch

        :returns: names, starts, chunks : file names, start times and data corresponding to a batch of audio chunks
        """

        df = self.labels.iloc[range(index * self.batch_size, (index + 1) * self.batch_size)]
        starts = df['epoch_start'].tolist()
        names = df["name"].tolist()

        # chunks = [self.target_read(name=n, start=s, duration=duration) for n, s in zip(names, starts)]

        chunks = np.zeros(shape=(self.batch_size, self.max_channels, int(self.duration * self.sr)), dtype=self.dtype)
        for i, (s, n) in enumerate(zip(starts, names)):
            chunk_ = self.target_read(name=n, start=s, duration=duration)
            # chunk_ = chunk_[randrange(0, chunk_.shape[0])]  # take one channel at random
            chunks[tuple([i] + [slice(0, c) for c in chunk_.shape])] = chunk_
            # np.random.shuffle(chunks[i])  # shuffle the channels, including the added 0s

        return names, starts, chunks

    def label_gen(self, name, start, duration):
        """
        Label generator function for the generator.

        Returns an array of shape (number of classes, 1) per frame.
        NB: col and classes are passed from BaseAudioGenerator.

        :param name
        :param start: start of the chunk for which we are generating the label
        :param dur: duration of the chunk for which we are generating the label. If set to below 0; use the entire file.

        :return: an array of 0 and 1s, of shape (frames, ) if classes=None or exclusive_classes=True, else
        (len(classes), frames)
        """
        label_df = self.labels.loc[(self.labels.name == name) &
                                   (self.labels.end > start) &
                                   (self.labels.start <= start + duration), :].copy()

        # Clip so 0 is the start of the clip
        label_df[['start', 'end']] -= start
        label_df[['start', 'end']] = label_df[['start', 'end']].clip(0, duration)

        # take even partial frames as full presence (to also take a tiny, tiny bit of leeway)
        label_df['start'] = np.floor(label_df['start'] / self.frame_dur).astype('int')
        label_df['end'] = np.ceil(label_df['end'] / self.frame_dur).astype('int')

        # if self.col is None:
        #     labels = np.zeros(shape=(self.frames,), dtype=self.dtype)
        #     mask = None
        #     for s, e in zip(label_df["start"], label_df["end"]):
        #         labels[slice(s, e + 1)] = 1.
        # else:
        labels = np.zeros(shape=(self.n_classes, self.frames), dtype=self.dtype)
        mask = np.ones(shape=(self.frames,), dtype=self.dtype)
        for s, e, c, i in zip(label_df["start"], label_df["end"], label_df[self.col], label_df["class_id"]):
            if c in self.classes:
                labels[i, slice(s, e)] = 1.
            elif c in self.exclude:
                mask[slice(s, e)] = 0.

        return labels, mask


class AudioGenerator(TrainingGenerator):
    """
    Generator specifically for validation and inference.
    """
    def __init__(self,
                 chunk_ovlp=0,
                 *args,
                 **kwargs):
        """
        :param chunk_ovlp: Overlap between successive blocks, between 0 and 1.
        """
        super().__init__(training=False, *args, **kwargs)
        self.chunk_ovlp = chunk_ovlp
        if isinstance(self.duration, list):
            raise ValueError("Duration should be a single number (float or int) for validation.")

        self.names = [[n] * math.ceil(self.lengths[n]/self.duration) for n in self.data_names]
        self.names = flatten(self.names)
        self.starts = [np.arange(0, self.lengths[n], self.duration*(1-self.chunk_ovlp)) for n in self.data_names]
        self.starts = flatten(self.starts)

    def __len__(self):
        return math.ceil(self.total / self.duration / self.batch_size)

    def chunk_batch(self, duration, index=None):
        """
        For validation, reads the entire validation files.

        :return: tuple of the names, offsets and data extracted
        """
        ind = slice(index * self.batch_size, (index + 1) * self.batch_size)

        names = self.names[ind]
        starts = self.starts[ind]
        chunks = np.zeros(shape=(self.batch_size, self.max_channels, int(self.duration * self.sr)), dtype=self.dtype)
        for i, (s, n) in enumerate(zip(starts, names)):
            chunk_ = self.target_read(name=n, start=s, duration=duration)
            # chunk_ = chunk_[randrange(0, chunk_.shape[0])]
            chunks[tuple([i] + [slice(0, c) for c in chunk_.shape])] = chunk_
            # np.random.shuffle(chunks[i])

        return names, starts, chunks


############ Subclasses for more specific uses ############
class BaseVariableGenerator(BaseAudioGenerator):
    def __init__(self,
                 sexing_mask,
                 identification=False,
                 detection=False,
                 presence=False,
                 sexing=False,
                 add_reconstruct=False,
                 **kwargs):
        super().__init__(**kwargs)
        self.identification = identification
        self.detection = detection
        self.presence = presence
        self.sexing = sexing
        self.add_reconstruct = add_reconstruct
        self.sexing_mask = tf.transpose(tf.constant(sexing_mask, dtype=self.dtype))

    def __getitem__(self, index):
        chunks, y, mask = super().__getitem__(index)
        out_y, out_mask = dict(), dict()

        if y is not None:
            if self.detection:
                y_binary = tf.reduce_max(y, axis=self.channel_axis)
                if mask is not None:
                    # take the complement of the mask so that even excluded vocalizations are learned from
                    y_binary = tf.expand_dims(tf.maximum(tf.squeeze(y_binary, axis=1), 1 - mask), axis=1)
                out_mask.update({"detection": tf.ones(shape=(self.batch_size,))})
                out_y.update({"detection": y_binary})

            if self.identification:
                # uncomment to learn ONLY from vocalizations
                # mask_id = tf.reduce_max(y, axis=[1, -1])
                # if mask is not None:
                #     mask_id = tf.minimum(mask, mask_id)

                out_y.update({"identification": y})
                out_mask.update({"identification": mask})

            if self.presence:
                y_presence = tf.reduce_max(y, axis=self.time_axis, keepdims=True)
                out_y.update({"presence": y_presence})
                out_mask.update({"presence": tf.ones(shape=(self.batch_size,))})

            if self.sexing:
                y_sexing = tf.matmul(y, self.sexing_mask)
                y_sexing = tf.clip_by_value(y_sexing, 0., 1.)

                # uncomment to learn ONLY from vocalizations
                # mask_sexing = tf.reduce_max(y_sexing, axis=[1, -1])
                # if mask is not None:
                #     mask_sexing = tf.minimum(mask, mask_sexing)
                out_y.update({"sexing": y_sexing})
                out_mask.update({"sexing": mask})
        else:
            out_y = None

        chunks = tf.convert_to_tensor(chunks)

        if self.add_reconstruct:
            out_y.update({'reconstructed': chunks})
            out_mask.update({'reconstructed': tf.ones_like(chunks)})

        if out_y is not None:
            out_y = {k: tf.convert_to_tensor(v) if v is not None else None for k, v in out_y.items()}
        if out_mask is not None:
            out_mask = {k: tf.convert_to_tensor(v) if v is not None else None for k, v in out_mask.items()}

        return chunks, out_y, out_mask


# These two concrete classes simply inherit their methods from their respective parents classes, saving some code
class VariableOutputTrainingGenerator(BaseVariableGenerator, TrainingGenerator):
    pass


class VariableOutputAudioGenerator(BaseVariableGenerator, AudioGenerator):
    pass


### Different AudioGenerator that takes separate but synchronisable audio files and combines them
class SyncAudioGenerator(TrainingGenerator):
    """
    Generator specifically for validation and inference.
    """
    def __init__(self,
                 offsets,
                 chunk_ovlp=0,
                 *args,
                 **kwargs):
        """
        :param chunk_ovlp: Overlap between successive blocks, between 0 and 1.
        """
        super().__init__(training=False, *args, **kwargs)
        self.chunk_ovlp = chunk_ovlp
        if isinstance(self.duration, list):
            raise ValueError("Duration should be a single number (float or int) for validation.")

        self.offsets = np.array(offsets, dtype='int32')
        self.blocks = flatten([list(sf.read(_, always_2d=True, dtype=self.dtype)[0].T) for _ in self.data_names])
        self.block_length = int(self.duration*(1-self.chunk_ovlp)*self.sr)
        self.n_samples = np.max([len(w) + off for w, off in zip(self.blocks, self.offsets)])
        self.n_samples = int((self.n_samples // self.block_length + 1) * self.block_length)

        paddings = [(off, self.n_samples - (off + len(_))) for _, off in zip(self.blocks, self.offsets)]
        self.blocks = [np.pad(array=_, pad_width=pad, mode='constant') for _, pad in zip(self.blocks, paddings)]
        self.blocks = np.stack(self.blocks, axis=0)

        self.starts = np.arange(0, self.n_samples, self.block_length)
        # self.starts = flatten(self.starts)

    def __len__(self):
        return math.ceil(len(self.starts) / self.batch_size)

    def chunk_batch(self, duration, index=None):
        """
        For validation, reads the entire validation files.

        :return: tuple of the names, offsets and data extracted
        """
        ind = slice(index * self.batch_size, (index + 1) * self.batch_size)

        starts = self.starts[ind]
        chunks = [self.blocks[..., st + np.arange(self.block_length)] for st in starts]

        return None, starts, chunks


####
class MultiObjectiveGenerator(TrainingGenerator):
    def __init__(self,
                 cols,
                 detection=True,
                 presence=True,
                 classes=None,
                 **kwargs):
        super().__init__(**kwargs)

        self.detection = detection
        self.presence = presence
        self.cols = [c.lower() for c in cols]

        if self.detection and 'detection' in self.cols:
            raise ValueError('"detection" is True but'
                             '"detection" is already a column in the cols argument, which may cause confusions')
        if self.detection and 'presence' in self.cols:
            raise ValueError('"presence" is True but'
                             '"presence" is already a column in the cols argument, which may cause confusions')

        if isinstance(classes, dict) and Counter([k.lower() for k in classes.keys()]) == Counter(self.cols):
            self.classes = {k: v if v is not None else sorted(self.reserve_labels[k.lower()].unique()) for k, v in classes.items()}
        elif classes is None:
            self.classes = dict(zip(self.cols, [sorted(self.reserve_labels[col.lower()].unique()) for col in self.cols]))
        else:
            raise ValueError(f'classes should either be None (to use the values of the provided columns as possible '
                             f'classes) or a dict whose keys are the same as the provided cols arguments.'
                             f'Provided: cols={cols}, classes={classes}')

    def label_gen(self, name, start, duration):
        """
        Label generator function for the generator.

        Returns an array of shape (number of classes, 1) per frame.

        :param name
        :param start: start of the chunk for which we are generating the label
        :param dur: duration of the chunk for which we are generating the label. If set to below 0; use the entire file.

        :return: an array of 0 and 1s, of shape (frames, ) if classes=None or exclusive_classes=True, else
        (len(classes), frames)
        """
        label_df = self.labels.loc[(self.labels.name == name) &
                                   (self.labels.end > start) &
                                   (self.labels.start <= start + duration), :].copy()

        # Clip so 0 is the start of the clip
        label_df[['start', 'end']] -= start
        label_df[['start', 'end']] = label_df[['start', 'end']].clip(0, duration)

        # take even partial frames as full presence (to also take a tiny, tiny bit of leeway)
        label_df['start'] = np.floor(label_df['start'] / self.frame_dur).astype('int')
        label_df['end'] = np.ceil(label_df['end'] / self.frame_dur).astype('int')

        labels = {col: np.zeros(shape=(self.frames, self.n_classes[col]), dtype=self.dtype) for col in self.cols}
        mask = {col: np.ones(shape=self.frames, dtype=self.dtype) for col in self.cols}
        for col in self.cols:
            for s, e, c in zip(label_df['start'], label_df['end'], label_df[col]):
                if c in self.classes[col]:
                    labels[col][e:s, self.classes[col].index(c)] = 1.
                elif c in self.exclude[col]:
                    mask[col][e:s] = 0.

        return labels, mask

    def chunk_batch(self, duration, index):
        """
        :param duration: float, duration in seconds of the chunks
        :param index: int, index of batch

        :returns: names, starts, chunks : file names, start times and data corresponding to a batch of audio chunks
        """
        df = self.labels.iloc[range(index * self.batch_size, (index + 1) * self.batch_size)]
        starts = df['epoch_start'].tolist()
        names = df["name"].tolist()

        # chunks = [self.target_read(name=n, start=s, duration=duration) for n, s in zip(names, starts)]

        chunks = np.zeros(shape=(self.batch_size, self.max_channels, int(self.duration * self.sr)), dtype=self.dtype)
        for i, (s, n) in enumerate(zip(starts, names)):
            chunk_ = self.target_read(name=n, start=s, duration=duration)
            # chunk_ = chunk_[randrange(0, chunk_.shape[0])]  # take one channel at random
            chunks[tuple([i] + [slice(0, c) for c in chunk_.shape])] = chunk_
            # np.random.shuffle(chunks[i])  # shuffle the channels, including the added 0s

        return names, starts, chunks

    def fill_batch(self, input):
        out = np.zeros(shape=(self.batch_size, *input[0].shape), dtype=self.dtype)
        for i, inp in enumerate(input):
            out[i] = inp
        return out

    def __getitem__(self, index):
        """
        Generate one batch of data

        :returns: a tuple of 3 tensors contaning the mini-batch of spectrograms, the respective ground truth labels, and
        optionally masks that will be passed to sample_weight in loss computation
        """
        # Generate the raw batch
        names, starts, chunks = self.chunk_batch(self.duration, index)

        # Pre-emphasis
        if self.pre > 0.:
            # chunks = preemphasis(chunks, pre=self.pre, normalise=True)
            chunks = [preemphasis(c, pre=self.pre, normalise=True) for c in chunks]

        # STFT
        # chunks = self.time_frequency_transform(chunks)
        chunks = [self.time_frequency_transform(c) for c in chunks]

        # Labels
        if self.labels is None:
            y = None
            masks = None
        else:
            outputs = map(self.label_gen(names, starts, [self.duration] * len(names)))
            y, masks = dictOfList_to_listOfDict(outputs)

            y = {k: self.fill_batch(v) for k, v in y.items()}
            masks = {k: self.fill_batch(v) for k, v in masks.items()}

            if self.detection:
                # assumes a single detection output for all
                y['detection'] = tf.reduce_max(tf.concat(list(y.values()) + list([1 - m for k, m in masks.items()]),
                                                         axis=self.channel_axis),
                                               axis=self.channel_axis,
                                               keepdims=True)

            if self.presence:
                y.update({f'presence_{k}' if len(self.cols) > 1 else 'presence':
                              tf.reduce_max(v, axis=self.time_axis)
                          for k, v in y.items()
                          })



        # Data augmentation on the spectrograms, the labels, and the masks
        # if self.augment_func and self.training:
        #     chunks, y, masks = self.augment_func(chunks, y, masks)

        chunks = tf.stack(chunks, axis=0)

        # Apply Mel Filterbank
        # chunks.shape: (batch_size, channels, time, freq)
        chunks = tf.matmul(chunks, self.filters)

        # get labels into the correct shapes
        chunks = tf.transpose(chunks, perm=[0, 3, 2, 1])

        return chunks, y, masks



class OneVOneAudioGenerator(AudioGenerator):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.encoding = self.one_v_one_encoding_matrix()

    def one_v_one_encoding_matrix(self):
        K = len(self.classes)

        cols = list(range(K * (K - 1) // 2))
        rows_neg = flatten([list(range(j + 1, K)) for j in range(K)])
        rows_pos = flatten([[j] * (K - j - 1) for j in range(K - 1)])

        indices = tf.constant([rows_pos + rows_neg,
                               cols + cols],
                              dtype=tf.int64)
        indices = tf.transpose(indices)
        values = tf.ones(shape=(K * (K - 1) // 2,))
        values = tf.concat([values, -values], axis=0)

        M = tf.sparse.SparseTensor(indices=indices,
                                   values=values,
                                   dense_shape=(K, K * (K - 1) // 2))
        M = tf.sparse.reorder(M)
        return tf.sparse.to_dense(M)

    def __getitem__(self, item):
        chunks, y, masks = super().__getitem__(item)
        y = tf.matmul(y, self.encoding)
        return chunks, y, masks


class OneVOneTrainingGenerator(TrainingGenerator):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.encoding = self.one_v_one_encoding_matrix()

    def one_v_one_encoding_matrix(self):
        K = len(self.classes)

        cols = list(range(K * (K - 1) // 2))
        rows_neg = flatten([list(range(j + 1, K)) for j in range(K)])
        rows_pos = flatten([[j] * (K - j - 1) for j in range(K - 1)])

        indices = tf.constant([rows_pos + rows_neg,
                               cols + cols],
                              dtype=tf.int64)
        indices = tf.transpose(indices)
        values = tf.ones(shape=(K * (K - 1) // 2,))
        values = tf.concat([values, -values], axis=0)

        M = tf.sparse.SparseTensor(indices=indices,
                                   values=values,
                                   dense_shape=(K, K * (K - 1) // 2))
        M = tf.sparse.reorder(M)
        return tf.sparse.to_dense(M)

    def __getitem__(self, item):
        chunks, y, masks = super().__getitem__(item)
        y = tf.matmul(y, self.encoding)
        return chunks, y, masks


class WaveformGenerator(AudioGenerator):
    def time_frequency_transform(self, chunk):
        return chunk


class WaveformTrainingGenerator(TrainingGenerator):
    def time_frequency_transform(self, chunk):
        return chunk



if __name__ == "__main__":
    gen = MultiObjectiveGenerator(
        batch_size=32,
        duration=10,
        wl=0.025,
        ovlp=75,
        window=tf.signal.hamming_window,
        n_mels=80,
        data='C:/Users/killian/Desktop/database_rooks/data/attempt_multiobjective',
        cols=["Source", "Sex", "Song"],
        classes={"Source": None, "Sex": None, "Song": None},
        negative_prop=.2)

    print(gen[0])
