# architecture based on 10.23919/EUSIPCO.2017.8081512

import warnings

import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras.layers import \
    Layer, Input, \
    BatchNormalization, LeakyReLU, \
    Conv1D, SpatialDropout1D, ZeroPadding1D, \
    Conv2D, SpatialDropout2D, ZeroPadding2D, AvgPool2D, MaxPool2D, \
    Conv3D, SpatialDropout3D, ZeroPadding3D, \
    LSTM, GRU, Conv2DTranspose, Conv1DTranspose, Conv3DTranspose

from utilities.custom_layers import *


# class SAMModel(tf.keras.Model):
#     def train_step(self, data):
#         # Unpack the data. Its structure depends on your model and
#         # on what you pass to `fit()`.
#         x, y, _ = data
#         indices = tf.reduce_all(x > 0, axis=range(1, len(x.shape)-1))
#         sizes = tf.reduce_sum(tf.cast(indices, tf.int32), axis=-1)
#         batch_size = x.shape[0]
#
#         x = tf.unstack(x, axis=0)
#         x = [tf.gather(x_, axis=-1, indices=tf.where(idx)) for x_, idx in zip(x, indices)]
#         x = tf.transpose(tf.concat(x, axis=-2), perm=[2, 0, 1, 3])
#         x = tf.split(x, axis=0, num_or_size_splits=[batch_size] * (x.shape[0] // batch_size) + [x.shape[0] % batch_size])
#
#         with tf.GradientTape() as tape:
#             # forward pass
#             y_pred = dict(zip(y.keys(), [[] for k in y]))
#             for _ in x:
#                 pred = self(_, training=True)
#                 for key in pred:
#                     y_pred[key].append(pred[key])
#             y_pred = {k: tf.concat(v, axis=0) for k, v in y_pred.items()}
#             y_pred = {k: tf.stack([tf.reduce_max(_, axis=0) for _ in tf.split(v, axis=0, num_or_size_splits=sizes)], axis=0) for k, v in y_pred.items()}
#
#             # Compute the loss value
#             # (the loss function is configured in `compile()`)
#             loss = self.compiled_loss(y, y_pred, regularization_losses=self.losses)
#
#         # Compute gradients
#         trainable_vars = self.trainable_variables
#         gradients = tape.gradient(loss, trainable_vars)
#         # Update weights
#         self.optimizer.apply_gradients(zip(gradients, trainable_vars))
#         # Update metrics (includes the metric that tracks the loss)
#         self.compiled_metrics.update_state(y, y_pred)
#         # Return a dict mapping metric names to current value
#         return {m.name: m.result() for m in self.metrics}


def SparrowLayerFunction(
        x,
        filters,
        kernel_size,
        padding=None,
        strides=1,
        dilation_rate=1,
        activation=None,
        activation_args=None,
        causal=False,
        conv_type=Conv2D,
        batchnorm=True,
        renorm=False,
        dropout_rate=0.,
        conv_kwargs=None,
        squeeze_excite=None,
        ):
    """
    Wrapper function over the composition SpatialDropout -> ZeroPadding -> Conv -> BatchNorm, with other arguments
    controlling the parameters of each.

    :param x: A Model object. The SparrowLayer operation will be added at the end of it.
    :param filters: int. Number of filters for the Conv operation.
    :param kernel_size: int or tuple of int. The convolutional kernel.
    :param strides: int or tuple of int. The convolutional strides.
    :param dilation_rate: int or tuple of int. The convolutional dilation rate.
    :param padding: None or list of int. Gives the axes to zero-pad over.
    :param activation: callable or str. The activation function for the convolution.
    :param activation_args: None or dict. Additional arguments passed to activation if activation is callable.
    :param causal: bool. Affects both the convolution and the zero padding operations. If True, both will be modified to
    be causal by zero-padding at the beginning of the second-to-last axis (which is assumed to be the time axis, the
    last being the filter axis). If False, zero-padding is done half at the beginning and half at the end of the
    second-to-last axis.
    :param conv_type: None or Layer class. Must be non-instantiated. If not None, one Conv1D, Conv2D or Conv3D, giving
    the type of operations to add to the model. If None, is inferred from len(kernel_size) with a warning.
    :param gate: One of None, "glu", "highway". If None, do a normal convolution, otherwise creates a gated convolution.
    The difference between "glu" and "highway" is the following:
            "glu": Sigmoid(ConvA(x)) * ConvB(x)
            "highway": Sigmoid(ConvA(x)) * ConvB(x) + (1 - Sigmoid(ConvA(x)) * x
    Basically, "highway" can allow x to flow through untransformed.
    :param batchnorm: bool. Whether (True) or not (False) to apply BatchNormalization after the convolution.
    BatchNormalization is used with default parameters in Tensorflow, except for renorm below.
    :param renorm: bool. If False, use BatchNormalization. If True, use BatchRenormalization.
    :param dropout_rate: float, 0. <= dropout_rate < 1. The dropout rate to use for SpatialDropout. Will randomly drop
    this proportion of the (entire) feature maps output by the layer before it at each learning step.
    As a special case, dropout_rate = 0. skips the inclusion of the SpatialDropout layer.
    :param conv_kwargs: None or dict. Additional convolution arguments
    :param squeeze_excite: None or int. If not None, adds a SqueezeExcitation block followign this specification.

    :return: a Model object, x with a SparrowLayer operation on top.
    """

    channel_axis = -1 if K.image_data_format() == "channels_last" else 1

    # ConvND args
    conv_kwargs = conv_kwargs or dict()
    # Activation args
    activation_args = activation_args or dict()

    conv_dim = conv_type.__name__[-2:]

    # Dropout
    if dropout_rate > 0.:
        dropout_layer = getattr(tf.keras.layers, f'SpatialDropout{conv_dim}')
        x = dropout_layer(rate=dropout_rate)(x)

    # Zero-padding
    if isinstance(padding, list) and len(padding) > 0:
        pad_layer = getattr(tf.keras.layers, f'ZeroPadding{conv_dim}')
        padds = [0] * len(kernel_size)
        for p in padding:
            padds[p] = (kernel_size[p] + 2 * dilation_rate - 1, 0) if causal else kernel_size[p] // 2 + dilation_rate - 1
        x = pad_layer(padding=padds)(x)

    # Conv operation
    x = conv_type(
        filters=filters,
        kernel_size=kernel_size,
        activation=activation if not isinstance(activation, type(Layer)) else None,
        padding=padding if isinstance(padding, str) else "valid",
        strides=strides,
        dilation_rate=dilation_rate,
        **conv_kwargs
    )(x)
    if activation is not None and isinstance(activation, type(Layer)):
        x = activation(**activation_args)(x)

    # Batch Normalization
    if batchnorm:
        x = BatchNormalization(axis=channel_axis, renorm=renorm)(x)

    # Squeeze-Excitation block
    if squeeze_excite is not None:
        x = SqueezeExcite(
            r=squeeze_excite,
            activation=activation,
            activation_args=activation_args,
            conv_kwargs=conv_kwargs,
        )(x)

    return x


def pool_function(x,
                  pool_type,
                  pool_size,
                  activation=None,
                  channel_axis=-1):
    if pool_type == "max":
        x = MaxPool2D(pool_size=pool_size)(x)
    elif pool_type == "average":
        x = AvgPool2D(pool_size=pool_size)(x)
    elif pool_type == "rms":
        x = LearnedNormPool2D(pool_size=pool_size, power=2., trainable=False)(x)
    elif isinstance(pool_type, float):
        x = LearnedNormPool2D(pool_size=pool_size, power=pool_type, trainable=True)(x)
    elif pool_type == "conv":
        x = Conv2D(filters=x.shape[channel_axis],
                   groups=x.shape[channel_axis],
                   kernel_size=pool_size,
                   strides=pool_size,
                   # kernel_constraint=[tf.keras.constraints.UnitNorm(), tf.keras.constraints.NonNeg()],
                   activation=activation)(x)
    else:
        warnings.warn(f"pool_type was {pool_type}, which is not covered by this function. Accordingly,"
                      f"input was returned without any action."
                      f"To actually do a pooling operation, pass one of 'max', 'average', 'rms', 'conv' or a float"
                      f"to pool_type argument.")
    return x


def ConvPoolSparrowFrontEnd(x,
                            pool_type="max",
                            activation=LeakyReLU,
                            activation_args=None,
                            padding=[1],
                            causal=False,
                            expanded=False,
                            batchnorm=True,
                            renorm=False,
                            squeeze_excite=None,
                            conv_type=Conv2D,
                            **kwargs):
    """
    Creates a SparrowNet front-end as a linear stack of convolution and pooling layers.

    :param inp: Input to the model. Either an Input layer instance, or another model.
    :param pool_type: str. One of "max", "rms", "average", "avg", "conv". Gives the type of pooling to use:
    "max" is MaxPooling, "rms" is RMS pooling, "average" and "avg" are both AveragePooling, and "conv" is convolutional
    pooling (a strided convolution with stride equal to the kernel_size).
    :param activation: callable or str. Activation function, passed to SparrowLayerFunction
    :param activation_args: None or dict. Activation function arguments, passed to SparrowLayerFunction
    :param causal: bool. Whether (True) or not (False) convolution should be causal. Passed to SparrowLayerFunction
    :param dropout_rate: float. SpatialDropout rate. Passed to SparrowLayerFunction
    :param expanded: bool. If False, all layers are created with 64 filters. If True, all layers are created with an
    expanded number of filters.
    :param renorm: Passed to SparrowLayerFunction
    :param kwargs: Passed to SparrowLayerFunction

    :return: A Model instance of the SparrowNet architecture.
    """
    channel_axis = -1 if K.image_data_format() == "channels_last" else 1
    kernel_size = (3, 3)
    pool_size = (3, 1)

    if activation == LeakyReLU and not activation_args:
        activation_args = {"alpha": 0.01}

    # Arguments used by all sparrow layers
    general_args = dict(
        activation=activation,
        activation_args=activation_args,
        # channel_axis=channel_axis,
        causal=causal,
        batchnorm=batchnorm,
        renorm=renorm,
        padding=padding,
        conv_kwargs=kwargs,
        squeeze_excite=squeeze_excite,
        conv_type=conv_type
    )

    x = SparrowLayerFunction(x, filters=64, kernel_size=kernel_size, **general_args)
    x = SparrowLayerFunction(x, filters=64, kernel_size=kernel_size, **general_args)
    x = SparrowLayerFunction(x, filters=64, kernel_size=kernel_size, **general_args)

    x = pool_function(x, pool_type=pool_type, pool_size=pool_size, activation=None, channel_axis=channel_axis)

    filters = 128 if expanded else 64
    x = SparrowLayerFunction(x, filters=filters, kernel_size=kernel_size, **general_args)
    x = SparrowLayerFunction(x, filters=filters, kernel_size=kernel_size, **general_args)

    freq_summary_kernel = (x.shape[1 if channel_axis == -1 else -1] - 2, 3)
    x = SparrowLayerFunction(x, filters=filters, kernel_size=freq_summary_kernel, **general_args)

    x = pool_function(x, pool_type=pool_type, pool_size=pool_size, activation=None, channel_axis=channel_axis)

    return x


def mixing_scale_layer(x,
                       kernel_size,
                       growth,
                       dilation,
                       activation,
                       activation_args=None,
                       batchnorm=True,
                       renorm=False,
                       conv_type=tf.keras.layers.Conv2D,
                       padding=None,
                       causal=False,
                       dropout_rate=0.,
                       squeeze_excite=None,
                       conv_kwargs=None,
                       **kwargs
                       ):
    if activation_args is None:
        activation_args = dict()
    if conv_kwargs is None:
        conv_kwargs = dict()

    all_args = dict(
        kernel_size=kernel_size,
        filters=growth,
        padding=padding,
        strides=(1, 1),
        activation=activation,
        activation_args=activation_args,
        causal=causal,
        conv_type=conv_type,
        batchnorm=batchnorm,
        renorm=renorm,
        dropout_rate=dropout_rate,
        conv_kwargs=conv_kwargs,
        squeeze_excite=squeeze_excite,
    )
    x_list = [x] + [SparrowLayerFunction(x, dilation_rate=d, **all_args) for d in dilation]

    out = tf.keras.layers.Concatenate()(x_list)

    return out


def multi_scale_dense_module(
        x,
        growth,
        dilation,
        depth,
        activation,
        kernel_size,
        c=0.,
        activation_args=None,
        batchnorm=True,
        renorm=False,
        conv_type=tf.keras.layers.Conv2D,
        padding=None,
        causal=False,
        dropout_rate=0.,
        squeeze_excite=None,
        conv_kwargs=None,
        **kwargs
):
    all_args = dict(
        batchnorm=batchnorm,
        renorm=renorm,
        conv_type=conv_type,
        padding=padding,
        causal=causal,
        dropout_rate=dropout_rate,
        squeeze_excite=squeeze_excite,
        conv_kwargs=conv_kwargs,
    )
    for j in range(depth):
        x = mixing_scale_layer(x=x,
                               kernel_size=kernel_size,
                               growth=growth,
                               dilation=dilation,
                               activation=activation,
                               activation_args=activation_args,
                               **all_args
                               )

        if c > 0:
            x = SparrowLayerFunction(
                x,
                filters=c,
                kernel_size=(1, 1),
                activation=None,
                activation_args=None,
                **all_args
            )

    return x


def MSD_Sparrow(
        x,
        activation,
        activation_args=None,
        pool_type='max',
        growth=16,
        depth=2,
        dilation=[1, 2, 4],
        batchnorm=True,
        squeeze_excite=None,
        renorm=True,
        expanded=False,
        causal=False,
        **kwargs,
):

    assert pool_type in ["max", "rms", "average", "avg", "conv"] or isinstance(pool_type, float)

    channel_axis = -1 if K.image_data_format() == "channels_last" else 1
    pool_size = (3, 1)

    if activation == LeakyReLU and not activation_args:
        activation_args = {"alpha": 0.01}

    # Arguments used by all sparrow layers
    general_args = dict(
        activation=activation,
        activation_args=activation_args,
        causal=causal,
        conv_kwargs=kwargs,
        batchnorm=batchnorm,
        renorm=renorm,
        squeeze_excite=squeeze_excite,
    )
    mds_args = dict(
        growth=growth,
        dilation=dilation,
        depth=depth,
    )

    x = SparrowLayerFunction(x, filters=64, kernel_size=(3, 3), padding=[1], **general_args,)

    x = multi_scale_dense_module(x=x, kernel_size=(3, 3),  c=0., padding="same", **mds_args, **general_args)

    x = pool_function(x, pool_type=pool_type, pool_size=pool_size, activation=None, channel_axis=channel_axis)

    x = multi_scale_dense_module(x=x, kernel_size=(3, 3),  c=0., padding="same", **mds_args, **general_args)

    freq_summary_kernel = (x.shape[1 if channel_axis == -1 else -1] - 2, 3)
    x = SparrowLayerFunction(x, filters=128 if expanded else 64, kernel_size=freq_summary_kernel, padding=[1], **general_args,)

    x = pool_function(x, pool_type=pool_type, pool_size=pool_size, activation=None, channel_axis=channel_axis)

    return x


def Head(model,
         n,
         expanded=False,
         dropout_rate=0.,
         general_args=None,
         final_activation="sigmoid",
         add_recurrent=False,
         add_attention=False,
         skip_connection=False,
         output_bias=None,
         name=None,
         combination=False,
         provided_bias=None,
         **kwargs):

    if provided_bias is not None and not isinstance(provided_bias, list):
        provided_bias = list(provided_bias)

    args = {k: v for k, v in general_args.items()} or dict()

    embed_name = f"embedding_{name}" if name else "embedding"
    # if "conv_kwargs" in args.keys():
    #     args["conv_kwargs"] = {**args["conv_kwargs"], "name": embed_name}

    x = SparrowLayerFunction(
        model,
        filters=512 if expanded else 64,
        kernel_size=(1, 1),
        # dropout_rate=dropout_rate,
        **args,
    )

    if add_recurrent:
        y = Squeeze(axis=1)(x)
        y = tf.keras.layers.Bidirectional(
            LSTM(
                units=x.shape[-1] // 2,
                return_sequences=True,
            )
        )(y)
        if skip_connection:
            y = Expand(axis=1)(y)
            x = tf.keras.layers.Concatenate(name=embed_name)([x, y])
        else:
            x = Expand(axis=1, name=embed_name)(y)
    elif add_attention:
        # takes too much memory
        x = tf.keras.layers.MultiHeadAttention(num_heads=8, key_dim=x.shape[-1] // 16)(x, x)

    if 'kernel_initiliazer' in general_args['conv_kwargs'].keys():
        kernel_initializer = general_args['conv_kwargs']['kernel_initializer']
    elif 'kernel_initializer' in kwargs.keys():
        kernel_initializer = kwargs['kernel_initializer']
    else:
        # default value for Conv2D
        kernel_initializer = 'glorot_uniform'

    if dropout_rate > 0.:
        x = SpatialDropout2D(rate=dropout_rate)(x)

    x_final = Conv2D(filters=n,
                     kernel_size=(1, 1),
                     bias_initializer=output_bias,
                     name=name if provided_bias is None and not combination else None,
                     dtype=tf.float32,
                     activation=final_activation if provided_bias is None else None,
                     kernel_initializer=kernel_initializer,
                     **kwargs)(x)
    if provided_bias is not None:
        x_final = tf.keras.layers.Add(dtype=tf.float32)([x_final, provided_bias])
        x_final = tf.keras.layers.Activation(activation=final_activation, dtype=tf.float32, name=name if combination else None)(x_final)

    return x, x_final


def PresenceHead(x,
                 n,
                 expanded=False,
                 dropout_rate=0.,
                 general_args=None,
                 final_activation="sigmoid",
                 output_bias=None,
                 name="presence",
                 provided_bias=None,
                 **kwargs,
                 ):
    args = {k: v for k, v in general_args.items() if k not in ['padding']}

    filters = 512 if expanded else 64
    kernel_size = (1, 9)
    strides = (1, 5)
    x = SparrowLayerFunction(x, filters=filters, kernel_size=kernel_size, strides=strides, **args, **kwargs)
    x = SparrowLayerFunction(x, filters=filters, kernel_size=kernel_size, strides=strides, **args, **kwargs)
    x = SparrowLayerFunction(x, filters=filters, kernel_size=kernel_size, strides=strides, **args, **kwargs)

    x = Conv2D(filters=filters,
               kernel_size=(1, x.shape[2]),
               activation=general_args['activation'],
               name=f"embedding_{name}",
               **kwargs)(x)

    if dropout_rate > 0.:
        x = SpatialDropout2D(rate=dropout_rate)(x)

    x_final = Conv2D(filters=n,
                     kernel_size=1,
                     bias_initializer=output_bias,
                     name=name if provided_bias is None else None,
                     dtype=tf.float32,
                     activation=final_activation if provided_bias is None else None,
                     **kwargs)(x)
    if provided_bias is not None:
        x_final = tf.keras.layers.Add(dtype=tf.float32)([x_final, provided_bias])
        x_final = tf.keras.layers.Activation(action=final_activation, dtype=tf.float32, name=name)(x_final)

    return x, x_final


def Reconstruct_Head(x,
                     n,
                     filters=128,
                     activation='relu',
                     final_activation='relu',
                     batchnorm=True,
                     renorm=True,
                     name='reconstructed',
                     **kwargs):

    x = Conv2DTranspose(filters=filters,
                        kernel_size=(3, 1),
                        padding='valid',
                        activation=activation,
                        **kwargs)(x)

    if batchnorm: x = BatchNormalization(renorm=renorm)(x)

    x = Conv2DTranspose(filters=filters,
                        kernel_size=(3, 1),
                        padding='valid',
                        activation=activation,
                        **kwargs)(x)

    if batchnorm: x = BatchNormalization(renorm=renorm)(x)

    x = Conv2DTranspose(filters=filters,
                        kernel_size=(4, 4),
                        strides=(2, 1),
                        padding='same',
                        activation=activation,
                        ** kwargs)(x)

    if batchnorm: x = BatchNormalization(renorm=renorm)(x)

    x = Conv2DTranspose(filters=filters,
                        kernel_size=(4, 4),
                        strides=(2, 1),
                        padding='same',
                        activation=activation,
                        ** kwargs)(x)

    if batchnorm: x = BatchNormalization(renorm=renorm)(x)

    x = Conv2DTranspose(filters=filters,
                        kernel_size=(4, 4),
                        strides=(2, 1),
                        padding='same',
                        activation=activation,
                        ** kwargs)(x)

    if batchnorm:
        x = BatchNormalization(renorm=renorm)(x)

    x = Conv2DTranspose(filters=filters,
                        kernel_size=(4, 4),
                        strides=(2, 1),
                        padding='same',
                        activation=activation,
                        name=f'{name}_embbeding',
                        ** kwargs)(x)

    x = Conv2D(filters=n, kernel_size=1, activation=final_activation, name=name, dtype=tf.float32, **kwargs)(x)

    return x


def SparrowNet(inputs=None,
               model_type="PoolConv",
               shape=None,
               first_layer=None,
               first_layer_args=None,
               n=1,
               pool_type="max",
               conv_type=tf.keras.layers.Conv2D,
               activation=LeakyReLU,
               activation_args=None,
               final_activation="softmax",
               name="SparrowNet",
               causal=False,
               dropout_rate=0.,
               output_bias=None,
               expanded=False,
               renorm=False,
               heads=["detection", "identification", "presence"],
               combinations=None,
               combination_op=None,
               add_recurrent=False,
               skip_connection=False,
               add_attention=False,
               variant=False,
               squeeze_excite=None,
               sexing_mask=None,
               add_reconstruct=False,
               **kwargs):

    # Check that we actually provide output(s)
    if len(heads) == 0:
        raise ValueError("No outputs were provided. Please provide a non-empty list comprised of any combination of:"
                         "'detection', 'identification', 'presence', 'sexing'")

    # Variants
    variant_keys = ['detection', 'identification', 'sexing', 'presence']
    if isinstance(variant, bool):
        variant = {key: variant if key in heads else False for key in variant_keys}
    elif isinstance(variant, list):
        variant = {key: v if key in heads else False for key, v in zip(heads, variant_keys)}

    if combinations is None:
        combinations = dict()

    if model_type not in ["PoolConv", "D3", "MSD"]:
        raise ValueError(f"Accepted values for model_type are 'PoolConv', 'D3', and 'MSD', but '{model_type}' was provided.")

    if not isinstance(add_recurrent, dict):
        add_recurrent = {k: add_recurrent for k in ['identification', 'detection', 'sexing']}
    if not isinstance(add_attention, dict):
        add_attention = {k: add_attention for k in ['identification', 'detection', 'sexing']}
    if not isinstance(skip_connection, dict):
        skip_connection = {k: skip_connection for k in ['identification', 'detection', 'sexing']}

    if "multiply" in heads and "clip" in heads:
        raise ValueError(f"Both 'multiply' and 'clip' were found in the heads argument. Please provide at most one.")

    if not isinstance(output_bias, dict):
        if output_bias is not None:
            output_bias = {k: tf.keras.initializers.Constant(o) if o is not None else None
                           for k, o in zip(heads, output_bias)}
        else:
            output_bias = {}
        output_bias.update({
            k: None for k in ['presence', 'identification', 'detection', 'sexing'] if k not in output_bias.keys()
        })

    channel_axis = -1 if K.image_data_format() == "channels_last" else 1
    time_axis = 2
    frequency_axis = 1

    # Pads along the time_axis only. ZeroPadding layers don't include the batch axis that goes first
    padding = kwargs['padding'] if 'padding' in kwargs else [time_axis - 1]

    # Arguments that are common to all SparrowLayerFunction calls
    general_args = dict(
        conv_type=conv_type,
        activation=activation,
        activation_args=activation_args,
        causal=causal,
        renorm=renorm,
        padding=padding,
        conv_kwargs={k: v for k, v in kwargs.items() if k != "padding"},
        squeeze_excite=squeeze_excite,
    )

    if first_layer_args is None:
        first_layer_args = dict()

    if shape and not inputs:
        inputs = Input(shape=shape)

    if first_layer is not None:
        first = first_layer(**first_layer_args)
        first.build(inputs.shape)
        pre = first(inputs)
        pre = BatchNormalization(axis=[channel_axis, frequency_axis])(pre)
    else:
        pre = BatchNormalization(axis=[channel_axis, frequency_axis])(inputs)

    if model_type == "PoolConv":
        x = ConvPoolSparrowFrontEnd(x=pre,
                                    pool_type=pool_type,
                                    activation=activation,
                                    activation_args=activation_args,
                                    causal=causal,
                                    expanded=expanded is True,
                                    renorm=renorm,
                                    squeeze_excite=squeeze_excite,
                                    **kwargs
                                    )
    elif model_type == "MSD":
        x = MSD_Sparrow(x=pre,
                        pool_type=pool_type,
                        activation=activation,
                        activation_args=activation_args,
                        # expanded=expanded is True,
                        renorm=renorm,
                        growth=16,  # 32 if expanded is True else 16,
                        depth=2,
                        causal=causal,
                        dilation=[1, 2, 4, 8],
                        squeeze_excite=squeeze_excite,
                        **kwargs)

    x = SparrowLayerFunction(
        x,
        filters=512 if expanded else 64,
        # time_summary_kernel
        kernel_size=(1, 9),
        # dropout_rate=dropout_rate,
        **general_args,
    )

    x = SparrowLayerFunction(
        x,
        filters=512 if expanded else 64,
        kernel_size=(1, 1),
        # dropout_rate=dropout_rate,
        **general_args,
    )
    module = multi_scale_dense_module(
        x,
        growth=64,
        depth=2,
        dilation=[1, 2, 4],
        kernel_size=(1, 3),
        c=0.,
        ** general_args,
        # dropout_rate=dropout_rate,
        # expanded=expanded,
        )

    common_head_args = dict(
        dropout_rate=dropout_rate,
        general_args=general_args,
        final_activation=final_activation,
        expanded=expanded,
    )

    presence, presence_out = PresenceHead(
        x=module if variant['presence'] else x,
        n=n,
        **common_head_args,
        output_bias=output_bias['presence'],
        name=None if "presence" in combinations.keys() and combination_op is not None else "presence",
    )

    id, id_out = Head(
        model=module if variant['identification'] else x,
        n=n,
        **common_head_args,
        output_bias=output_bias['identification'],
        add_recurrent=add_recurrent['identification'],
        skip_connection=skip_connection['identification'],
        add_attention=add_attention['identification'],
        name="identification",
        combination=("identification" in combinations.keys() and combination_op is not None),
    )

    detection, detection_out = Head(
        model=module if variant['detection'] else x,
        n=1,
        **common_head_args,
        output_bias=output_bias['detection'],
        add_recurrent=add_recurrent['detection'],
        skip_connection=skip_connection['detection'],
        add_attention=add_attention['detection'],
        name="detection",
        combination=("detection" in combinations.keys() and combination_op is not None),
    )

    sexing, sexing_out = Head(
        model=module if variant['sexing'] else x,
        n=2,
        **common_head_args,
        output_bias=output_bias['sexing'],
        add_recurrent=add_recurrent['sexing'],
        skip_connection=skip_connection['sexing'],
        add_attention=add_attention['sexing'],
        name="sexing",
        combination=("sexing" in combinations.keys() and combination_op is not None),
    )

    potential_outs = dict(zip(['detection', 'identification', 'presence', 'sexing'],
                              [detection_out, id_out, presence_out, sexing_out]))
    outs = {k: v for k, v in potential_outs.items() if k in heads}

    # Combines the outputs of several heads
    # combinations is a dict of the form {'head': ['head', 'head']}
    # e.g.: {'identification': ['presence', 'detection']}
    # for each  key, all values will be combined into the key head using combination_op
    # 'sexing' is a special case because it is not broadcastble with 'identification' or 'presence'; to allow it
    # anyway, we use a mask corresponding to which individuals are males or females
    if len(outs) > 1 and combination_op is not None and combinations is not None:
        new_outs = {
            k: combination_op(
                name=k,
                dtype=tf.float32
            )([outs[k]] + [(v if _ != 'sexing' else MatMul(mask=tf.constant(sexing_mask))(outs['sexing'])) for _, v in outs.items() if _ in comb_heads])
            for k, comb_heads in combinations.items() if k in outs.keys()
        }

        outs.update(new_outs)

    if add_reconstruct:
        outs.update({'reconstructed': Reconstruct_Head(x=module,
                                                       n=inputs.shape[channel_axis],
                                                       filters=256,
                                                       activation=activation,
                                                       final_activation='relu')
                     })

    model = tf.keras.Model(inputs=inputs,
                           outputs=outs,
                           name=f"{model_type}_{name}")

    return model


if __name__ == "__main__":
    inputs = tf.keras.Input(shape=(80, 800, 6))
    heads = ["presence", "detection", 'identification', "sexing"]
    mod = SparrowNet(
        inputs=inputs,
        model_type="MSD",
        n=15,
        activation=mish,
        pool_type="conv",
        conv_type=Conv2D,
        combination_op=Clip,
        add_recurrent=True,
        heads=heads,
        variant=True,
        expanded=True,
        combinations={"identification": ['presence', 'detection'], 'sexing': ['detection']},
        add_reconstruct=False,
    )
    tf.keras.utils.plot_model(mod, to_file='C:/Users/killian/Desktop/model.png', show_shapes=True)
    print(mod.summary())
