"""
Script for evaluation of models.

Two possible modes (might benefit from splitting into different scripts?).
'eval' requires defining metrics and will run model.evaluate on a given generator with these metrics.
'plot' plots precision and recall plots on the model outputs as a function of threshold value.

Both modes can be passed a directory of models (intended as folders with the checkpoints in a subdirectory).

"""
import os
import tensorflow as tf
import importlib.util
import pickle
from tqdm import tqdm
import pandas as pd

from model.audio_generator import VariableOutputAudioGenerator
from utilities.functions import flatten, get_models
from utilities.custom_layers import mish, LearnedDRC
from utilities.metrics import MultichannelAUC, FocalCrossentropy, MyMSE
from utilities.pcen import PCEN
from utilities.output_manipulation import format_outputs


os.chdir('C:/Users/killian/Desktop/database_rooks')

# model(s) path (must be a list, but can be the path to a directory containing several models)
models = ["runs/temp"]
models = flatten([[os.path.join(m, f) for f in os.listdir(m)] for m in models])
# data path (must be the path to a directory containing both audio and label files of the formats given in config)
data_path = "data/test"
# output path (where to write out the performance.csv file that will contain the model metrics)
out_path = "performance.csv"

model_comparison_path = "model_tests"

# Retrieve model(s)
model_path = get_models(models=models,
                        # get_best_only=["val_ap",
                        #                "val_detection_ap",
                        #                "val_sexing_ap",
                        #                "val_identification_ap"]
                        )

# Load models
custom_objects = {
    "PCEN": PCEN,
    "mish": mish,
    "LearnedDRC": LearnedDRC,
    # "MagnitudeToDecibel": MagnitudeToDecibel,
    "MultichannelAUC": MultichannelAUC,
    "FocalCrossentropy": FocalCrossentropy,
    "MyMSE": MyMSE
}


# Predictions
full_table = []
for m in tqdm(model_path, total=len(model_path)):
    try:
        import re
        spec = importlib.util.spec_from_file_location("config", re.sub(pattern='checkpoints.+', repl='config.py', string=m))
        config = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(config)
    except FileNotFoundError:
        import config

    # Get pattern
    pattern = re.search(pattern='[0-9]+_[0-9]+', string=m)

    # Retrive model
    model = tf.keras.models.load_model(m,
                                       custom_objects=custom_objects,
                                       compile=False)

    # Create generator
    gen = VariableOutputAudioGenerator
    gen_args = dict(data=f"{data_path}",
                    n_channels=config.n_channels,
                    add_reconstruct=config.model_parameters['add_reconstruct'] if 'add_reconstruct' in config.model_parameters else False,
                    **config.multiclass,
                    **config.val_params)

    heads = dict(zip(['identification', 'detection', 'presence', 'sexing'], [False]*4))
    heads.update(dict(zip(model.output_shape.keys(), [True] * len(model.outputs))))

    local_gen = gen(**gen_args,
                    **heads,
                    sexing_mask=config.sexing_mask,
                    )

    # Recreate metrics
    metric_args = dict(mode="macro", num_thresholds=200)
    auroc_args = {**metric_args, **dict(curve="roc", name="auroc")}
    auprc_args = {**metric_args, **dict(curve="pr", name="ap")}
    metrics_list = {k: [MultichannelAUC(num_classes=c, **auroc_args), MultichannelAUC(num_classes=c, **auprc_args)]
                    for k, c in zip(['identification', 'sexing', 'detection', 'presence'],
                                    [config.n_classes, 2, 1, config.n_classes])}
    metrics_dict = {k: v for k, v in metrics_list.items() if heads[k]}

    # Compute metrics and add to the table
    model.compile(loss=None, metrics=metrics_dict)
    result = model.evaluate(x=local_gen, return_dict=True)
    result = {k: [v] if not isinstance(v, list) else v for k, v in result.items()}
    full_table.append(pd.DataFrame(result))

    # Save predictions
    if model_comparison_path is not None:
        predictions = format_outputs(model, data=local_gen, return_labels=True)
        with open(os.path.join(model_comparison_path, f'{pattern.group()}_ovlp{int(config.val_params["chunk_ovlp"] * 100)}.pickle'),
                  'wb') as connection:
            pickle.dump(obj=predictions, file=connection, protocol=pickle.HIGHEST_PROTOCOL)

# Write out the table
full_table = pd.concat(full_table, axis=0)
full_table["Model"] = model_path
full_table.to_csv(out_path, index=False)
