"""
Predict and save predictions on new data.
"""
import os
import re
import importlib.util
import tensorflow as tf
import numpy as np
import pickle
from tqdm import tqdm
import pandas as pd
from gc import collect

import soundfile as sf

from model.audio_generator import VariableOutputAudioGenerator, SyncAudioGenerator

from utilities.output_manipulation import format_outputs
from utilities.custom_layers import mish
from utilities.pcen import PCEN

# Common root
root = "C:/Users/killian/Desktop/database_rooks"

# Path to the model (should end in a .tf file)
model_path = [f'{root}/inference_models/20220603_225427/checkpoints/ep30.tf',
              f'{root}/inference_models/20220613_214724/checkpoints/ep20.tf',
              f'{root}/inference_models/20220630_003933/checkpoints/ep24.tf']

# Path to wav files
data_path = 'F:/Database_Backup/data/2020_Killian/Rooks/raw/audio/strasbourg_aviary'
# data_path = f'{root}/unseen'
# data_path = 'C:/Users/killian/Desktop/test_audio'
# Path to output
# out_path = f"{root}/unseen"
out_path = 'C:/Users/killian/Desktop/test_audio'

# Generator type
# gen = VariableOutputAudioGenerator
gen = SyncAudioGenerator


# Import the relevant config(s) and fallback to the config.py script in the main directory
import config
# Generator arguments
gen_args = dict(n_channels=config.n_channels,
                sexing_mask=config.sexing_mask,
                **config.multiclass,
                **config.val_params)
audio_format = config.val_params['audio_format']

models = [tf.keras.models.load_model(path,
                                     custom_objects={"PCEN": PCEN, "mish": mish},
                                     compile=False) for path in model_path]

if gen == SyncAudioGenerator:
    # Read in offset files
    offsets = pd.read_csv('C:/Users/killian/Desktop/audio_sync_with_offsets.tsv', sep="\t")

    offsets = offsets.loc[(offsets['date'].astype('str').str.contains('|'.join(['202001|202002|202101|202102'])))
                          & (~offsets['offset'].isnull())
                          & ((offsets['computed_length'] - offsets['expected_length']).abs() < 1)]
    offsets = offsets.reset_index(drop=True)

    for _, group in offsets.groupby(['group_name']):
        file = re.sub(pattern=audio_format, repl='pickle', string=group["group_name"].iloc[0])

        sr = sf.info(file=f'{data_path}/{group["file"].iloc[0]}').samplerate
        audio_size = int(round(group['computed_length'].iloc[0] * sr))

        file_offsets = group.groupby('file').agg({'offset': 'min'})['offset']

        audio = np.zeros(shape=(audio_size, group.shape[0]), dtype=np.int16)
        for i, (aud, off) in enumerate(zip(group['file'].unique(), file_offsets)):
            audio[slice(off, off + sf.info(file=f'{data_path}/{aud}').frames), slice(2*i, 2*i+2)] = sf.read(file=f'{data_path}/{aud}', dtype='int16')[0]
        sf.write(file=f'{out_path}/{file.replace("pickle", "flac")}', data=audio, samplerate=sr)

        local_gen = gen(data=[f'{data_path}/{_}' for _ in list(set(group['file']))],
                        offsets=group['offset'],
                        **gen_args)

        predictions = format_outputs(model=models, data=local_gen, return_labels=False)[-1]

        with open(os.path.join(out_path, file.replace('wav', 'pickle')), 'wb') as connection:
            pickle.dump(obj=predictions, file=connection, protocol=pickle.HIGHEST_PROTOCOL)

        _ = collect()

else:
    # List of files
    files = os.listdir(data_path)
    files = [f for f in files
             if bool(re.search(pattern=audio_format, string=f))
             and not re.sub(pattern=audio_format, string=f, repl='pickle') in files]

    for file in tqdm(files, total=len(files)):
        local_gen = gen(data=os.path.join(data_path, file), **gen_args)
        predictions = format_outputs(model=models, data=local_gen, return_labels=False)[-1]

        with open(os.path.join(out_path, re.sub(pattern=audio_format, repl='pickle', string=file)), 'wb') as connection:
            pickle.dump(obj=predictions, file=connection, protocol=pickle.HIGHEST_PROTOCOL)






