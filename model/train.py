"""
Handles network training.
See config to define most parameters, but several variables are defined here for convenience
"""

import os
import tensorflow as tf
import tensorflow.keras.backend as K
import random
import numpy as np
from kapre import MagnitudeToDecibel

from model.sparrow import SparrowNet
from model.audio_generator import VariableOutputAudioGenerator, VariableOutputTrainingGenerator

from utilities.metrics import MultichannelAUC, FocalCrossentropy, MyMSE
from utilities.pcen import PCEN
from utilities.custom_layers import LearnedDRC
from utilities.functions import product_dict
from utilities.callbacks import PlotModel

from utilities.send_email import send_email

import config

# "net" trains a network as normal. "lr" runs a learning rate range test to determine best learning rate values.
train_type = "net"

train_gen_type = VariableOutputTrainingGenerator
val_gen_type = VariableOutputAudioGenerator
model_parameters_list = product_dict(**config.loop_parameters)
number_of_trainings = len(list(product_dict(**config.loop_parameters)))

for k, local_params in enumerate(model_parameters_list):
    # Set seeds
    if 'seed' in local_params.keys():
        # that might be overkill, but at least I'm sure to cover my bases
        tf.random.set_seed(local_params['seed'])
        random.seed(local_params['seed'])
        np.random.seed(local_params['seed'])
        local_params.pop('seed')

    # Define the generators
    if "heads" not in local_params.keys() or local_params["heads"] == []:
        outputs = dict(identification=True)
    else:
        outputs = dict(
            identification="identification" in local_params["heads"],
            detection="detection" in local_params["heads"],
            presence="presence" in local_params["heads"],
            sexing="sexing" in local_params["heads"]
        )
    train_gen = train_gen_type(
        **outputs,
        **config.train_params,
        **config.multiclass,
        data=f"{config.data}/training",
        augment_function=config.augmenter,
        n_channels=config.n_channels,
        sexing_mask=config.sexing_mask,
        add_reconstruct=config.model_parameters['add_reconstruct'],
    )
    val_gen = val_gen_type(
        **outputs,
        **config.val_params,
        **config.multiclass,
        data=f"{config.data}/validation",
        n_channels=config.n_channels,
        sexing_mask=config.sexing_mask,
        add_reconstruct=config.model_parameters['add_reconstruct'],
    )

    # Create the model
    param_name = '_'.join([f'{k}{v.__name__ if callable(v) else v if isinstance(v, str) else None}'
                           for k, v in local_params.items()])
    model_name = f"SparrowNet_{param_name}_n{config.n_classes}"

    # First layer arguments
    first_layer_params = config.layer_kwargs
    if local_params["first_layer"] == "PCEN":
        first_layer_params.update(config.pcen_params)
        local_params.update({'first_layer': PCEN})
    elif local_params["first_layer"] == "learnDRC":
        local_params.update({'first_layer': LearnedDRC})
        first_layer_params.update(config.learn_drc_params)
    else:
        # using Decibel for a log compression
        local_params.update({'first_layer': MagnitudeToDecibel})

    frames = int(config.params["duration"] / ((1 - config.params["ovlp"] / 100) * config.params["wl"]))
    input_shape = (config.params["n_mels"], frames, config.n_channels)

    # Losses objects
    if not local_params['heads']:
        loss_dict = {
            "identification": FocalCrossentropy(**config.loss_args,
                                                num_classes=config.n_classes,
                                                class_weights=None)
        }
        local_params['heads'] = ['identification']
    else:
        loss_dict = {
            k: FocalCrossentropy(**config.loss_args,
                                 num_classes=n,
                                 class_weights=weights)
            for k, n, weights in zip(['identification', 'sexing', 'detection', 'presence'],
                                     [config.n_classes, 2, 1, config.n_classes],
                                     [None, None, None, None])
            if k in local_params['heads']
        }
    if config.model_parameters['add_reconstruct']:
        loss_dict.update({'reconstructed': MyMSE()})

    model = SparrowNet(
        shape=input_shape,
        **local_params,
        first_layer_args=first_layer_params,
        **config.model_parameters,
        **config.conv_kwargs,
        sexing_mask=config.sexing_mask,
        output_bias=None,
        name=model_name,
    )
    print(local_params["heads"], model.name)
    print(model.summary())

    # Compile: update loss and metrics according to heads
    compile_args = dict(
        optimizer=config.optimiser_initialisation(
            steps=train_gen.__len__(),
            cycle_length=config.lr_cycle,
            training=train_type),
        loss=loss_dict,
        # loss_weights={'detection': 1., 'identification': 1., 'presence': 1., 'sexing': 1.}
    )

    # Train
    if train_type == "lr":
        model.compile(**compile_args)
        from utilities.callbacks import LearningRateRangeFinder
        import datetime

        def callback_func(path,
                          epochs,
                          lr_min=1e-10,
                          lr_max=10,
                          ):
            path = os.path.join(path, datetime.datetime.now().strftime('%Y%m%d_%H%M%S'))
            if not os.path.exists(path):
                os.makedirs(path)

            stop_if_nan = tf.keras.callbacks.TerminateOnNaN()
            lr_finder = LearningRateRangeFinder(filename=f"{path}/lr_range.csv",
                                                e=epochs,
                                                lr_min=lr_min,
                                                lr_max=lr_max)
            plot = PlotModel(to_file=os.path.join(path, "model.png"))

            return [stop_if_nan, lr_finder, plot]

        epochs = 5
        model.fit(
            x=train_gen,
            epochs=epochs,
            verbose=1,
            callbacks=callback_func(path=f"lr_test/{model_name}",
                                    epochs=epochs,
                                    lr_max=10)
        )

    elif train_type == "net":
        # Define metrics
        metric_args = dict(mode="macro",
                           num_thresholds=200)
        auroc_args = {**metric_args, **dict(curve="roc", name="auroc")}
        auprc_args = {**metric_args, **dict(curve="pr", name="ap")}
        metrics_list = {k: [MultichannelAUC(num_classes=c, **auroc_args,),
                            MultichannelAUC(num_classes=c, **auprc_args)]
                        for k, c in zip(['identification', 'sexing', 'detection', 'presence'],
                                        [config.n_classes,  2,        1,           config.n_classes])}
        if "heads" not in local_params.keys() or local_params["heads"] == []:
            metrics_dict = {"identification": metrics_list['identification']}
        else:
            metrics_dict = {k: v for k, v in metrics_list.items() if k in local_params["heads"]}

        # Monitor the AUPRC (ap) of the correct task
        if "heads" not in local_params.keys() or len(local_params["heads"]) <= 1:
            monitor = "val_ap"
        elif "identification" in local_params["heads"]:
            monitor = "val_identification_ap"
        elif "sexing" in local_params["heads"]:
            monitor = "val_sexing_ap"
        elif "detection" in local_params["heads"]:
            monitor = "val_detection_ap"
        else:
            monitor = "val_ap"

        model.compile(**compile_args, metrics=metrics_dict)
        model.fit(
            x=train_gen,
            validation_data=val_gen,
            **config.train_args,
            callbacks=config.callbacks(
                **config.callback_args,
                monitor=monitor,
                path=f"{config.path}/{model_name}"
            )
        )

    # Notify me (NOTE: for privacy reasons this function is not in the repository at the moment)
    send_email(body=f"Entrainement {k+1} sur {number_of_trainings} fini !")

    # Clear session
    K.clear_session()
