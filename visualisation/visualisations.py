"""
A few functions for model output visualization.
"""

import tensorflow as tf
from sklearn.metrics import roc_curve, precision_recall_curve, average_precision_score, roc_auc_score
from sklearn.calibration import calibration_curve
import matplotlib.pyplot as plt
from utilities.metrics import MultichannelAUC
from utilities.output_manipulation import format_outputs
from tensorflow.keras.metrics import AUC


def model_diagnostics(y_true, y_pred,
                      class_names=None,
                      model_names=None,
                      plot_breakdown="models",
                      title=None):
    """
    Base function for plotting performance curves.

    :param y_true: Ground truth. Single tensor of arbitrary shape [..., classes]
    :param y_pred: Model predictions. List of tensors of the same shape [..., classes], one per model.
    :param class_names: List of names for the classes
    :param model_names: List of names for the models
    :param plot_breakdown: If "models", the plot columns will correspond to each model. If "classes", the plot columns
    will correspond to each class. Can be passed as an unambiguous abbreviation (e.g. "mod", "class").
    :param title: An optional overall title for the plots.
    :param add_baselines: If True, simulates an additional all-negative classifier.

    :return: A grid of plots. Each row is, in turn: ROC curves, PRC curves, calibration curves. A fourth row contains the
    legends of the previous three, with associated scores
    """

    if title is not None:
        plt.suptitle(title)

    y_pred = tf.unstack(y_pred, axis=0)
    y_pred = tf.concat(y_pred, axis=0)

    y_true = tf.unstack(y_true, axis=0)
    y_true = tf.concat(y_true, axis=0)

    y_true = tf.transpose(y_true)
    y_pred = tf.transpose(y_pred)

    if plot_breakdown in "models":
        # Make one plot column per MODEL
        ncols = y_pred.shape[1]
        y_pred = tf.unstack(y_pred, axis=1)
        label_names = class_names
        legend_names = model_names
    else:
        # Make one plot column per CLASS
        ncols = y_pred.shape[0]
        y_pred = tf.unstack(y_pred, axis=0)
        label_names = model_names
        legend_names = class_names

    fig, axs = plt.subplots(nrows=6, ncols=ncols, sharex=True, sharey=True, squeeze=False)

    for i, pred in enumerate(y_pred):
        pred = tf.squeeze(pred)
        if tf.rank(y_true) > 1:
            for t, p in zip(y_true, pred):
                if tf.reduce_max(t) > 0.:
                    fpr, tpr, _ = roc_curve(t, p)
                    axs[0, i].plot(fpr, tpr, label=legend_names)

                    prec, rec, thr = precision_recall_curve(t, p)
                    axs[1, i].plot(rec, prec, label=legend_names)
                    axs[2, i].plot(thr, prec[1:], label=legend_names)
                    axs[3, i].plot(thr, rec[1:], label=legend_names)
                    axs[4, i].plot(thr, 2 / ((1/prec[1:]) + (1/rec[1:])), label=legend_names)

                    prob_true, prob_pred = calibration_curve(t, p)
                    axs[5, i].plot(prob_true, prob_pred, label=legend_names)

                    # true_hist = p[t == 1].numpy().flatten()
                    # false_hist = p[t == 0].numpy().flatten()
                    # axs[3, i].hist(true_hist, bins=20, alpha=.5)
                    # axs[3, i].hist(false_hist, bins=20, alpha=.5)

        else:
            fpr, tpr, _ = roc_curve(y_true, pred)
            axs[0, i].plot(fpr, tpr, label=legend_names)

            prec, rec, thr = precision_recall_curve(y_true, pred)
            axs[1, i].plot(rec, prec, label=legend_names)
            axs[2, i].plot(thr, prec[1:], label=legend_names)
            axs[3, i].plot(thr, rec[1:], label=legend_names)
            axs[4, i].plot(thr, 2 / ((1 / prec[1:]) + (1 / rec[1:])), label=legend_names)

            prob_true, prob_pred = calibration_curve(y_true, pred)
            axs[5, i].plot(prob_true, prob_pred, label=legend_names)

        axs[0, i].plot([0, 1], [0, 1], color="black", label="_no_legend_", linewidth=1)
        if label_names is not None:
            axs[0, i].title.set_text(label_names[i])
        axs[0, i].axis([0, 1, 0, 1])

        # not a meaningful baseline for precision-recall curve
        # ax2.plot([0, 1], [1, 0], transform=ax2.transAxes, color="black", label="_no_legend_", linewidth=1)
        axs[1, i].axis([0, 1, 0, 1])
        axs[2, i].axis([0, 1, 0, 1])
        axs[3, i].axis([0, 1, 0, 1])
        axs[4, i].axis([0, 1, 0, 1])

        axs[5, i].plot([0, 1], [0, 1], color="black", label="_no_legend_", linewidth=1)
        axs[5, i].axis([0, 1, 0, 1])

    # if isinstance(y_true, list):
    #     for t, p in zip(y_true, y_pred):
    #         if tf.reduce_max(t) > 0.:
    #             y, x, _ = det_curve(t, p)
    #             ax5.plot(x, y, label=names)
    # else:
    #     for p in y_pred:
    #         x, y, _ = det_curve(y_true, p)
    #         ax5.plot(x, y, label=names)
    #
    # ax5.set_xlabel("False positive rate")
    # ax5.set_ylabel("False negative rate")
    # ax5.axis([0, 1, 0, 1])
        auroc = MultichannelAUC(curve="roc")
        auprc = MultichannelAUC(curve="pr")
        auroc1 = AUC(curve="roc")
        auprc1 = AUC(curve="pr")
        if legend_names is not None:
            legend_names = [""] * ncols
        if tf.rank(y_true) > 1:
            names = [n +
                     f" (AUROC {(round(100 * roc_auc_score(t, p, average='micro'), 2),  round(100 * auroc(t, p).numpy(), 2)), (round(100 * auroc1(t, p).numpy(), 2))}% -" +
                     f" AUPRC {(round(100 * average_precision_score(t, p, average='micro'), 2), round(100 * auprc(t, p).numpy(), 2), round(100 * auprc1(t, p).numpy(), 2) )}% -" +
                     f" Positives {round(100 * tf.reduce_mean(t).numpy(), 2)}%)"
                     for n, t, p in zip(legend_names, y_true, pred) if tf.reduce_max(t) > 0.]
        else:
            names = [n +
                     f" (AUROC {(round(100 * roc_auc_score(y_true, p), 2),  round(100 * auroc(y_true, p).numpy(), 2), round(100 * auroc1(y_true, p).numpy(), 2))}% -" +
                     f" AUPRC {(round(100 * average_precision_score(y_true, p), 2),round(100 * auprc(y_true, p).numpy(), 2), round(100 * auprc1(y_true, p).numpy(), 2) )}% -" +
                     f" Positives {round(100 * tf.reduce_mean(y_true).numpy(), 2)}%)"
                     for n, p in zip(legend_names, pred)]
        print(names)

    axs[0, ncols-1].set_xlabel("False Positive Rate")
    axs[0, 0].set_ylabel("True Positive Rate")
    axs[1, ncols-1].set_xlabel("Recall")
    axs[1, 0].set_ylabel("Precision")
    axs[2, ncols-1].set_xlabel("Threshold")
    axs[2, 0].set_ylabel("Precision")
    axs[3, ncols-1].set_xlabel("Threshold")
    axs[3, 0].set_ylabel("Recall")
    axs[4, ncols-1].set_xlabel("Threshold")
    axs[4, 0].set_ylabel("F1-score")
    axs[5, ncols-1].set_xlabel("Fraction of positives")
    axs[5, 0].set_ylabel("Mean predicted value")

    plt.legend(legend_names, loc="center")
    plt.show()

    return None


def plot_predictions(
        model,
        data
):
    # model_layers = [layer for layer in model.layers if not ("clip" in layer.name or "multiply" in layer.name)]

    inp, true, pred = format_outputs(model, data=data)

    n = inp.shape[0]
    for i in range(n):
        fig, axs = plt.subplots(nrows=len(pred) + 1)
        S = tf.reduce_mean(inp[i], axis=-1)
        axs[0].imshow(S ** (1./3.), origin="lower")
        for j, key in enumerate(pred.keys()):
            if pred[key][i].shape[0] == S.shape[-1]:
                t = axs[j+1].plot(pred[key][i])
                if key == "identification":
                    plt.legend(t,
                               ['Balbo', 'Bashir', 'Braad', 'Brain', 'Elie', 'Feisty',
                                'Gigi', 'Jolene', 'Jonas', 'Kafka', 'Merlin', 'Osiris', 'Pomme', 'Siobhan', 'Tom'],
                               bbox_to_anchor=(1.1, 1),
                               )
            else:
                t = axs[j+1].plot(tf.concat([pred[key][i]] * S.shape[-1], axis=0))
            axs[j+1].axis([0, S.shape[-1], 0, 1])
        [ax.set_aspect('auto') for ax in axs]
        plt.show()
