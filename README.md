# Automatic detection of rook vocalisations

Code used in our experiments for the paper "Acoustic detection and identification of individual rooks in field recordings using multi-task neural networks".

The dataset we collected and used for this task can be found on Zenodo.

The task was to learn to recognise the individual rook emitting each vocalisation. The study group was a captive colony of rooks housed in an outdoor aviary in Strasbourg, France. 
We implemented the task as a temporal framewise detection/classification approach, with sub-tasks dedicated to detecting rook vocalisations, sexing the emitter of each vocalisation, detecting the individual rooks present in a clip and finally identifying the individual emitting a vocalisation.


## Structure of the repository
- config.py: configuration script used to parametrise runs.
- model: scripts for building, training, evaluating and predicting using the model. Also contains the audio_generator script to read in the audio files and the labels.
- utilities: various functions, custom layers and other tensorflow objects. Contains the PCEN layer definition
- visualisation: scripts used in previous versions but not adjusted for the current one.


## Usage of pcen.py
Per Channel Energy Normalisation ([Wang et al, 2018](https://arxiv.org/abs/1607.05666)) is an operation that combines automatic gain control (AGC) and dynamic range compression (DRC). It also attenuates slow-varying events (usually background noises). 

PCEN involves 5 parameters (potentially frequency and/or time dependent), but since it is linearly differentiable with respect to each, it can be implemented as a neural network layer and the parameters learning as part of network training.

The layer can be put in front of any neural network to normalise input spectrograms. See docstring in `pcen.py` for details regarding parameters

WIP: the current version needs specific dimensions (except for the batch size). Current objective is to bypass this requirement at least for the axes that don't need to be of fixed size (e.g. temporal axis if learning parameters along the frequency axis), possibly by impelementing an RNN cell for the FIR filter.

## audio_generator.py
Contains generator classes used for reading the audio files and the labels. Two different generators are usable in model training:
    - `TrainingGenerator` takes a collection of audio and label files, and returns batches of Mel-spectrograms of windows sampled around each vocalisation in the labels, and optionally background noise samples, all in random order
    - `AudioGenerator` takes one or more audio files and optionally label files (if used for evaluation) and simply reads them in order, splitting them in clips of the correct length (with padding if necessary).


## How to use
### Clone the repository somewhere on your computer

Note: The original code was set up in a PyCharm environment, and all the scripts are coded to run from within it (or another IDE), not from the command line.

### Project structure
The repository is meant to train networks on mel-spectrograms extracted from long recordings, not specifically spectrograms for pre-extracted clips (although this could be adapted easily by modifying audio_generator).
The project structure should have a single folder as the root (that does not need to contain the code):
- root
    - data: each subfolder should contain both the audio and corresponding label files. Note that audio_generator does NOT try to match file names, so there should be a 1-to-1 correspondence of the audio and labels in the same order.
        - train
        - validation
        - test (optionally)
    - runs: to save the models

### Set up a training run
The parameters for the runs are all defined using the config.py script (see the comments within for explanations).
To replicate the runs used in our experiments, use the loop_parameters dict and edit the following values:
    
- for single-task runs:
    - `heads`: uncomment any combination of:
            `["detection"]`
            `["sexing"]`
            `["identification"]`
            `["presence"]`
    - `variant`: 
            `True` to insert a MSD module before the head,
            `False` to not insert the module
- for multi-task runs:
    - `heads`: `["identification", "detection", "presence", "sexing"]`
    - `variant`: `{'detection': False, 'identification': True, 'presence': True, 'sexing': False}`
    - `combination_op`: uncomment any combination of: 
        `None`,
        `tf.keras.layers.Multiply`,
        `Clip`

`loop_parameters` allows setting up multiple runs at once by passing a list of length > 1 to any combination of the key-value pairs. For instance, passing the following value for the `heads` key sets up two separate runs
    ```
    heads = [
        ['identification'], 
        ['presence']
    ]
    ```

Edit other arguments as desired, then execute the train.py script to launch the run.

In particular, if using another dataset, we strongly recommend editing the Generator parameters section:   
- adjust spectrogram settings in the `params` dict
- adjust `n_classes` and `n_channels` to reflect, respectively, the number of individuals in the data and the number of audio channels      (note that is is only a maximum value for padding all clips to the same number of channels)
- adjust the multiclass dict to reflect your labels:
    - `col`: column contain the individual IDs in the label files
    - `classes`: list of the individual IDs (order does not matter here, but will be sorted to alphabetical in the generator)
    - `exclude`: classes to ignore during training (for instance we ignored vocalisations from unknown individuals and multiple individuals vocalising at once, "Inc" and "Pls" respectively)
    - `multilabel_class`: classes that actually consist of several of the normal classes. If not an empty list, the generator looks for a "Comment" column in the label, and extracts the classes in the corresponding cell. The final label is considered as if all classes are present for the entire duration. 
        - For instance: if setting `multilabel_class = ['Pls']`, and a label carrying "Pls" in the `col` column contains "Merlin" and "Kafka" in the Comment column, the final label will consider that both individuals were vocalising for the entire duration. 
        - For our case this was not a realistic condition, so we left it empty


### Evaluating
Simply run the `evaluate.py` script after editing the correct path to both the saved model(s) and the test data.
Multiple models can be specfied and will all be evaluated with the same test data.
The script ends by writing a CSV file (with a customisable path, including name) containing a dataframe with:  
- one row per model
- one column per metric (defined in the script), with another giving the model id (by default, a filename containing the path to the specific model and the epoch)


### Predicting
Run the predict.py script after editing the correct paths (similar to Evaluating above).
This script can run in two different modes, depending on whether you pass a folder that contains both audio and label files or only audio files
- If both audio and label files are found: 
    - multiple models can be specified
    - the script will write out the raw outputs of each model to a .pickle file named after the model
- If only audio files are found:
    - only one model will be used (the first, if passing a list)
    - the script will write out the raw predictions of the model on each audio file SEPARATELY and write out one .pickle file and one .txt file with the same name as the audio file.
        The .pickle file contains the raw output of the model
        The .txt file contains processed predictions for further analysis/review. This processing uses the create_labels_from_network_function with a few arguments (see the script for info).
        Note that (as of 2022-02-18) the .txt file only contains predictions for the detection task. The incorporation of the other tasks is WIP.


## Citation
If using either the dataset at Zenodo or the code in this repository, please cite: (TODO)
